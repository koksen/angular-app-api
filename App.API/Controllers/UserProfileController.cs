﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using App.API.Filters;
using App.Core.Extensions;
using App.Core.Interfaces.Services;
using App.Core.ViewModels;

namespace App.API.Controllers
{
    [Authorize]
    public class UserProfileController : ApiController
    {
        private readonly IUserProfileService _userProfileService;

        public UserProfileController(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [ModelStateValidator]
        public IHttpActionResult Post(UserProfileSummary model)
        {
            try
            {
                model.UserId = User.GetUserID();
                
                var validator = _userProfileService.AddOrUpdate(model);

                if (validator.IsValid)
                    return Ok();

                foreach (var e in validator.Errors)
                {
                    ModelState.AddModelError(e.Key, e.Value.ToString());
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
