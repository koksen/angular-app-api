﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using App.API.Hubs;
using App.API.Models;

namespace App.API.Controllers
{

    [RoutePrefix("api/dashboard")]
    public class DashboardController : SignalRBase<WidgetHub>
    {
        // POST api/<controller>
        public HttpResponseMessage Post(Widget item)
        {
            if (item == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            // add to database in a real app

            // notify all connected clients
            Hub.Clients.All.newWidget(item);

            // return the item inside of a 201 response
            return Request.CreateResponse(HttpStatusCode.Created, item);
        }

        // GET api/values
        [Route("Analytics")]
        public IHttpActionResult GetAnalytics()
        {
            var model = new DashboardModel{
                Analytics = new AnalyticsInfo
                {
                    Growth = 28,
                    NewUsers = 56,
                    Profit = 4676.54M,
                    Sales = 367
                },
                ChartSeries = new List<ChartInfo>
                {
                    new ChartInfo {Name = "Some data 1", Data = new[] {1, 2, 4, 7, 3}},
                    new ChartInfo {Name = "Some data 2", Data = new[] {3, 1, 0, 7, 3}, ConnectNulls = true},
                    new ChartInfo {Name = "Some data 3", Data = new[] {5, 2, 2, 3, 5}, Type = ChartType.column},
                    new ChartInfo {Name = "Some data 4", Data = new[] {1, 1, 2, 3, 2}, Type = ChartType.column},
                    new ChartInfo {Name = "Some data 5", Data = new[] {1, 2, 4, 7, 3}}
                }
            };
            return Ok(model);
        }

    }
}