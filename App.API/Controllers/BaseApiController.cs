﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;
using App.API.Filters;
using App.API.Interfaces;
using App.API.Models;
using App.Common.Tracing;
using App.Core.Interfaces.Services;
using App.Core.Models.Common;
using App.Core.ViewModels;
using Omu.ValueInjecter;

namespace App.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TViewModel">The type of the view model.</typeparam>
    public abstract class BaseApiController<T, TViewModel> : ApiController, IBaseController<T, TViewModel>
        where T : BaseModel, new()
        where TViewModel : BaseViewModel, new()
    {
        protected IService<T> Service;
        protected Func<IQueryable<T>, IOrderedQueryable<T>> SortBy = null;
        protected Expression<Func<T, bool>> Predicate = null;
        protected int _masterUserId;
        protected IDictionary<string, IList<string>> Errors;

        /// <summary>
        /// Gets all records for the specified viewmodel type
        /// </summary>
        /// <returns>IQueryable collection</returns>
        public virtual IQueryable<TViewModel> GetAll()
        {
            
            var records = Service.Find(Predicate, SortBy).ToList();
            var totalRecords = records.Count();
            HttpContext.Current.Response.Headers.Add("X-InlineCount", totalRecords.ToString(CultureInfo.InvariantCulture));

            var models = records.Select( 
                            o => new TViewModel().InjectFrom(o) 
                        ).Cast<TViewModel>().AsQueryable();

            return models;
        }

        /// <summary>
        /// Gets a specific record by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public virtual IHttpActionResult Get(int id)
        {
            try
            {
                var o = Service.GetById(id);
                if (o == null) 
                    return NotFound();

                var model = new TViewModel().InjectFrom(o);
                return Ok(model);
            }
            catch (Exception ex)
            {
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Trace.Fail(opStatus.ExceptionMessage);
                return BadRequest();
            }
           
        }

        /// <summary>
        /// Determines whether the specified property is unique.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        [HttpGet]
        public virtual IHttpActionResult IsUnique(string property, object value)
        {
            try
            {
                bool unique = Service.IsUnique(property, Int32.Parse(value.ToString()));
                return Ok( new { unique });

            } catch (Exception ex) {
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Tracing.Error(opStatus.ExceptionMessage);
                return BadRequest();
            }
        }
        
        protected virtual T Save(TViewModel model)
        {
            var entity = new T();
            entity.InjectFrom(model);
           
            var validator = Service.SaveOrUpdate(entity);
            Errors = validator.Errors;

            return validator.Entity;

        }
        /// <summary>
        /// Posts the specified model. POST api/controller
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [ModelStateValidator]
        public virtual IHttpActionResult Post([FromBody]TViewModel model)
        {
            try
            {
                var entity = Save(model);
                if (entity==null)
                    return BadRequest(Errors.Values.ToString());

                string action;
                if (model.Id == 0)
                {
                    model.Id = entity.Id;
                    model.Created = entity.Created;
                    action = "created";
                }
                else
                {
                    action = "updated";
                }

                return Ok(String.Format("Record successfully {0}", action));
            }
            catch (Exception ex)
            {
                //Have the option to log detailed information from exceptions.
                //This also could be done by calling the event within the Event_OnError module
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Tracing.Error(opStatus.ExceptionMessage);
                return BadRequest(opStatus.ExceptionMessage);
            }
        }

        /// <summary>
        /// Updates a records.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [ModelStateValidator]
        public virtual IHttpActionResult Put([FromUri] int id, [FromBody] TViewModel model)
        {
            try
            {
              
                T entity = Service.GetById(id);
                if (entity == null)
                {
                    return NotFound();
                }

                entity.InjectFrom<FilterId>(model);
                Service.SaveOrUpdate(entity);

                return Ok(String.Format("Record successfully updated"));
            }
            catch (Exception ex)
            {
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Tracing.Error(opStatus.ExceptionMessage);
                return BadRequest(opStatus.ExceptionMessage);
            }
        }

        // DELETE api/<controller>/5
        /// <summary>
        /// Deletes a record.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [HttpDelete]
        public virtual IHttpActionResult Delete([FromUri]int id)
        {
            try
            {
                T o = Service.GetById(id);
                if (o == null) {
                    return NotFound();
                } 

                Service.Delete(id);
                return Ok(); 
            }
            catch (Exception ex)
            {
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Tracing.Error(opStatus.ExceptionMessage);
                return BadRequest();
            }
        }

        // DELETE api/<controller>/5,6,7
        /// <summary>
        /// Deletes many records by the specified ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        [HttpPut]
        public virtual IHttpActionResult Delete([FromBody]List<int> ids)
        {
            try
            {
                Service.Delete(ids);
                return Ok();
            }
            catch (Exception ex)
            {
                var opStatus = OperationStatus.CreateFromException(ex.Message, ex);
                Tracing.Error(opStatus.ExceptionMessage);
                return BadRequest();
            }
        }

    }
}