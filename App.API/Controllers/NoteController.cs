﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using App.API.Hubs;
using App.Core.Extensions;
using App.Core.Interfaces.Services;
using App.Core.ViewModels.Common;
using Omu.ValueInjecter;

namespace App.API.Controllers
{

    [RoutePrefix("api/notes")]
    public class NoteController : SignalRBase<NoteHub>
    {

        private readonly INoteService _noteService;

        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }

        // POST api/<controller>
        [Route("~/api/note/add", Name = "AddNote")]
        public HttpResponseMessage PostAddNote(NoteSummary item)
        {

            var note = _noteService.Create();
            note.InjectFrom(item);

            _noteService.SaveOrUpdate(note);
            // notify all connected clients
            Hub.Clients.All.newNote(item);

            // return the item inside of a 201 response
            return Request.CreateResponse(HttpStatusCode.Created, item);
        }

        // GET api/values
        [Route("Unseen")]
        public IHttpActionResult GetUnseen()
        {
            var userId = User.GetUserID();
            var model = _noteService.GetUnseen(userId);
            return Ok(model);
        }

        // GET api/values
        [Route("Status")]
        public IHttpActionResult PostStatus(NoteStatusUpdate model)
        {
            model.UserId = User.GetUserID();
            var ok = _noteService.UpdateStatus(model);
            return Ok(new { Success = ok });
        }

    }
}