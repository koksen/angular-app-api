﻿using App.Core.Interfaces.Services;
using App.Core.Models;
using App.Core.ViewModels;

namespace App.API.Controllers
{
    
    public partial class ThingController : BaseApiController<Thing, ThingSummary>
    {
        private readonly IThingService _thingService;

        public ThingController(IThingService thingService)
        {
            Service = _thingService = thingService; 
        }

    }
}