﻿using System.Threading.Tasks;
using System.Web.Http;
using App.Core.Models.Common;

namespace App.API.Controllers.Account
{

    public partial class AccountController
    {

        // POST api/Account/IsUsernameValid
        [Route("IsUsernameValid/{username}")]
        public async Task<IHttpActionResult> GetIsUsernameValid(string username)
        {
            var user = await UserManager.FindByNameAsync(username);

            return Ok( new InfoResult { Success = (user == null) });
        }

        // POST api/Account/IsEmailValid
        [Route("IsEmailValid/{email}")]
        public async Task<IHttpActionResult> GetIsEmailValid(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);

            return Ok(new InfoResult { Success = (user == null) });
        }  

    }
}
