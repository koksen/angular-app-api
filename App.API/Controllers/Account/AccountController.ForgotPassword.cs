﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;
using App.Core.Enums;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {

        /// <summary>
        /// If the user forget the password this action will send him a reset password mail
        /// </summary>
        /// <param name="model">The forgot password model</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPost]
        [AllowAnonymous]
        [ModelStateValidator]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordBindingModel model)
        {
            
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                ModelState.AddModelError("", "The user either does not exist or is not confirmed.");
                return BadRequest(ModelState);
            }

            var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var dict = new Dictionary<string, object>
            {
                { "userid", user.Id }, 
                { "code", HttpUtility.UrlEncode(code) }
            };
            var callbackUrl = GetCallbackUrl(AccountEvent.ResetPassword, dict);
            
            dict = new Dictionary<string, object>
            {
                { "url", callbackUrl }, 
                { "displayName", user.UserName }
            };
            var body = GetMailBody(AccountEvent.ResetPassword, dict); 
            await UserManager.SendEmailAsync(user.Id, "Reset password", body);

            return Ok();
            
        }

    }
}
