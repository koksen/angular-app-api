﻿using System.Threading.Tasks;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        // POST api/Account/ChangePhoneNumber
        [ModelStateValidator]
        [Route("ChangePhoneNumber")]
        public async Task<IHttpActionResult> ChangePhoneNumber(ChangePhoneNumberBindingModel model)
        {

            //var userId = User.GetUserID();
            
            //var token = await UserManager.GenerateChangePhoneNumberTokenAsync(userId, model.NewPhoneNumber);
            
            //IHttpActionResult errorResult = GetErrorResult(result);

            //if (errorResult != null)
            //{
            //    return errorResult;
            //}

            return Ok();
        }

    }
}
