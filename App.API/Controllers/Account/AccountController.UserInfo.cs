﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using App.API.Models.Account;
using App.Core.Extensions;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        /// <summary>
        /// Get user info
        /// 401 if not authenticated
        /// </summary>
        /// <returns>The user info</returns>
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public async Task<UserInfoViewModel> GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            var userIdentity = User.Identity as ClaimsIdentity;

            // Get roles from the user claims
            // We are setting the claims in the AuthenticationOAuthProvider properties
            var roles = new List<string>();
            if (userIdentity != null)
                userIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).ForEach(claim => roles.Add(claim.Value));

            //Check for Email confirmed
            var emailConfirmed = externalLogin != null || await UserManager.IsEmailConfirmedAsync(User.GetUserID());

            return new UserInfoViewModel
            {
                UserName = User.Identity.GetUserName(),
                IsEmailConfirmed = emailConfirmed,
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null,
                Roles = roles
            };
        }      

    }
}
