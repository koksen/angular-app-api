﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using App.API.Models.Account;
using App.Core.Enums;
using App.Core.Extensions;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    
    public partial class AccountController
    {
        
        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmail")] //{userId}/{code}
        public async Task<IHttpActionResult> ConfirmEmail(int userId, string code)
        {
            if (userId == 0 || code == null)
            {
                ModelState.AddModelError("error", "You need to provide your user id and confirmation code");
                return BadRequest(ModelState);
            }

            var justUser = await UserManager.FindByIdAsync(userId);
            if (justUser == null)
            {
                ModelState.AddModelError("error", "User not found");
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ConfirmEmailAsync(userId, code);
            var status = result.Succeeded ? "success" : "failure";

            var dict = new Dictionary<string, object> { { "status", status } };

            var callbackUrl = GetCallbackUrl(AccountEvent.ConfirmEmailResult, dict);
            return Redirect(callbackUrl);
            
        }

        [HttpPost]
        [Route("ResendConfirmationEmail", Name = "ResendConfirmationEmail")]
        public async Task<IHttpActionResult> ResendConfirmationEmail()
        {
            var user = await UserManager.FindByIdAsync(User.GetUserID());
            var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = Url.Link("ConfirmEmail", new { userId = user.Id, code = code });

            var notification = new AccountNotificationModel
            {
                Code = code,
                Url = callbackUrl,
                UserId = user.Id,
                Email = user.Email,
                DisplayName = user.UserName
            };

            string body = "Ok"; //ViewRenderer.RenderView("~/Views/Mailer/NewAccount.cshtml", notification);
            await UserManager.SendEmailAsync(user.Id, "Account confirmation", body);

            return Ok();
        }
       

    }
}
