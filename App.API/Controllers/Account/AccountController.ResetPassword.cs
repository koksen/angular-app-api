﻿using System.Threading.Tasks;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        /// <summary>
        /// Reset the user password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ModelStateValidator]
        [Route("ResetPassword", Name = "ResetPassword")]
        public async Task<IHttpActionResult> PostResetPassword(ResetPasswordBindingModel model)
        {
           
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "No user found.");
                return BadRequest(ModelState);
            }
            IdentityResult result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Ok();
            }

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }
       
    }
}
