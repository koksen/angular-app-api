﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using App.API.AppStart;
using App.Common.Extensions;
using App.Core.Enums;
using App.Core.Interfaces.Services;
using App.Core.Models;
using App.Infrastructure.Config;
using App.Infrastructure.IdentityManager;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace App.API.Controllers.Account
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public partial class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private readonly IUserProfileService _userProfileService;
        private readonly IUserSessionService _userSessionService;
        private UserProfileManager _userManager;

        public AccountController(IUserProfileService userProfileService, IUserSessionService userSessionService)
        {
            _userProfileService = userProfileService;
            _userSessionService = userSessionService;
            AccessTokenFormat = Startup.OAuthOptions.AccessTokenFormat;
        }

        public UserProfileManager UserManager
        {
            get
            {
                var um = Request.GetOwinContext().GetUserManager<UserProfileManager>();
                _userManager = um;
                return um;
            }
            private set
            {
                _userManager = value;
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            // Clear the temporary cookies used for external and two factor sign ins
            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            Authentication.SignIn(new AuthenticationProperties
            {
                IsPersistent = isPersistent
            },
            await user.GenerateUserIdentityAsync(UserManager));

            //throw new ApplicationException("An exception example from API.");
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserManager.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            var errors = ModelState;

            if (result == null)
                return InternalServerError();

            if (result.Succeeded) 
                return null;

            if (result.Errors != null)
            {
                foreach (var e in result.Errors)
                {
                    errors.AddModelError("", e);
                }
                //var errors = new List<KeyValuePair<string, string>>();
                //errors.AddRange(result.Errors.Select(error => new KeyValuePair<string, string>("", error)));
            }

            if (ModelState.IsValid)
            {
                // No ModelState errors are available to send, so just return an empty BadRequest.
                return BadRequest();
            }

            return BadRequest(errors);
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IEnumerable<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static readonly RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        private string GetCallbackUrl(AccountEvent evt, Dictionary<string, object> parameters)
        {
            var headers = Request.GetOwinContext().Request.Headers;
            var userAgent = headers["User-Agent"];

            var domain = AppConfig.Instance.Mailer.Domain;
            var callbackUrl = String.Format("{0}#/account/", domain);

            switch (evt)
            {
                case AccountEvent.ChangePassword:
                    return callbackUrl + ExecRegex("ChangePassword/?userid={userid}&code={code}", parameters);
                case AccountEvent.ConfirmEmail:
                    return domain + ExecRegex("/api/account/ConfirmEmail/?userid={userid}&code={code}", parameters);
                case AccountEvent.ConfirmEmailResult:
                    return callbackUrl + ExecRegex("Confirm/{status}", parameters);
                case AccountEvent.DeleteAccount:
                    return callbackUrl + ExecRegex("DeleteAccount/?userid={userid}&code={code}", parameters);
                case AccountEvent.ForgotPassword:
                    return callbackUrl + ExecRegex("ForgotPassword/?userid={userid}&code={code}", parameters);
                case AccountEvent.Register:
                    return callbackUrl + ExecRegex("SignUp", parameters);
                case AccountEvent.ResendConfirmationEmail:
                    return callbackUrl + ExecRegex("ResendConfirmationEmail/{userid}/{code}", parameters);
                case AccountEvent.ResetPassword:
                    return callbackUrl + ExecRegex("ResetPassword/{userid}/{code}", parameters);
                default :
                    return string.Empty;
            }

        }

        private static string GetMailBody(AccountEvent evt, Dictionary<string, object> parameters)
        {
            //var body = GetTemplate(evt);
            string body;
            switch (evt)
            {
                case AccountEvent.ChangePassword:
                    return ExecRegex("{displayName}, Please change your password by clicking : <a href=\"{url}\">here</a>", parameters);
                case AccountEvent.ConfirmEmail:
                    return ExecRegex("{displayName}, Please reset your password by clicking : <a href=\"{url}\">here</a>", parameters);
                case AccountEvent.DeleteAccount:
                    return ExecRegex("{displayName}, Please reset your password by clicking : <a href=\"{url}\">here</a>", parameters);
                case AccountEvent.ForgotPassword:
                    return ExecRegex("{displayName}, Please reset your password by clicking : <a href=\"{url}\">here</a>", parameters);
                case AccountEvent.ResendConfirmationEmail:
                    return ExecRegex("{displayName}, Please reset your password by clicking : <a href=\"{url}\">here</a>", parameters);
                case AccountEvent.ResetPassword:
                    return ExecRegex("{displayName}, Please reset your password by clicking : <a href=\"{url}\">here</a>", parameters);
                default:
                    return string.Empty;
            }
        }

        private static string GetTemplate(AccountEvent evt)
        {
            return evt.GetDescription();
        }

        static string ExecRegex(string url, IReadOnlyDictionary<string, object> parameters)
        {
            return Regex.Replace(url, @"\{(.+?)\}", m => parameters[m.Groups[1].Value].ToString() );                    
        }

        #endregion
    }
}
