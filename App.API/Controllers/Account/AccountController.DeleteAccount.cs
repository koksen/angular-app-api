﻿using System.Threading.Tasks;
using System.Web.Http;
using App.Core.Extensions;
using App.Core.Models;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {

        [HttpPost]
        [Route("DeleteAccount")]
        public async Task<IHttpActionResult> DeleteAccount()
        {
            User user = await UserManager.FindByIdAsync(User.GetUserID());

            if (user == null)
            {
                return BadRequest();
            }

            IdentityResult result = await UserManager.DeleteAsync(user);

            var errorResult = GetErrorResult(result);

            return errorResult ?? Ok();
        }       

    }
}
