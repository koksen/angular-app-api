﻿using System.Threading.Tasks;
using System.Web.Http;
using App.Core.Extensions;

namespace App.API.Controllers.Account
{
    
    public partial class AccountController
    {
        /// <summary>
        /// Get user info
        /// 401 if not authenticated
        /// </summary>
        /// <returns>The user info</returns>
        [Route("Profile")]
        public async Task<IHttpActionResult> GetProfile()
        {
            var userId = User.GetUserID();
            
            var user = await UserManager.FindByIdAsync(userId);
          
            if (user == null)
                return NotFound();

            var profileSummary = _userProfileService.GetSummary(user);
            
            return Ok(profileSummary);

        }

    }
}
