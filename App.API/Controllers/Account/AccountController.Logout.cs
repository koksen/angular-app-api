﻿using System.Web.Http;
using Microsoft.Owin.Security.Cookies;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {

            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }      

    }
}
