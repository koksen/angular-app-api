﻿using System.Threading.Tasks;
using System.Web.Http;
using App.Core.Enums;
using App.Core.Extensions;
using App.Core.ViewModels;
using App.Infrastructure.Helpers;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        /// <summary>
        /// Get user info
        /// 401 if not authenticated
        /// </summary>
        /// <returns>Locks user session</returns>
        [HttpPost]
        [Route("Lock")]
        public async Task<bool> Lock()
        {
            var appContext = new AppContext();
            var isMobile = appContext.IsMobile();
            var userSession = new UserSessionRequest
            {
                UserId = User.GetUserID(),
                Ip = appContext.GetIP(),
                UserAgent = appContext.GetUserAgent(),
                Media = !isMobile ? AccessMedia.Web : AccessMedia.Mobile,
                DeviceId = appContext.GetDeviceId()
            };
            return _userSessionService.Lock(userSession);
        }

        /// <summary>
        /// Get user info
        /// 401 if not authenticated
        /// </summary>
        /// <returns>Locks user session</returns>
        [Route("IsLocked")]
        public async Task<UnlockResponse> GetIsLocked()
        {
            var userSession = new UserSessionRequest
            {
                UserId = User.GetUserID()
            };
            return new UnlockResponse {Success = _userSessionService.Lock(userSession)};
        }

        /// <summary>
        /// Get user info
        /// 401 if not authenticated
        /// </summary>
        /// <returns>Locks user session</returns>
        [HttpPost]
        [Route("Unlock")]
        public async Task<UnlockResponse> Unlock(UnlockRequest model)
        {
            var appContext = new AppContext();
            var isMobile = appContext.IsMobile();

            var user = await UserManager.FindAsync(model.UserName, model.Password);

            if (user == null)
            {
                return new UnlockResponse { Success = false, Message = "Invalid username or password"} ;
            }

            var userSession = new UserSessionRequest
            {
                UserId = User.GetUserID(),
                Ip = appContext.GetIP(),
                UserAgent = appContext.GetUserAgent(),
                Media = !isMobile ? AccessMedia.Web : AccessMedia.Mobile,
                DeviceId = isMobile ? appContext.GetDeviceId() : string.Empty
            };

            return _userSessionService.UnLock(userSession);
        } 

    }
}
