﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using App.API.Models.Account;
using App.Core.Extensions;
using App.Infrastructure.Config;

namespace App.API.Controllers.Account
{
    
    public partial class AccountController
    {
        
        [AllowAnonymous]
        [HttpGet]
        [Route("VerifyPhoneNumber", Name = "VerifyPhoneNumber")] //{userId}/{code}
        public async Task<IHttpActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);

            if (user == null)
            {
                ModelState.AddModelError("error", "User not found");
                return BadRequest(ModelState);
            }

            var result = await UserManager.ChangePhoneNumberAsync(User.GetUserID(), user.PhoneNumber, model.Code);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to verify phone");
                return BadRequest(ModelState);                
            }
            
            await SignInAsync(user, false);

            var uri = new Uri(string.Format("{0}/#", AppConfig.Instance.Mailer.Domain));
            return Redirect(uri);
            
        }

    }
}
