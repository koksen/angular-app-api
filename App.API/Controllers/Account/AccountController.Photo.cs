﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using App.Infrastructure.Images;

namespace App.API.Controllers.Account
{
    
    public partial class AccountController 
    {

        [Route("Photo")]
        public Task<HttpResponseMessage> PostPhoto()
        {            

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var streamProvider = new MultipartMemoryStreamProvider();

            var t = Request.Content.ReadAsMultipartAsync(streamProvider).ContinueWith(task =>
                {
                    if (task.IsFaulted  || task.IsCanceled)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, task.Exception);
                    }

                    var stream = task.Result.Contents[0].ReadAsStreamAsync().Result;

                    if (!ImageManager.IsImage(stream))
                    {
                        throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                    }

                    if (stream.Length == 0)
                        return Request.CreateResponse(HttpStatusCode.NoContent);

                    var userName = User.Identity.Name;

                    var request = new ImageRequest(stream, "image/jpeg", null);

                    var photoUniqueName = string.Format("avatar_{0}", userName.ToLower());

                    var photoList = ImageManager.Provider.SaveImageForAllSizes(request, userName, photoUniqueName, false);

                    var photoId = photoList.First(x => x.ResizeName.ToLower().Contains("medium"));

                    photoId.Url = _userProfileService.SetProfilePicture(userName, photoId.ResizeName, photoUniqueName);

                    return Request.CreateResponse(HttpStatusCode.OK, new { photoId = photoId.Url } );
                });

            return t;

        }
       
    }
}
