﻿using System.Threading.Tasks;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;
using App.Core.Extensions;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {

        [ModelStateValidator]
        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            IdentityResult result = await UserManager.AddPasswordAsync(User.GetUserID(), model.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            return errorResult ?? Ok();
        }      

    }
}
