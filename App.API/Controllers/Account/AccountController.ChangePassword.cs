﻿using System.Threading.Tasks;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;
using App.Core.Extensions;
using Microsoft.AspNet.Identity;

namespace App.API.Controllers.Account
{
    [Authorize]
    public partial class AccountController
    {
        // POST api/Account/ChangePassword
        [ModelStateValidator]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            // Cannot change passwords for test users
            // Remove following lines for real usage
            if (User.IsInRole("Administrator") || User.Identity.GetUserName() == "user")
            {
                ModelState.AddModelError("Unable to change the password", "Cannot change the admin password in this demo app. Remove lines in ChangePassword (AccountController) action for real usage");
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.GetUserID(), model.OldPassword,
                model.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

    }
}
