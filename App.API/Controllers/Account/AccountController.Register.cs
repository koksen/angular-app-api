﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using App.API.Filters;
using App.API.Models.Account;
using App.Core.Enums;
using App.Core.Models;
using Microsoft.AspNet.Identity;
using Omu.ValueInjecter;

namespace App.API.Controllers.Account
{
   
    public partial class AccountController
    {
        // POST api/Account/Register
        [AllowAnonymous]
        [ModelStateValidator]
        [HttpPost]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {

            var user = new User
            {
				EmailConfirmed = false,
                TwoFactorEnabled = true
                
            }.InjectFrom(model) as User;

            IdentityResult identityResult = await UserManager.CreateAsync(user, model.Password);
            var actionResult = GetErrorResult(identityResult);

            if (actionResult != null)
            {
                return actionResult;
            }

            var justCreatedUser = await UserManager.FindByNameAsync(model.UserName);

            var roleResult = await UserManager.AddToRoleAsync(justCreatedUser.Id, "User");
            var claimResult = await UserManager.AddClaimAsync(justCreatedUser.Id, new Claim(ClaimTypes.Role, "User"));

            if (!roleResult.Succeeded || !claimResult.Succeeded)
            {
                return BadRequest();
            }

            if (UserManager.EmailService != null)
            {
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                var dict = new Dictionary<string, object> {{"userid", user.Id}, {"code", HttpUtility.UrlEncode(code)}};
                var callbackUrl = GetCallbackUrl(AccountEvent.ConfirmEmail, dict);
                var n = new AccountNotificationModel
                {
                    Code = code,
                    Url = callbackUrl,
                    UserId = justCreatedUser.Id,
                    Email = justCreatedUser.Email,
                    DisplayName = justCreatedUser.UserName
                };
                
                var body =
                    string.Format(
                        "<html><body><p>Hello <b>{0}</b>,<p>Your account has been successfully created, but you still need one more step.<br />Click <a href=\"{1}\">here</a> to verify your account.</p></body></html>",
                        n.DisplayName, n.Url);

                await UserManager.SendEmailAsync(user.Id, "Account confirmation", body);
            }

            if (UserManager.SmsService != null)
            {
                var codeSms = await UserManager.GenerateChangePhoneNumberTokenAsync(user.Id, user.PhoneNumber);
                var message = new IdentityMessage
                {
                    Destination = model.PhoneNumber,
                    Body = "Your security code is: " + codeSms
                };
                // Send token
                await UserManager.SmsService.SendAsync(message);
            }

            return Ok();
            
        }

        [ModelStateValidator]
        // POST api/Account/RegisterExternal
        [HttpPost]
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            var user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                EmailConfirmed = true
            };

            user.Logins.Add(new UserLogin
            {
                LoginProvider = externalLogin.LoginProvider,
                ProviderKey = externalLogin.ProviderKey,
                UserId = user.Id
            });

            IdentityResult identityResult = await UserManager.CreateAsync(user);

            var actionResult = GetErrorResult(identityResult);

            if (actionResult != null)
            {
                return actionResult;
            }

            User justCreatedUser = await UserManager.FindByNameAsync(model.UserName);

            IdentityResult roleResult = await UserManager.AddToRoleAsync(justCreatedUser.Id, "User");

            IHttpActionResult addRoleResult = GetErrorResult(roleResult);

            return addRoleResult ?? Ok();
        }

    }
}
