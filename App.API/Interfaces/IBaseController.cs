﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace App.API.Interfaces
{
    public interface IBaseController<in T, TViewModel>
    {
        IQueryable<TViewModel> GetAll();
        IHttpActionResult Get(int id);
        IHttpActionResult Post(TViewModel entity);
        IHttpActionResult Put(int id, TViewModel entity);
        IHttpActionResult Delete(int id);
        IHttpActionResult Delete(List<int> ids);

    }
}
