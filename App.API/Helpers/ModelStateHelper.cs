﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace App.API.Helpers
{
    public static class ModelStateHelper
    {
        public static IEnumerable<KeyValuePair<string, string[]>> Errors(this ModelStateDictionary modelState)
        {
            if (modelState.IsValid) return null;

            var errors = modelState.ToDictionary(kvp => kvp.Key.Replace("model.",""), kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray())
                .Where(m => m.Value.Any());

            return errors;
        }
    }
}
