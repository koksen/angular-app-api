﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace App.API.Filters
{
    /// <summary>
  /// Defines a global handler for unhandled exceptions.
  /// </summary>
  public class GlobalExceptionHandler : ExceptionHandler
  {
    /// <summary>
    /// Serializes exception for client-side processing.
    /// </summary>
    /// <param name="context">The exception handler context.</param>
    /// <remarks>
    /// see http://aspnetwebstack.codeplex.com/wikipage?title=Global%20Error%20Handling
    /// </remarks>
    public override void Handle(ExceptionHandlerContext context)
    {
      var requestContext = context.RequestContext;
      var config = requestContext.Configuration;

      context.Result = new ErrorResult(
        context.Exception,
        requestContext != null && requestContext.IncludeErrorDetail,
        config.Services.GetContentNegotiator(),
        context.Request,
        config.Formatters);
    }

    /// <summary>
    /// An implementation of IHttpActionResult interface.
    /// </summary>
    private class ErrorResult : ExceptionResult
    {
      public ErrorResult(
        Exception exception,
        bool includeErrorDetail,
        IContentNegotiator negotiator,
        HttpRequestMessage request,
        IEnumerable<MediaTypeFormatter> formatters) :
        base(exception, includeErrorDetail, negotiator, request, formatters)
      {
      }

      /// <summary>
      /// Creates an HttpResponseMessage instance asynchronously.
      /// </summary>
      /// <param name="cancellationToken"></param>
      /// <returns></returns>
      public override Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
      {
        var content = new HttpError(Exception, IncludeErrorDetail)
        {
            {"ErrorID", Exception.Data["NesterovskyBros:id"] as long?}
        };

          var result = 
          ContentNegotiator.Negotiate(typeof(HttpError), Request, Formatters);
        var message = new HttpResponseMessage
        {
          RequestMessage = Request,
          StatusCode = result == null ?
            HttpStatusCode.NotAcceptable : HttpStatusCode.InternalServerError
        };

        if (result != null)
        {
          try
          {
            message.Content = new ObjectContent<HttpError>(
              content, 
              result.Formatter, 
              result.MediaType);
          }
          catch
          {
            message.Dispose();

            throw;
          }
        }

        return Task.FromResult(message);
      }
    }
  }
}