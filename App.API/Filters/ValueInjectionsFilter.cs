﻿using System;
using System.Linq;
using App.Core.Filters;

namespace App.API.Filters
{

    public class FilterId : CustomLoopValueInjection
    {

        protected override bool TypesMatch(Type sourceType, Type targetType)
        {
            var isInjectable = SourceAttributes.OfType<NotInjectedAttribute>().Any(); 
            return !isInjectable;

        }
       
    }
}
