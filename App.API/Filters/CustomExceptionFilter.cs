﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace App.API.Filters
{
    /// <summary>
  /// Defines a global handler for unhandled exceptions.
  /// </summary>
    public class MyCustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
        }
    }
}