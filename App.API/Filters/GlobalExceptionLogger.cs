﻿using System;
using System.Diagnostics;
using System.Web.Http.ExceptionHandling;

namespace App.API.Filters
{
    /// <summary>
    /// Defines a global logger for unhandled exceptions.
    /// </summary>
    public class GlobalExceptionLogger : ExceptionLogger
    {
    /// <summary>
    /// Writes log record to the database synchronously.
    /// </summary>
    /// <param name="context">The exception logger context.</param>
    public override void Log(ExceptionLoggerContext context)
    {
        try
        {
        var request = context.Request;
        var exception = context.Exception;
        var id = LogError(
                request.RequestUri.ToString(),
                context.RequestContext == null ? 
                null : context.RequestContext.Principal.Identity.Name, 
                request.ToString(), 
                exception.Message, 
                exception.StackTrace);

        exception.Data["NesterovskyBros:id"] = id;
        }
        catch
        {
        // logger shouldn't throw an exception!!!
        }
    }

    private static long LogError(
        string address, 
        string userid, 
        string request, 
        string message, 
        string stackTrace)
        {
            var id = DateTime.Now.Ticks;

            Trace.TraceError(
            "ID: {5}\nRequested address: {0}\nUser name: {1}\nMesage: {3}\nRaw request: {2}\nStack trace: {4}\n\n",
            address, 
            userid, 
            request, 
            message, 
            stackTrace,
            id);

            return id;
        }
    }
}