﻿using System;
using System.ComponentModel;
using Omu.ValueInjecter;

namespace App.API.Filters
{
    public class CustomLoopValueInjection : CustomizableValueInjection
    {
        protected Type TargetPropType;
        protected Type SourcePropType;

        protected virtual bool UseSourceProp(string sourcePropName)
        {
            return true;
        }

        protected virtual string TargetPropName(string sourcePropName)
        {
            return sourcePropName;
        }

        protected AttributeCollection SourceAttributes;
        protected AttributeCollection TargetAttributes;

        protected override void Inject(object source, object target)
        {
            var sourceProps = source.GetProps();
            for (var i = 0; i < sourceProps.Count; i++)
            {
                var s = sourceProps[i];
                if (!UseSourceProp(s.Name)) continue;

                var t = target.GetProps().GetByName(SearchTargetName(TargetPropName(s.Name)));
                if (t == null) continue;

                SourceAttributes = s.Attributes;
                TargetAttributes = t.Attributes;

                if (!TypesMatch(s.PropertyType, t.PropertyType)) continue;
                TargetPropType = t.PropertyType;
                SourcePropType = s.PropertyType;
                var value = s.GetValue(source);
                if (AllowSetValue(value))
                    t.SetValue(target, SetValue(value));
            }
        }
    }
}
