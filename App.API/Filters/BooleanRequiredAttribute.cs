﻿using System.ComponentModel.DataAnnotations;

namespace App.API.Filters
{
    public class BooleanRequiredAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value != null && (bool)value == true;
        }
    }
}