﻿
namespace App.API.Filters
{
    
    public class ModelStateError
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }

    }
}
