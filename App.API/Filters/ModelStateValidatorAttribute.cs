using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using App.API.Helpers;

namespace App.API.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class ModelStateValidatorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;
            if (modelState.IsValid)
                return;

            var errors = modelState.Errors();
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Errors = errors } );

        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {

            var controller = actionExecutedContext.ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            //Tracing.Information(String.Format("Controller: {0} - Action {1} ", controller, action));
        }

    }
}