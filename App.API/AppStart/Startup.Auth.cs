﻿using System;
using App.API.Providers;
using App.Infrastructure.Data;
using App.Infrastructure.Extensions;
using App.Infrastructure.IdentityManager;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace App.API.AppStart
{
    public partial class Startup
    {

        private static void SetupAuth(IAppBuilder app)
        {
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true,
                AccessTokenFormat = new TicketDataFormat(app.CreateDataProtector(
                                    typeof(OAuthAuthorizationServerMiddleware).Namespace,
                                    "Access_Token", "v1")),
                RefreshTokenFormat = new TicketDataFormat(app.CreateDataProtector(
                                    typeof(OAuthAuthorizationServerMiddleware).Namespace,
                                    "Refresh_Token", "v1")),
                AccessTokenProvider = new AuthenticationTokenProvider(),
                RefreshTokenProvider = new AuthenticationTokenProvider()
            };

            OAuthBearerOptions = new OAuthBearerAuthenticationOptions
            {
                AccessTokenFormat = OAuthOptions.AccessTokenFormat,
                AccessTokenProvider = OAuthOptions.AccessTokenProvider,
                AuthenticationMode = OAuthOptions.AuthenticationMode,
                AuthenticationType = OAuthOptions.AuthenticationType,
                Description = OAuthOptions.Description,
                SystemClock = OAuthOptions.SystemClock,
                Provider = new OAuthBearerTokenProvider()
            };

            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(DataContext.Create);
            app.CreatePerOwinContext<UserProfileManager>(UserProfileManager.Create);
            app.UseDbContextFactory(DataContext.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enable the application to use bearer tokens to authenticate users
            // Enabling 3 components:
            // 1. Authorization Server middleware. For creating the bearer tokens
            // 2. Application bearer token middleware. Will atuthenticate every request with Authorization : Bearer header
            // 3. External bearer token middleware. For external providers
            //app.UseOAuthBearerTokens(OAuthOptions, OAuthOptions.AuthenticationType);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

           

        }
    }
}
