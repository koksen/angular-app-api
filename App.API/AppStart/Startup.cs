﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using App.API.AppStart;
using App.DependencyResolution;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace App.API.AppStart
{
    public partial class Startup
    {
        public static string PublicClientId { get; private set; }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        private static IDependencyResolver _resolver;

        //Enable OWIN Bearer Token Middleware	
        static Startup()
        {
            PublicClientId = "self";
        }
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            var container = SimpleInjectorStartup.Start(app);

            _resolver = new SimpleInjectorWebApiDependencyResolver(container);
            config.DependencyResolver = _resolver;

            app.UseCors(CorsOptions.AllowAll);  

            SetupAuth(app);
            SetupSignalR(app, container);
            SetupWebApi(config);
            SetupErrorHandling(config);
            SetupBinders();
            
            app.UseWebApi(config);

            config.EnsureInitialized();

            container.Verify();
        }

    }
}