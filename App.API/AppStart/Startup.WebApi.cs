﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using App.API.Filters;
using App.Core.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace App.API.AppStart
{
    public partial class Startup
    {

        private static void SetupWebApi(HttpConfiguration config)
        {
            //config.EnableQuerySupport();

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{id}",
                 defaults: new { id = RouteParameter.Optional }
             );

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            //// Remove default XML handler
            var matches = config.Formatters
                                .Where(f => f.SupportedMediaTypes.Any(m => m.MediaType.ToString(CultureInfo.InvariantCulture) == "application/xml" ||
                                                                           m.MediaType.ToString(CultureInfo.InvariantCulture) == "text/xml"))
                                .ToList();
            foreach (var match in matches)
                config.Formatters.Remove(match);

            // Need to disable validation since it currently fails on DbGeography
            //config.Services.Clear(typeof(System.Web.Http.Validation.ModelValidatorProvider));
            config.Filters.Add(new ModelStateValidatorAttribute());
            // Create Json.Net formatter serializing DateTime using the ISO 8601 format
            var Settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateParseHandling = DateParseHandling.DateTimeOffset,
                DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Converters = new List<JsonConverter>
                    {
                        new IsoDateTimeConverter(),
                        new DbGeographyConverter(),
                        new DateTimeOffsetConverter2()
                    }
            };

            //All routes forbidden by default
            config.Filters.Add(new AuthorizeAttribute());

            config.Formatters.JsonFormatter.SerializerSettings = Settings;
          
        }
    }
}
