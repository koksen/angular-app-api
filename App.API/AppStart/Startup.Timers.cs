﻿using App.API.Timers;

namespace App.API.AppStart
{
    public partial class Startup
    {
        private BackgroundServerTimeTimer bstt;
        private void SetupTimers()
        {
            bstt = new BackgroundServerTimeTimer();
        }
    }
}
