﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using App.API.Filters;

namespace App.API.AppStart
{
    public partial class Startup
    {
        private static void SetupErrorHandling(HttpConfiguration config)
        {
            config.Services.Add(typeof(IExceptionLogger), new GlobalExceptionLogger());
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            var customErrors =
              (CustomErrorsSection)ConfigurationManager.GetSection("system.web/customErrors");

            if (customErrors == null) return;
            switch (customErrors.Mode)
            {
                case CustomErrorsMode.RemoteOnly:
                {
                    config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;
                    break;
                }
                case CustomErrorsMode.On:
                {
                    config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Never;
                    break;
                }
                case CustomErrorsMode.Off:
                {
                    config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
                    break;
                }
                default:
                {
                    config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Default;
                    break;
                }
            }

            //TaskScheduler.UnobservedTaskException += (sender, e) =>
            //{
            //    try
            //    {
            //        // Write all unobserved exceptions
            //        ReportError(e.Exception);
            //    }
            //    catch
            //    {
            //        // Swallow!
            //    }
            //    finally
            //    {
            //        e.SetObserved();
            //    }
            //};
        }

        private static void ReportError(Exception e)
        {

        }
    }
}
