﻿using System.Web.Http;
using System.Web.Http.Validation;
using App.API.Helpers;

namespace App.API.AppStart
{
    public partial class Startup
    {
        private static void SetupBinders()
        {
            var config = GlobalConfiguration.Configuration;
            config.Services.Replace(typeof(IBodyModelValidator), new CustomBodyModelValidator());
        }

    }
}
