﻿using Owin;

namespace AngularJSAuth.API.AppStart
{
    public partial class Startup
    {

        private static void SetupExternalLogins(IAppBuilder app)
        {
            // Uncomment the following lines to enable logging in with third party login providers
            app.UseMicrosoftAccountAuthentication(
                clientId: "",
                clientSecret: "");

            app.UseTwitterAuthentication(
                consumerKey: "",
                consumerSecret: "");

            app.UseFacebookAuthentication(
                appId: "",
                appSecret: "");

            app.UseGoogleAuthentication();
        }
    }
}
