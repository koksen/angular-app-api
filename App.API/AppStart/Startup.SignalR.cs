﻿using App.API.Helpers;
using App.DependencyResolution;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Configuration;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.AspNet.SignalR.Transports;
using Newtonsoft.Json;
using Owin;
using Container = SimpleInjector.Container;

namespace App.API.AppStart
{
    public partial class Startup
    {
        private static void SetupSignalR(IAppBuilder app, Container container)
        {
            var resolver = new SignalRSimpleInjectorDependencyResolver(container);
            var connectionManager = resolver.Resolve<IConnectionManager>();
            var heartbeat = resolver.Resolve<ITransportHeartbeat>();
            var hubPipeline = resolver.Resolve<IHubPipeline>();
            var configuration = resolver.Resolve<IConfigurationManager>();

            //container.RegisterSingle(connectionManager);

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new SignalRContractResolver()
            };
            var serializer = JsonSerializer.Create(serializerSettings);

            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => new SimpleInjectorHubActivator(container));

            var config = new HubConfiguration
            {
                EnableDetailedErrors = true
                ,EnableJSONP = true
                //,Resolver = resolver // this cause the server -> client communication stop working
            };

            app.MapSignalR(config);
            //var monitor = new PresenceMonitor(container, connectionManager, heartbeat);
            //monitor.Start();
        }
    }
}
