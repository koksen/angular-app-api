﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Hosting;
using App.API.Hubs;
using Microsoft.AspNet.SignalR;

namespace App.API.Timers
{
    public class BackgroundServerTimeTimer : IRegisteredObject //SignalRBase<NotificationHub>, 
    {
        private readonly Timer taskTimer;
        private readonly IHubContext _hub;
        public BackgroundServerTimeTimer()
        {
            HostingEnvironment.RegisterObject(this);
            _hub = GlobalHost.ConnectionManager.GetHubContext<ClientPushHub>();
            taskTimer = new Timer(OnTimerElapsed, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        private void OnTimerElapsed(object sender)
        {
            _hub.Clients.All.serverTime(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
        }

        public void Stop(bool immediate)
        {
            taskTimer.Dispose();
            HostingEnvironment.UnregisterObject(this);
        }
    }
}