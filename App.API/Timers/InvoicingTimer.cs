﻿using System;
using System.Threading;
using System.Web.Hosting;

namespace App.API.Timers
{
    public class InvoicingTimer : IRegisteredObject
    {
        private readonly Timer taskTimer;
        //private readonly IHubContext hub;

        public InvoicingTimer()
        {
            HostingEnvironment.RegisterObject(this);

            //hub = GlobalHost.ConnectionManager.GetHubContext<ClientPushHub>();
            taskTimer = new Timer(OnTimerElapsed, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5));
        }

        private static void OnTimerElapsed(object sender)
        {
            //hub.Clients.All.serverTime(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
        }

        public void Stop(bool immediate)
        {
            taskTimer.Dispose();
            HostingEnvironment.UnregisterObject(this);
        }
    }
}