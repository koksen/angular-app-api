﻿using System.Diagnostics;
using System.Threading;
using System.Web.Hosting;
using App.API.Hubs;
using Microsoft.AspNet.SignalR;

namespace App.API.Timers
{
    public class BackgroundPerformanceDataTimer : IRegisteredObject
    {
        private readonly PerformanceCounter processorCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private readonly Timer taskTimer;
        private readonly IHubContext hub;

        public BackgroundPerformanceDataTimer()
        {
            HostingEnvironment.RegisterObject(this);

            hub = GlobalHost.ConnectionManager.GetHubContext<PerformanceDataHub>();
            taskTimer = new Timer(OnTimerElapsed, null, 1000, 1000);
        }

        private void OnTimerElapsed(object sender)
        {
            var perfValue = processorCounter.NextValue().ToString("0.0");
            hub.Clients.All.newCpuDataValue(perfValue);
        }

        public void Stop(bool immediate)
        {
            taskTimer.Dispose();

            HostingEnvironment.UnregisterObject(this);
        }
    }
}