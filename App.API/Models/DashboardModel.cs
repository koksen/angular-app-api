﻿using System.Collections.Generic;

namespace App.API.Models
{
    public class DashboardModel
    {
        public AnalyticsInfo Analytics { get; set; }
        public IEnumerable<ChartInfo> ChartSeries { get; set; }

    }
}
