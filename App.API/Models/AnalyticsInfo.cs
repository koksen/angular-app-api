﻿namespace App.API.Models
{
    public class AnalyticsInfo
    {
        public decimal Growth { get; set; }
        public int NewUsers { get; set; }
        public decimal Profit { get; set; }
        public decimal Sales { get; set; }

    }
}
