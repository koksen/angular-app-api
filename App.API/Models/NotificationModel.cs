﻿using System.Collections.Generic;

namespace App.API.Models
{
    public class NotificationModel
    {
        public AnalyticsInfo Analytics { get; set; }
        public IEnumerable<ChartInfo> ChartSeries { get; set; }

    }
}
