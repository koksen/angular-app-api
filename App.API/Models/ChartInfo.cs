﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App.API.Models
{
    public class ChartInfo
    {
        public string Name { get; set; }
        public int[] Data { get; set; }
        public bool ConnectNulls { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ChartType Type { get; set; }

        public ChartInfo()
        {
            Type = ChartType.line;
        }
    }
}