﻿using System.ComponentModel;

namespace App.API.Models
{
    public enum ChartType
    {

        [Description("Line")]
        line,
        
        [Description("Smooth line")]
        spline,
        
        [Description("Area")]
        area,
        
        [Description("Smooth area")]
        areaspline,
        
        [Description("Column")]
        column,
        
        [Description("Bar")]
        bar,
        
        [Description("Scatter")]
        scatter
    }
}