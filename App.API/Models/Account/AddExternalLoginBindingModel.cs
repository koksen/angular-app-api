﻿using System.ComponentModel.DataAnnotations;

namespace App.API.Models.Account
{
    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }
}