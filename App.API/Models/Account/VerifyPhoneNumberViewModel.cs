﻿
using System.ComponentModel.DataAnnotations;

namespace App.API.Models.Account
{
    // Models returned by AccountController actions.

    public class VerifyPhoneNumberViewModel 
    {
        public string PhoneNumber { get; set; }

        [Required]
        public string UserName { get; set; }
        
        [Required]
        public string Code { get; set; }
    }

  }
