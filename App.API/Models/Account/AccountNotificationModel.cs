﻿namespace App.API.Models.Account
{
    // Models used as parameters to AccountController actions.

    public class AccountNotificationModel
    {
        public int UserId { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
