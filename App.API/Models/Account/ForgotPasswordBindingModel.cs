using System.ComponentModel.DataAnnotations;

namespace App.API.Models.Account
{
    public class ForgotPasswordBindingModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}