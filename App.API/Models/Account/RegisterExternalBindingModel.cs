﻿using System.ComponentModel.DataAnnotations;

namespace App.API.Models.Account
{
    public class RegisterExternalBindingModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(200)]
        [EmailAddress]
        public string Email { get; set; }
    }
}