﻿using System.ComponentModel.DataAnnotations;

namespace App.API.Models.Account
{
    public class ChangePhoneNumberBindingModel
    {
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Current phone number")]
        public string OldPhoneNumber { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "New phone number")]
        public string NewPhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Confirm new phone number")]
        [Compare("NewPhoneNumber", ErrorMessage = "The new phone number and confirmation phone number do not match.")]
        public string ConfirmPhoneNumber { get; set; }
    }
}