﻿using Microsoft.AspNet.SignalR;
using SimpleInjector;

namespace App.API.Hubs
{
    public class BaseHub : Hub
    {

        internal Scope Scope { get; set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing && Scope != null)
            {
                Scope.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}