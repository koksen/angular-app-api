﻿using Microsoft.AspNet.SignalR.Hubs;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace App.API.Hubs
{
    public class HubActivator : IHubActivator
    {
        private readonly Container _container;

        public HubActivator(Container container)
        {
            _container = container;
        }

        public IHub Create(HubDescriptor descriptor)
        {
            var dispose = true;
            Scope scope = null;
            try
            {
                scope = _container.BeginExecutionContextScope();

                var hub = _container.GetInstance(descriptor.HubType);

                var baseHub = hub as BaseHub;
                if (baseHub != null)
                {
                    baseHub.Scope = scope;
                    dispose = false;
                }

                return (IHub)hub;
            }
            finally
            {
                if (dispose && scope != null)
                {
                    scope.Dispose();
                }
            }
        }

    }
}