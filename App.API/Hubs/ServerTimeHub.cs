﻿using System;
using System.Globalization;
using App.Core.Interfaces.Services;
using Microsoft.AspNet.SignalR;

namespace App.API.Hubs
{
    public class ServerTimeHub : Hub
    {
        private IUserProfileService _userProfileService;
        public ServerTimeHub(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        public string GetServerTime()
        {
            var offset = Infrastructure.Helpers.Common.GetTimeZoneOffsetByIP();
            return DateTime.UtcNow.AddHours(offset).ToString(CultureInfo.InvariantCulture);
        }
    }
}