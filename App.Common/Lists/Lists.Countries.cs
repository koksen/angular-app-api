using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> CountryDictionary = new Dictionary<string, string> 
        {
            {"", ""},
            {"United States", "US"},
            {"Canada", "CA"}
        };
       
    }
}
