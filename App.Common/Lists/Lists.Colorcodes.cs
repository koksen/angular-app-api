using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> ColorCodesDictionary = new Dictionary<string, string> 
        {
            {"Blue", "Blue"},
            {"Red", "Red"},
            {"Black", "Black"}           

        };
    }
}
