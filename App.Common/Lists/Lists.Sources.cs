using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {

        public static readonly IDictionary<string, string> SourcesDictionary = new Dictionary<string, string> 
        {
            {"Google", "Toyota"},
            {"Maps", "Maps"},
            {"PPC", "PPC"},
            {"Yellow Book", "YellowBook"},
            {"Direct Mail", "DirectMail"},
            {"Internet", "Internet"},
            {"411", "411"},
            {"Referral", "Referral"},
            {"Account", "Account"}
        };

    }
}
