using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> TaxtypeDictionary = new Dictionary<string, string> 
        {
            {"Company Parts", "CompanyParts"},
            {"Tech Parts", "TechParts"},
            {"Part Of Total", "PartOfTotal"}           

        };
    }
}
