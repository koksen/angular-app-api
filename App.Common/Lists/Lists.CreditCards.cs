using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> CreditCardDictionary = new Dictionary<string, string> 
        {
            {"", ""},
            {"Visa", "V"},
            {"Mastercard", "M"}
        };
    }
}
