using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> PrivacyLevel = new Dictionary<string, string> 
        {
            {"Anybody can see my profile", "ALL"},
            {"Only members can see my profile", "MEMBERS"},
            {"Only partners can see my profile", "PARTNERS"}
        };
    }
}
