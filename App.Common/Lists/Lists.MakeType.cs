using System.Collections.Generic;

namespace App.Common.Lists
{
    public static partial class Lists
    {
        public static readonly IDictionary<string, string> MaketypeDictionary = new Dictionary<string, string> 
        {
            {"Toyota", "Toyota"},
            {"Fiat", "Fiat"},
            {"Farrari", "Farrari"},
            {"Audi", "Audi"},
            {"Ford", "Ford"},
            {"Other", "Other"}

        };
    }
}
