﻿
namespace App.Common.Utils
{
    public static class CacheConstants
    {
        public const string CachedRolesSourcesList = "CachedRolesSourcesList";
        public const string CachedAdSourcesList = "CachedAdSourcesList";
        public const string CachedPaymentTypesList = "CachedPaymentTypesList";
        public const string CachedCancellationReasonList = "CachedCancellationReasonList";
        public const string CachedCompanySourcesList = "CachedCompanySourcesList";
        public const string CachedServiceAreasList = "CachedServiceAreasList";
        public const string CachedServiceLocalesList = "CachedServiceLocalesList";
        public const string CachedServiceTypesList = "CachedServiceTypesList";
        public const string CurrentUserTimeZoneOffset = "CurrentUserTimeZoneOffset";
        public const string CachedMakeTypeList = "CachedMakeTypeList";
        public const string CachedReportTypes = "CachedReportTypes";
    }
}