﻿using System;
using System.Data.Entity.Spatial;
using System.Globalization;

namespace App.Common.Utils
{
    
    public static class GeographyHelpers
    {
        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                     "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.FromText(text, 4326);
        }

        public static double MetersToMiles(double? meters, short dec = 0)
        {
            if (meters == null)
                return 0F;

            var miles = meters.Value*0.000621371192;
            return dec > 0 ? Math.Round(miles, dec) : miles;
        }

        public static double MilesToMeters(double? miles)
        {
            if (miles == null)
                return 0;

            return miles.Value * 1609.344;
        }
    }


}
