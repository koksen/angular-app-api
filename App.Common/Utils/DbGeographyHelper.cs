﻿using System;
using System.Data.Entity.Spatial;
using System.Globalization;

namespace TechsApp.Web.Helpers
{
    public static class DbGeographyHelper
    {
        public static DbGeography Parse(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return null;
            }
            string[] latLongStr = value.Split(',');
            if (latLongStr.Length != 2)
                return null;

            return GetCoord(latLongStr[0], latLongStr[1]);

        }

        public static DbGeography Build(double latitude, double longitude)
        {
            if (latitude == 0 || longitude ==0)
            {
                return null;
            }

            return GetCoord(latitude.ToString(CultureInfo.InvariantCulture), longitude.ToString(CultureInfo.InvariantCulture));

        }

        private static DbGeography GetCoord(string latitude, string longitude)
        {
            //Are we supposed to populate ModelState with errors here if we can't conver the value to a point?
            string point = string.Format("POINT ({0} {1})", longitude, latitude);
            //4326 format puts LONGITUDE first then LATITUDE
            var result = DbGeography.FromText(point, 4326);
            return result;
        }
    }
}
