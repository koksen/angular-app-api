﻿#region credits
// ***********************************************************************
// Assembly	: SmartApi.Common
// Author	: Victor Cardins
// Created	: 03-23-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace App.Common.Crypto
{
    #region

    

    #endregion

    public static class CryptoHelper
    {
        internal const char PasswordHashingIterationCountSeparator = '.';
        internal static Func<int> GetCurrentYear = () => DateTime.Now.Year;

        public static string Md5(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] data = md5.ComputeHash(Encoding.Default.GetBytes(input));
            var sbString = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
                sbString.Append(data[i].ToString("x2"));
            return sbString.ToString();
        }

        public static string Hash(string value)
        {
            return Crypto.Hash(value);
        }

        public static string GenerateSalt()
        {
            return Crypto.GenerateSalt();
        }

        public static string GenerateRandonKey()
        {
            using (var rngProvider = new RNGCryptoServiceProvider())
            {
                byte[] key = new byte[32]; //256 bites

                rngProvider.GetBytes(key);

                return Convert.ToBase64String(key);
            }
        }

        public static string HashPassword(string password, int count)
        {
            if (count <= 0)
            {
                count = GetIterationsFromYear(GetCurrentYear());
            }
            var result = Crypto.HashPassword(password, count);
            return EncodeIterations(count) + PasswordHashingIterationCountSeparator + result;
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            if (hashedPassword.Contains(PasswordHashingIterationCountSeparator))
            {
                var parts = hashedPassword.Split(PasswordHashingIterationCountSeparator);
                if (parts.Length != 2) return false;

                int count = DecodeIterations(parts[0]);
                if (count <= 0) return false;

                hashedPassword = parts[1];
                
                return Crypto.VerifyHashedPassword(hashedPassword, password, count);
            }
            else
            {
                return Crypto.VerifyHashedPassword(hashedPassword, password);
            }
        }

        public static string EncodeIterations(int count)
        {
            return count.ToString("X");
        }

        public static int DecodeIterations(string prefix)
        {
            int val;
            if (Int32.TryParse(prefix, System.Globalization.NumberStyles.HexNumber, null, out val))
            {
                return val;
            }
            return -1;
        }

        // from OWASP : https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet
        const int StartYear = 2000;
        const int StartCount = 1000;
        internal static int GetIterationsFromYear(int year)
        {
            if (year > StartYear)
            {
                var diff = (year - StartYear) / 2;
                var mul = (int)Math.Pow(2, diff);
                int count = StartCount * mul;
                // if we go negative, then we wrapped (expected in year ~2044). 
                // Int32.Max is best we can do at this point
                if (count < 0) count = Int32.MaxValue;
                return count;
            }
            return StartCount;
        }

        public static string HashPasswordSHA256(string password, string privateSharedKey)
        {
            using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(privateSharedKey)))
            {
                var passwordBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                return Convert.ToBase64String(passwordBytes);
            }
        }
    }
}
