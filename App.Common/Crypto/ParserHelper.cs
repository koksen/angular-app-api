﻿#region credits
// ***********************************************************************
// Assembly	: SmartApi.Common
// Author	: Victor Cardins
// Created	: 03-23-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace App.Common.Crypto
{
    #region

    

    #endregion

    public static class ParserHelper
    {

        public static IEnumerable<string> ParseHashtag(string message, bool removeHashtag = true)
        {
            var hashTag = new Regex(@"#\w+");

            while (hashTag.Match(message).Success)
            {
                string value = hashTag.Match(message).Groups[0].Value;
                message = message.Replace(value, "");
                if (removeHashtag)
                    value = value.Replace("#", string.Empty);

                yield return value;
            }
        }

    }
}
