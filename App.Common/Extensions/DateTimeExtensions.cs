﻿#region credits
// ***********************************************************************
// Assembly	: SmartApi
// Author	: Victor Cardins
// Created	: 03-17-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System;

namespace App.Common.Extensions
{
    #region

    

    #endregion

    public static class DateTimeExtensions
    {
        public static DateTime ToLocalTimezone(this DateTime source, double clientTimeZoneOffset)
        {
            return source.AddHours(clientTimeZoneOffset);
        }

        public static string ToLocalTimezone(this DateTime source, double clientTimeZoneOffset, string format)
        {
            return source.ToLocalTimezone(clientTimeZoneOffset).ToString(format);
        }

        public static string ToLocalTimezone(this DateTime? source, double clientTimeZoneOffset, string format)
        {
            return !source.HasValue ? string.Empty : source.Value.ToLocalTimezone(clientTimeZoneOffset).ToString(format);
        }

        public static DateTime UtcToLocal(this DateTime source, TimeZoneInfo localTimeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(source, localTimeZone);
        }

        public static DateTime LocalToUtc(this DateTime source, TimeZoneInfo localTimeZone)
        {
            source = DateTime.SpecifyKind(source, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(source, localTimeZone);
        }
       
    }
}