﻿using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The photo resize element.
    /// </summary>
    public class ApplePushElement : BaseMessagingServiceElement, IApplePushSettings
    {
       
        [ConfigurationProperty("certificateFile")]
        public string CertificateFile
        {
            get { return (string)base["certificateFile"]; }
            set { base["certificateFile"] = value; }
        }

        [ConfigurationProperty("certificatePassword")]
        public string CertificatePassword
        {
            get { return (string)base["certificatePassword"]; }
            set { base["certificatePassword"] = value; }
        }

        [ConfigurationProperty("defaultSound")]
        public string DefaultSound  {
            get { return (string)base["defaultSound"]; }
            set { base["defaultSound"] = value; }
        }
    }
}