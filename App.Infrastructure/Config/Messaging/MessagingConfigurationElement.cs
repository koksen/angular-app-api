using System.Configuration;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The photo section.
    /// </summary>
    public class MessagingConfigurationElement : ConfigurationElement
    {

        /// <summary>
        /// Gets the Apple push notification service settings.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("apple")]
        public ApplePushConfigurationElement Apple
        {
            get { return (ApplePushConfigurationElement)base["apple"]; }

        }
        /// <summary>
        /// Gets the Apple push notification service settings.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("google")]
        public GooglePushConfigurationElement Google
        {
            get { return (GooglePushConfigurationElement)base["google"]; }

        }

        /// <summary>
        /// Gets the Apple push notification service settings.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("parse")]
        public ParsePushConfigurationElement Parse
        {
            get { return (ParsePushConfigurationElement)base["parse"]; }

        }
        /// <summary>
        /// Gets the Twilio push notification service settings.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("twilio")]
        public TwilioSmsConfigurationElement Sms
        {
            get { return (TwilioSmsConfigurationElement)base["twilio"]; }

        }

        /// <summary>
        /// Gets the Twilio push notification service settings.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("sendgrid")]
        public SendGridMailerConfigurationElement Mailer
        {
            get { return (SendGridMailerConfigurationElement)base["sendgrid"]; }

        }

        //[ConfigurationProperty("defaultPushService")]
        //public string DefaultPushService
        //{
        //    get
        //    {
        //        return (string)base["defaultPushService"]; 
        //        //switch (base["defaultPushService"].ToString().ToLower())
        //        //{
        //        //    case "apple" : return MessagingService.PushApple;
        //        //    case "parse": return MessagingService.PushParse;
        //        //    case "google": return MessagingService.PushGoogle;                    
        //        //}
        //        //return MessagingService.PushParse;
        //    }

        //    set { base["defaultPushService"] = value; }
        //}
    }
}