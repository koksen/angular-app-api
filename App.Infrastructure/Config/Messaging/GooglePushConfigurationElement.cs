using System;
using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The photo section.
    /// </summary>
    public class GooglePushConfigurationElement : ConfigurationElement
    {

        /// <summary>
        /// The lock.
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// The _provider.
        /// </summary>
        private static IGooglePushSettings _current;

        /// <summary>
        /// Gets the web services.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("environments")]
        public GooglePushCollection Environments
        {
            get { return (GooglePushCollection)base["environments"]; }

        }

        /// <value>
        /// The base URL.
        /// </value>
        [ConfigurationProperty("default")]
        public string Default
        {
            get { return (string)base["default"]; }

            set { base["default"] = value; }
        }

        /// <value>
        /// The base URL.
        /// </value>
        public IGooglePushSettings Current
        {
            get
            {
                lock (Lock)
                {
                    var section = AppConfig.Instance.Messaging.Google;

                    _current = section.Environments[section.Default];

                    if (_current == null)
                    {
                        throw new Exception("Unable to load default context");
                    }
                }
                return _current;
            }

        }

    }
}