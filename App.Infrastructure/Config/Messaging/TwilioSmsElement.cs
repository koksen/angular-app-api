﻿using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The photo resize element.
    /// </summary>
    public class TwilioSmsElement : BaseMessagingServiceElement, ITwilioSmsSettings
    {

        [ConfigurationProperty("gateway")]
        public string Gateway
        {
            get { return (string)base["gateway"]; }
            set { base["gateway"] = value; }
        }

        [ConfigurationProperty("baseurl", IsRequired = true)]
        public string BaseUrl
        {
            get { return (string)base["baseurl"]; }
            set { base["baseurl"] = value; }
        }

        [ConfigurationProperty("accountsid", IsRequired = true)]
        public string AccountSid
        {
            get { return (string)base["accountsid"]; }
            set { base["accountsid"] = value; }
        }

        [ConfigurationProperty("authtoken", IsRequired = true)]
        public string AuthToken
        {
            get { return (string)base["authtoken"]; }
            set { base["authtoken"] = value; }
        }

        [ConfigurationProperty("sender", IsRequired = true)]
        public string Sender
        {
            get { return (string)base["sender"]; }
            set { base["sender"] = value; }
        }

        [ConfigurationProperty("fromnumber", IsRequired = true)]
        public string FromNumber
        {
            get { return (string)base["fromnumber"]; }
            set { base["fromnumber"] = value; }
        }

        [ConfigurationProperty("smsmaxlength", DefaultValue = 160)]
        public int SmsMaxLength
        {
            get { return (int)base["smsmaxlength"]; }
            set { base["smsmaxlength"] = value; }
        }
    }
}