using System;
using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The photo section.
    /// </summary>
    public class ApplePushConfigurationElement : ConfigurationElement
    {

        /// <summary>
        /// The lock.
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// The _provider.
        /// </summary>
        private static IApplePushSettings _current;

        /// <summary>
        /// Gets the web services.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("environments")]
        public ApplePushCollection Environments
        {
            get { return (ApplePushCollection)base["environments"]; }

        }

        /// <value>
        /// The base URL.
        /// </value>
        [ConfigurationProperty("default")]
        public string Default
        {
            get { return (string)base["default"]; }

            set { base["default"] = value; }
        }

        /// <value>
        /// The base URL.
        /// </value>
        public IApplePushSettings Current
        {
            get
            {
                lock (Lock)
                {
                    var section = AppConfig.Instance.Messaging.Apple;

                    _current = section.Environments[section.Default];

                    if (_current == null)
                    {
                        throw new Exception("Unable to load default context");
                    }
                }
                return _current;
            }

        }

    }
}