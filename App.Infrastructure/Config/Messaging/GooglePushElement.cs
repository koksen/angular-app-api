﻿using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{

    public class GooglePushElement : BaseMessagingServiceElement, IGooglePushSettings
    {

        [ConfigurationProperty("senderId")]
        public string SenderId
        {
            get { return (string)base["senderId"]; }
            set { base["senderId"] = value; }
        }

        [ConfigurationProperty("authToken")]
        public string AuthToken
        {
            get { return (string)base["authToken"]; }
            set { base["authToken"] = value; }
        }

        [ConfigurationProperty("applicationIdPackageName")]
        public string ApplicationIdPackageName
        {
            get { return (string)base["applicationIdPackageName"]; }
            set { base["applicationIdPackageName"] = value; }
        }
    }
}