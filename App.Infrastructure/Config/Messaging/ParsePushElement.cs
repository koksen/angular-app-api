﻿using System.Configuration;
using App.Core.Interfaces.Config.Messaging;

namespace App.Infrastructure.Config.Messaging
{
    /// <summary>
    /// The Parse element.
    /// </summary>
    public class ParsePushElement : BaseMessagingServiceElement, IParsePushSettings
    {

        [ConfigurationProperty("applicationId")]
        public string ApplicationId
        {
            get { return (string)base["applicationId"]; }
            set { base["applicationId"] = value; }
        }

        [ConfigurationProperty("dotNetKey")]
        public string DotNetKey
        {
            get { return (string)base["dotNetKey"]; }
            set { base["dotNetKey"] = value; }
        }

        [ConfigurationProperty("restApiKey")]
        public string RestApiKey
        {
            get { return (string)base["restApiKey"]; }
            set { base["restApiKey"] = value; }
        }

        [ConfigurationProperty("broadcastChannel")]
        public string BroadcastChannel
        {
            get { return (string)base["broadcastChannel"]; }
            set { base["broadcastChannel"] = value; }
        }

        [ConfigurationProperty("apiUrl")]
        public string ApiUrl
        {
            get { return (string)base["apiUrl"]; }
            set { base["apiUrl"] = value; }
        }
    }
}