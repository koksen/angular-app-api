﻿using System.Configuration;
using App.Infrastructure.Config.Storage;

namespace App.Infrastructure.Config
{
    public partial class AppConfig
    {
        [ConfigurationProperty("storage", IsRequired = true)]
        public StorageElement Storage
        {
            get
            {
                return (StorageElement)base["storage"];
            }
        }
    }
}
