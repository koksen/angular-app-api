using System.Configuration;
using App.Infrastructure.Config.Images;

namespace App.Infrastructure.Config
{
    public partial class AppConfig
    {
        [ConfigurationProperty("images", IsRequired = true)]
        public ImageConfigurationElement Images
        {
            get
            {
                return (ImageConfigurationElement)base["images"];
            }
        }
    }
}
