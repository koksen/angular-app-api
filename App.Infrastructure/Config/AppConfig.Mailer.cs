#region credits
// ***********************************************************************
// Assembly	: Deten.Infrastructure
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-21-2013
// ***********************************************************************
#endregion

using System.Configuration;
using App.Infrastructure.Config.Mailer;

namespace App.Infrastructure.Config
{
    #region

    

    #endregion

    public partial class AppConfig
    {
        [ConfigurationProperty("mailer", IsRequired = true)]
        public MailerElement Mailer
        {
            get
            {
                return (MailerElement)base["mailer"];
            }
        }
    }
}
