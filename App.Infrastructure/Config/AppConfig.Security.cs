#region credits
// ***********************************************************************
// Assembly	: Deten.Infrastructure
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-21-2013
// ***********************************************************************
#endregion

using System.Configuration;
using App.Infrastructure.Config.Security;

namespace App.Infrastructure.Config
{
    #region

    

    #endregion

    public partial class AppConfig
    {
        [ConfigurationProperty("security", IsRequired = true)]
        public SecurityElement Security
        {
            get
            {
                return (SecurityElement)base["security"];
            }
        }
    }
}
