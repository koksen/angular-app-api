﻿using System.Configuration;
using App.Core.Interfaces.Config.GeoLocation;

namespace App.Infrastructure.Config.GeoLocation
{
    /// <summary>
    /// The photo resize element.
    /// </summary>
    public class GeoServiceElement : ConfigurationElement, IGeoService
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [ConfigurationProperty("name")]
        public string Name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        [ConfigurationProperty("title")]
        public string Title
        {
            get { return (string)base["title"]; }
            set { base["title"] = value; }
        }

        [ConfigurationProperty("params")]
        public string Params
        {
            get { return (string)base["params"]; }
            set { base["params"] = value; }
        }

        [ConfigurationProperty("format")]
        public string Format
        {
            get { return (string)base["format"]; }
            set { base["format"] = value; }
        }

        [ConfigurationProperty("type")]
        public string Type
        {
            get { return (string)base["type"]; }
            set { base["type"] = value; }
        }

        [ConfigurationProperty("description")]
        public string Description
        {
            get { return (string)base["description"]; }
            set { base["description"] = value; }
        }

        [ConfigurationProperty("enabled")]
        public bool Enabled
        {
            get { return (bool)base["enabled"]; }
            set { base["enabled"] = value; }
        }

    }
}