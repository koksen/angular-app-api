using System.Configuration;
using App.Core.Interfaces.Config.GeoLocation;

namespace App.Infrastructure.Config.GeoLocation
{
    /// <summary>
    /// The photo section.
    /// </summary>
    public class GeoLocationConfigurationElement : ConfigurationElement, IGeoServiceSettings
    {

        /// <summary>
        /// Gets the web services.
        /// </summary>
        /// <value>
        /// The web services.
        /// </value>
        [ConfigurationProperty("webservices")]
        public GeoServiceCollection WebServices
        {
             get { return (GeoServiceCollection)base["webservices"]; }

        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        [ConfigurationProperty("authToken")]
        public string AuthToken
        {
            get { return (string)base["authToken"]; }

            set { base["authToken"] = value; }
        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        [ConfigurationProperty("authKey")]
        public string AuthKey
        {
            get { return (string)base["authKey"]; }

            set { base["authKey"] = value; }
        }

        /// <value>
        /// The base URL.
        /// </value>
        [ConfigurationProperty("baseUrl")]
        public string BaseUrl
        {
            get { return (string)base["baseUrl"]; }

            set { base["baseUrl"] = value; }
        }

    }
}