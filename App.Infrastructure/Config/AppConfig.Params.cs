using System.Configuration;
using App.Infrastructure.Config.Params;

namespace App.Infrastructure.Config
{
    public partial class AppConfig
    {
        [ConfigurationProperty("params")]
        public ParamsElement Params
        {
            get
            {
                return (ParamsElement)base["params"];
            }
        }
    }
}
