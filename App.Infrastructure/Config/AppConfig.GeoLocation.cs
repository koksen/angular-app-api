using System.Configuration;
using App.Infrastructure.Config.GeoLocation;

namespace App.Infrastructure.Config
{
    public partial class AppConfig
    {
        [ConfigurationProperty("geoLocation")]
        public GeoLocationConfigurationElement GeoLocation
        {
            get
            {
                return (GeoLocationConfigurationElement)base["geoLocation"];
            }
        }
    }
}
