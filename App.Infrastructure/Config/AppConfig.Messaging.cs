using System.Configuration;
using App.Infrastructure.Config.Messaging;

namespace App.Infrastructure.Config
{
    public partial class AppConfig
    {
        [ConfigurationProperty("messaging", IsRequired = true)]
        public MessagingConfigurationElement Messaging
        {
            get
            {
                return (MessagingConfigurationElement)base["messaging"];
            }
        }
    }
}
