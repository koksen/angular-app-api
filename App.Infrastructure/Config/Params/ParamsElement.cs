using System.Configuration;
using App.Core.Interfaces.Config.Params;

namespace App.Infrastructure.Config.Params
{
    public partial class ParamsElement : ConfigurationElement, IParamsSettings
    {

        [ConfigurationProperty("longDateTimeFormat", DefaultValue = "MM/dd/yyyy hh:mm tt")]
        public string LongDateTimeFormat
        {
            get { return (string)base["longDateTimeFormat"]; }
            set { base["longDateTimeFormat"] = value; }
        }

        [ConfigurationProperty("shortDateTimeFormat", DefaultValue = "MM/dd/yyyy h:mm tt")]
        public string ShortDateTimeFormat
        {
            get { return (string)base["shortDateTimeFormat"]; }
            set { base["shortDateTimeFormat"] = value; }
        }

        [ConfigurationProperty("tinyDateTimeFormat", DefaultValue = "MM/dd h:mm tt")]
        public string TinyDateTimeFormat
        {
            get { return (string)base["tinyDateTimeFormat"]; }
            set { base["tinyDateTimeFormat"] = value; }
        }

        [ConfigurationProperty("startWorkTime", DefaultValue = "480")]
        public int StartWorkTime
        {
            get { return (int)base["startWorkTime"]; }
            set { base["startWorkTime"] = value; }
        }

        [ConfigurationProperty("endWorkTime", DefaultValue = "1080")]
        public int EndWorkTime
        {
            get { return (int)base["endWorkTime"]; }
            set { base["endWorkTime"] = value; }
        }

        [ConfigurationProperty("cacheDurationInMinutes", DefaultValue = "100")]
        public int CacheDurationInMinutes
        {
            get { return (int)base["cacheDurationInMinutes"]; }
            set { base["cacheDurationInMinutes"] = value; }
        }


        [ConfigurationProperty("maxDistanceOnPartnerSearch", DefaultValue = "100")]
        public int MaxDistanceOnPartnerSearch
        {
            get { return (int)base["maxDistanceOnPartnerSearch"]; }
            set { base["maxDistanceOnPartnerSearch"] = value; }
        }

        [ConfigurationProperty("maxDistanceOnAssigningPartner", DefaultValue = "100")]
        public int MaxDistanceOnAssigningPartner
        {
            get { return (int)base["maxDistanceOnAssigningPartner"]; }
            set { base["maxDistanceOnAssigningPartner"] = value; }
        }

        
        
    }
}