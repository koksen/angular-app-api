#region credits
// ***********************************************************************
// Assembly	: Deten.Infrastructure
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-21-2013
// ***********************************************************************
#endregion

using System.Configuration;
using App.Core.Interfaces.Config.Meta;

namespace App.Infrastructure.Config.Meta
{
    #region

    #endregion

    public class MetaElement : ConfigurationElement, IMetaSettings
    {

        [ConfigurationProperty("name", IsRequired = true)]
        public string AppName
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("title", IsRequired = true)]
        public string Title
        {
            get { return (string)base["title"]; }
            set { base["title"] = value; }
        }

        [ConfigurationProperty("description", IsRequired = true)]
        public string Description
        {
            get { return (string)base["description"]; }
            set { base["description"] = value; }
        }

        [ConfigurationProperty("copyright", IsRequired = true)]
        public string Copyright
        {
            get { return (string)base["copyright"]; }
            set { base["copyright"] = value; }
        }

        [ConfigurationProperty("keywords", IsRequired = true)]
        public string Keywords
        {
            get { return (string)base["keywords"]; }
            set { base["keywords"] = value; }
        }

        [ConfigurationProperty("email", IsRequired = true)]
        public string Email
        {
            get { return (string)base["email"]; }
            set { base["email"] = value; }
        }

    }
}