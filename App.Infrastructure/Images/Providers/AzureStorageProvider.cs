#region credits
// ***********************************************************************
// Assembly	: TechsApp.Infrastructure
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using App.Core.Common.Images;
using App.Core.Common.Images.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace App.Infrastructure.Images.Providers
{
    #region

    

    #endregion

    public class AzureStorageProvider : ImageProvider
    {
        private string _filePath;

        private const string SearchPattern = ".bmp,.gif,.jpg,.jpeg,.png";
        private CloudBlobContainer _blobContainer;

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            _filePath = config["imageFolder"];
            if (string.IsNullOrEmpty(_filePath))
            {
                throw new ProviderException("Empty or missing 'imageFolder' value");
            }

            config.Remove("imageFolder");

            if (config.Count <= 0) return;
            var attr = config.GetKey(0);
            if (!string.IsNullOrWhiteSpace(attr))
            {
                throw new ProviderException("Unrecognized attribute: " + attr);
            }
        }

        private void SetContainer(string containerName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnection"].ConnectionString);
            CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobStorage.GetContainerReference(containerName);

            if (container.CreateIfNotExists())
            {
                // configure container for public access
                var permissions = container.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(permissions);
            }

            _blobContainer = container;
        }

        #region Overrides of ImageProvider

        public override Image SaveImageResize(IImageRequest item, string resizeName)
        {
            // todo: if resizeName doesn't exist, then we need to throw an exception
            var photoResize = ImageManager.ImageResizes[resizeName];

            using (var stream = item.Stream)
            {
                string extension = GetExtension(item.MimeType);
                string name = string.Format("{0}{1}", DateTime.UtcNow.ToString("dd_MM_yyyy_hh_mm_ss_ffff"), extension);
                var photo = SaveImage(
                    stream, photoResize.Width, photoResize.Height, resizeName);

                return photo;
            }
        }

        public override IList<Image> SaveImageForAllSizes(IImageRequest photoRequest, string containerName, bool keepOriginalSize)
        {
            var photoNamePattern = string.Format("avatar_{0}", containerName.ToLower());
            return SaveImageForAllSizes(photoRequest, containerName, photoNamePattern, keepOriginalSize);
        }

        public override IList<Image> SaveImageForAllSizes(IImageRequest photoRequest, string containerName, string photoNamePattern, bool keepOriginalSize)
        {
            List<Image> photoList;
            SetContainer(containerName);

            var photoResizes = ImageManager.ImageResizes.Where( x => x.Value.Enabled );
            
            using (var stream = photoRequest.Stream)
            {
                var extension = GetExtension(photoRequest.MimeType);
                photoList =
                    photoResizes.Select(
                        resize =>
                        SaveImage(
                            stream,
                            resize.Value.Width,
                            resize.Value.Height,
                            string.Format("{0}_{1}{2}", photoNamePattern, resize.Value.Name.ToLower(), extension),
                            photoRequest.MimeType)).ToList();
            }
            return photoList;

        }

        public override Image GetImageResize(string id, ImageSizeTypeEnum sizeName)
        {
            var photo = GetImage(id, sizeName.ToString().ToLower());
            return photo;
        }

        public override IDictionary<string, Image> GetAllImageResizes(string id)
        {
            var photoResizes = ImageManager.ImageResizes;
            var photos = new Dictionary<string, Image>();

            foreach (var resize in photoResizes)
            {
                var photo = GetImage(id, resize.Key);

                if (photo != null)
                {
                    photos.Add(resize.Key, photo);
                }
            }

            var originalImage = GetImage(id, null);

            if (originalImage != null)
            {
                photos.Add("original", originalImage);
            }

            return photos;
        }

        public override IList<Image> GetImagesByResize(string resizeName, string[] ids)
        {
            return new List<Image>();
        }

        #endregion

        #region private methods

        private Image SaveImage(
            Stream stream,
            int width,
            int height,
            string imageFileName,
            string mimeType = "image/jpeg")
        {
            
            CloudBlockBlob blob;
            using (var resizedImage = ImageHelper.ResizeImage(stream, width, height))
            {
                var memoryStream = new MemoryStream();
                resizedImage.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                memoryStream.Position = 0;
                blob = _blobContainer.GetBlockBlobReference(imageFileName);
                blob.Properties.ContentType = mimeType;
                blob.UploadFromStream(memoryStream);
            }

            return new Image
            {
                ImageId = blob.Uri.ToString(),
                ResizeName = imageFileName,
                Url = blob.Uri.ToString()
            };
        
        }

        private Image GetImage(string id, string sizeName)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            var virtualImagePath = string.Format("{0}{1}", _filePath, id);
            if (!sizeName.Equals("medium"))
                virtualImagePath = virtualImagePath.Replace("medium", sizeName);

            var photo = new Image { ImageId = id.Split('.')[0], ResizeName = sizeName, Url = virtualImagePath };

            return photo;
        }

        private string GetExtension(string mimeType, string defaultExtension = ".jpeg")
        {
            // todo: need to isolate the logic here for parsing the value
            string extension = mimeType.Split('/').Length > 1 ? mimeType.Split('/')[1] : defaultExtension;
            extension = string.Format(".{0}", extension.Trim('.'));

            if (!SearchPattern.Split(',').Contains(extension))
            {
                extension = ".jpeg";
            }

            return extension;
        }

        #endregion
    }
}