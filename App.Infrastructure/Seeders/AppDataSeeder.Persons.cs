﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using App.Core.Models;
using App.Infrastructure.Data;

namespace App.Infrastructure.Seeders
{
    public partial class AppDataSeeder
    {
        public static void SeedProfiles(DataContext context)
        {
            if (context.Profile.Any())
                return;

            var persons = new List<UserProfile>
                {
                    new UserProfile { FirstName = "Person 01", Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 02", Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 03", Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 04",  Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 05", Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 06",Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 07", Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 08",  Created = DateTime.UtcNow },
                    new UserProfile { FirstName = "Person 09", Created = DateTime.UtcNow }

                };

            persons.ForEach(t => context.Profile.AddOrUpdate(t));
        }
    }
}
