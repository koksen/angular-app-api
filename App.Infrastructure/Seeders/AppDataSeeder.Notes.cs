﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using App.Core.Enums;
using App.Core.Models;
using App.Infrastructure.Data;
using App.Infrastructure.IdentityManager;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Seeders
{
    public partial class AppDataSeeder
    {
        public static void SeedNotes(DataContext context)
        {
            
            if (context.Notes.Any())
                return;

            var UserManager = new UserProfileManager(new UserStore(context));
            var user = UserManager.FindByName("admin");

            if (user == null)
                return;

            var notes = new List<Note>();

            for (var i = 1; i < 6; i++)
            {
                notes.Add(new Note
                {
                    UserToId = user.Id,
                    Type = NoteType.General,
                    Title = string.Format("Note title {0}", i),
                    Description = string.Format("Note description {0}",i),
                    Tags = NoteType.General.ToString(),
                    Created = DateTime.UtcNow
                });

            }

            notes.ForEach(n => context.Notes.AddOrUpdate(n));
        }
    }
}
