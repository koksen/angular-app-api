﻿using System.Security.Claims;
using App.Core.Models;
using App.Infrastructure.Data;
using App.Infrastructure.IdentityManager;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Seeders
{
    public partial class AppDataSeeder
    {
        public static void SeedAdmin(DataContext context)
        {
            var UserManager = new UserProfileManager(new UserStore(context));
            var RoleManager = new ApplicationRoleManager(new RoleStore(context));

            if (!RoleManager.RoleExists("Administrator"))
            {
                RoleManager.Create(new Role("Administrator"));
            }

            if (!RoleManager.RoleExists("User"))
            {
                RoleManager.Create(new Role("User"));
            }

            if (UserManager.FindByName("admin") == null)
            {
                var user = new User
                {
                    UserName = "admin", 
                    Email = "admin@angularjsauth.com", 
                    EmailConfirmed = true, 
                    PhoneNumber = "5555555555", 
                    PhoneNumberConfirmed = true
                };
                var result = UserManager.Create(user, "Administr@t0r");
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Administrator");
                    UserManager.AddClaim(user.Id, new Claim(ClaimTypes.Role, "Admin"));
                }
            }

        }
    }
}
