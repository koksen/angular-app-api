﻿using App.Infrastructure.Data;

namespace App.Infrastructure.Seeders
{

    public static partial class AppDataSeeder
    {
        public static void Seed(DataContext context)
        {
            SeedAdmin(context);
            SeedNotes(context);
            //SeedProfiles(context);
        }

    }
}