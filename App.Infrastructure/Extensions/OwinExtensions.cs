﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Owin;

namespace App.Infrastructure.Extensions
{
    public static class OwinExtensions
    {
        public static IAppBuilder UseDbContextFactory(this IAppBuilder app, Func<DbContext> createCallback)
        {
            if (app == null)
            {
                throw new ArgumentNullException("app");
            }
            if (createCallback == null)
            {
                throw new ArgumentNullException("createCallback");
            }

            app.Use(typeof(IdentityFactoryMiddleware<DbContext, IdentityFactoryOptions<DbContext>>),
                    new IdentityFactoryOptions<DbContext>
                        {
                            Provider = new IdentityFactoryProvider<DbContext>
                                {
                                   // OnCreate = options => createCallback()
                                }
                        });
            return app;
        }

        public static DbContext GetDbContext(this IOwinContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Get<DbContext>();
        }
    }
}