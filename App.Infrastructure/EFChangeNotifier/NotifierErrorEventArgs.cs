﻿using System;
using System.Data.SqlClient;

namespace App.Infrastructure.EFChangeNotifier
{
    public class NotifierErrorEventArgs : EventArgs
    {
        public string Sql { get; set; }
        public SqlNotificationEventArgs Reason { get; set; }
    }
}
