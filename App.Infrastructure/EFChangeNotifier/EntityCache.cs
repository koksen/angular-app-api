﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Caching;
using App.Common.Tracing;

namespace App.Infrastructure.EFChangeNotifier
{
    public class EntityCache<TEntity, TDbContext>
        : IDisposable
        where TDbContext : DbContext, new()
        where TEntity : class
    {
        private DbContext _context;
        private readonly Expression<Func<TEntity, bool>> _query;

        private readonly string _cacheKey = Guid.NewGuid().ToString();

        public EntityCache(Expression<Func<TEntity, bool>> query)
        {
            _context = new TDbContext();
            _query = query;
        }

        private IEnumerable<TEntity> GetCurrent()
        {
            var query = _context.Set<TEntity>().Where(_query);

            return query;
        }

        private IEnumerable<TEntity> GetResults()
        {
            var value = MemoryCache.Default[_cacheKey] as List<TEntity>;

            if (value == null)
            {
                value = GetCurrent().ToList();

                var changeMonitor = new EntityChangeMonitor<TEntity, TDbContext>(_query);

                var policy = new CacheItemPolicy();

                policy.ChangeMonitors.Add(changeMonitor);

                MemoryCache.Default.Add(_cacheKey, value, policy);

                Tracing.Information("From Database...");
            }
            else
            {
                Tracing.Information("From Cache...");
            }

            return value;
        }

        public IEnumerable<TEntity> Results
        {
            get
            {
                return GetResults();
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
