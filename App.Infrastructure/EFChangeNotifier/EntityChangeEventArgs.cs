﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.EFChangeNotifier
{
    public class EntityChangeEventArgs<T> : EventArgs
    {
        public IEnumerable<T> Results { get; set; }
        public bool ContinueListening { get; set; }
    }
}
