﻿
using App.Core.Interfaces.Infrastructure;

namespace App.Infrastructure.Helpers
{
    public class AppContext : IAppContext
    {
        public string GetIP()
        {
            return Common.GetIPAddress();
        }

        public string GetUserAgent()
        {
            return Common.GetUserAgent();
        }

        public bool IsMobile()
        {
            return Common.IsMobile();
        }

        public string GetDeviceId()
        {
            return Common.GetDeviceId();
        }
    }
}