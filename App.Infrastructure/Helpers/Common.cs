﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using App.Infrastructure.Config;

namespace App.Infrastructure.Helpers
{
    public static class Common
    {
        private const string BingMapsApiKey = "Atk_aRF91kdsKA5s3iAn2RdT50GtNme0elFx7x3tAxWJBQ-gK5N90Y5WJG_hjXA6";

        public static double? TimeZoneOffset
        {
            get
            {
                if (HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset] != null)
                    return (double)HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset];
               
                if (HttpContext.Current.Request.Cookies[CacheConstants.CurrentUserTimeZoneOffset] != null)
                    return Double.Parse(HttpContext.Current.Request.Cookies[CacheConstants.CurrentUserTimeZoneOffset].Value);

                return null;
            }
            set
            {
                HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset] = value;
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(CacheConstants.CurrentUserTimeZoneOffset, value.ToString()));
            }
        }

        public static string FormatDateTime(DateTime date)
        {
            var custom = date.ToString(AppConfig.Instance.Params.LongDateTimeFormat ?? "MMM dd, h:mmtt");

            return custom;
        }

        public static string FormatDateTimeShort(DateTime date)
        {
            var custom = date.ToString("MMMM dd");

            return custom;
        }
        /*
        public static Image ResizeByHeight(Image imgPhoto, int newHeight)
        {
            if (imgPhoto.Height <= newHeight)
                return imgPhoto;

            int targetH = newHeight;
            int targetW = (int)(imgPhoto.Width * (newHeight / (float)imgPhoto.Height));

            var bmPhoto = new Bitmap(targetW, targetH, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.FillRectangle(Brushes.White, 0, 0, targetW, targetH);
            grPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetW, targetH), 0, 0, imgPhoto.Width, imgPhoto.Height,
                              GraphicsUnit.Pixel);

            grPhoto.Flush();
            grPhoto.Dispose();
            imgPhoto.Dispose();

            return bmPhoto;
        }
        */
        /// <summary>
        /// Converts a comma separated list values into Array collection
        /// </summary>
        /// <param name="csl">Comma separated list</param>
        /// <returns>returns an IEnumerable collection</returns>
        public static T[] CslToCollection<T>(string csl)
        {
            return CslToCollection<T>(csl, ",");
        }

        /// <summary>
        /// Converts a list with a separator into Array collection
        /// </summary>
        /// <typeparam name="T">Value type</typeparam>
        /// <param name="input">string input</param>
        /// <param name="separator">string value by which the input will be seperated</param>
        /// <returns></returns>
        public static T[] CslToCollection<T>(string input, string separator)
        {
            var result = new List<T>();

            if (!String.IsNullOrEmpty(input))
            {
                try
                {
                    var items = input.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);

                    result.AddRange(items.Select(s => (T)Convert.ChangeType(s, typeof(T))));
                }
                catch
                {
                    throw new InvalidCastException();
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// Gets the latitude and longitude values for a given location
        /// </summary>
        /// <param name="location">Location ex. Los Angeles, USA</param>
        /// <param name="latitude">Returns the location's latitude</param>
        /// <param name="longitude">Returns the location's longitude</param>
        /// <returns>Returns whether the location has been found</returns>
        public static bool GetCoordinates(string location, ref float latitude, ref float longitude)
        {
            string url = string.Format("http://dev.virtualearth.net/REST/v1/Locations?query={0}&key={1}&o=xml", location, BingMapsApiKey);
            return GetCoordinatesPrepared(url, ref  latitude, ref longitude);
        }

        public static bool GetCoordinates(string city, string address, ref float latitude, ref float longitude)
        {
            if (string.IsNullOrEmpty(city))
                return GetCoordinates(address, ref latitude, ref longitude);
            
            string url = string.Format("http://dev.virtualearth.net/REST/v1/Locations?locality={0}&addressLine={1}&key={2}&o=xml", city, address, BingMapsApiKey);
            return GetCoordinatesPrepared(url, ref  latitude, ref longitude);
        }

        public static double? GetTimeZoneOffset(string location, ref DateTime localTime)
        {
            double? result = null;

            try
            {
                float latitude = 0;
                float longitude = 0;
                var isSuccess = GetCoordinates(location, ref latitude, ref longitude);

                if (isSuccess)
                {
                    result = GetTimeZoneOffsetByCoordinate(latitude, longitude, ref localTime);
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public static double? GetTimeZoneOffsetByCoordinate(float latitude, float longitude, ref DateTime localTime)
        {
            const string apiQuery = "http://api.geonames.org/timezone?lat={0}&lng={1}&username=techsapp";
            double? result = null;

            try
            {
                var query = String.Format(apiQuery, latitude, longitude);
                var xdoc = XDocument.Load(query);
                var dstOffsetNode = xdoc.Descendants("dstOffset").FirstOrDefault();
                var timeNode = xdoc.Descendants("time").FirstOrDefault();

                if (dstOffsetNode != null)
                {
                    result = double.Parse(dstOffsetNode.Value, CultureInfo.InvariantCulture);

                    if (timeNode != null)
                        DateTime.TryParse(timeNode.Value, out localTime);
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }      

        public static string GetIPAddress()
        {

            try
            {
                HttpContext context = HttpContext.Current;

                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                return string.Empty; 
            }
            
        }

        public static int GetTimeZoneOffsetByIP()
        {
          
            DateTime localTime = DateTime.Now;
            int offset = 0;

            var ip = GetIPAddress();

            if (ip == null) 
                return offset;
            
            var locationInfo = GetLocationInfo(ip);

            if (locationInfo != null)
            {
                offset = (int)GetTimeZoneOffsetByCoordinate(locationInfo.Latitude, locationInfo.Longitude, ref localTime).GetValueOrDefault();
            }

            return offset;
        }

        public static LocationInfo GetLocationInfo(string ipAddress)
        {
            IPAddress i = IPAddress.Parse(ipAddress);

            return GetLocationInfo(i);
        }

        public static LocationInfo GetLocationInfo(IPAddress ipAddress)
        {

            LocationInfo result = null;
            try
            {
                string ip = ipAddress.ToString();

                string r;
                using (var w = new WebClient())
                {
                    r = w.DownloadString(String.Format("http://freegeoip.net/xml/{0}", ip));
                }

                var xmlResponse = XDocument.Parse(r);


                result = (from x in xmlResponse.Descendants("Response")
                          select new LocationInfo
                          {
                              CountryCode = x.Element("CountryCode").Value,
                              CountryName = x.Element("CountryName").Value,
                              Latitude = float.Parse(x.Element("Latitude").Value),
                              Longitude = float.Parse(x.Element("Longitude").Value),
                              Name = x.Element("City").Value
                          }).SingleOrDefault();
            }
            catch
            {
                //Looks like we didn't get what we expected.
            }

            return result;
        }

        private static bool GetCoordinatesPrepared(string url, ref float latitude, ref float longitude)
        {
            bool result = false;

            try
            {
                XDocument xdoc = XDocument.Load(url);

                if (xdoc.Descendants().FirstOrDefault(c => c.Name.LocalName == "Latitude") != null)
                {
                    latitude = float.Parse(xdoc.Descendants().First(d => d.Name.LocalName == "Latitude").Value,
                                           CultureInfo.InvariantCulture);
                    longitude = float.Parse(xdoc.Descendants().First(d => d.Name.LocalName == "Longitude").Value,
                                            CultureInfo.InvariantCulture);

                    result = true;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public static string GetUserAgent()
        {
            var headers = HttpContext.Current.Request.Headers;
            var userAgent = headers["User-Agent"];
            return userAgent;
        }

        public static bool IsMobile()
        {
            var request = HttpContext.Current.Request;
            var headers = request.Headers;
            var userAgent = headers["User-Agent"];
            //string u = request.ServerVariables["HTTP_USER_AGENT"];
            var b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return ((b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4))));
        }

        
        public static string GetDeviceId()
        {
            var request = HttpContext.Current.Request;
            var headers = request.Headers;
            var deviceId = headers["X-ATT-DeviceId"];
            if (!string.IsNullOrEmpty(deviceId))
                return deviceId.Trim();

            return null;
        }

    }

    public class LocationInfo
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
    }

}