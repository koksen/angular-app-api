﻿using System;
using System.Globalization;
using System.Web;
using App.Core.Models.Common;
using App.Infrastructure.Config;
using RestSharp;
using HttpCookie = System.Web.HttpCookie;

namespace App.Infrastructure.Helpers
{
    public static class GeoInfo
    {

        private static RestRequest GetRestRequest(string webservice, out string baseUrl)
        {
            baseUrl = "";
            var config = AppConfig.Instance.GeoLocation;
            var serviceConfig = config.WebServices[webservice];
            if (serviceConfig == null)
                return null;

            baseUrl = String.Format("{0}/{1}", config.BaseUrl, serviceConfig.Name);
            var request = new RestRequest(Method.GET);
            request.AddParameter(config.AuthToken, config.AuthKey);
            request.RequestFormat = serviceConfig.Format.Equals("Json") ? DataFormat.Json : DataFormat.Xml;
            return request;
        }

        public static Location GetTimeZoneInfo(double latitude, double longitude)
        {
            try
            {
                string baseUrl;
                var request = GetRestRequest("timezone", out baseUrl);
                if (request == null)
                    return null;

                var client = new RestClient(baseUrl);
                request.AddParameter("lat", latitude.ToString(CultureInfo.InvariantCulture));
                request.AddParameter("lng", longitude.ToString(CultureInfo.InvariantCulture));

                IRestResponse<Location> response = client.Execute<Location>(request);
                return response.Data;
            }
            catch
            {
                return null;
            }

        }

        public static Weather GetWeatherInfo(double latitude, double longitude, string lang = "us")
        {
            try
            {
                string baseUrl;
                var request = GetRestRequest("findNearByWeather", out baseUrl);
                if (request == null)
                    return null;

                var client = new RestClient(baseUrl);
                request.AddParameter("lat", latitude);
                request.AddParameter("lng", longitude);
                request.AddParameter("lang", lang);

                IRestResponse<Weather> response = client.Execute<Weather>(request);
                return response.Data;
            }
            catch
            {
                return null;
            }

        }
       
        public static double ClientTimeZoneOffset
        {
             get
            {
                if (HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset] != null)
                    return (double)HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset];

                if (HttpContext.Current.Request.Cookies[CacheConstants.CurrentUserTimeZoneOffset] != null)
                {
                    var timezoneOffset = Double.Parse(HttpContext.Current.Request.Cookies[CacheConstants.CurrentUserTimeZoneOffset].Value);
                    ClientTimeZoneOffset = timezoneOffset;
                    return timezoneOffset;
                }

                return 0;
            }
            set
            {
                HttpContext.Current.Session[CacheConstants.CurrentUserTimeZoneOffset] = value;
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(CacheConstants.CurrentUserTimeZoneOffset, value.ToString(CultureInfo.InvariantCulture)));
            }

        }

        public static DateTime ToLocalTime(DateTime utcDate)
        {
            return utcDate.AddHours(ClientTimeZoneOffset);
        }


    }

}