﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using App.Core.Interfaces.Config.Messaging;
using App.Core.Interfaces.Messaging;
using App.Core.Models.Messaging;
using App.Infrastructure.Config;
using SendGrid;

namespace App.Infrastructure.Messaging
{
    public class EmailMessageService : IEmailService
    {

        private static readonly ISendGridMailerSettings _config;

        static EmailMessageService()
        {
            _config = AppConfig.Instance.Messaging.Mailer.Current;
        }

        public async Task Send(string[] recipients, string message, string subject, Dictionary<string, string> data)
        {
            foreach (var recipient in recipients)
            {
                await Send(recipient, message, subject, data);
            }
        }

        public async Task Send(MessageInfo message)
        {
            await Send(message.To, message.Body, message.Subject, null);
        }

        public async Task Send(string recipient, string message, string subject, Dictionary<string, string> data)
        {
            // Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();
            myMessage.AddTo(recipient);
            myMessage.From = new MailAddress(_config.FromEmail, _config.FromName);
            myMessage.Subject = subject;
            myMessage.Text = message;

            // Create credentials, specifying your user name and password.
            var credentials = new NetworkCredential(_config.Username, _config.Password);

            // Create a REST transport for sending email.
            var transportREST = new Web(credentials);

            // Send the email.
            await transportREST.DeliverAsync(myMessage);

        }

    }
}
