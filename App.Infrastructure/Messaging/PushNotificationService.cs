﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Common.Tracing;
using App.Core.Enums;
using App.Core.Interfaces.Config.Messaging;
using App.Core.Interfaces.Messaging;
using App.Core.Interfaces.Services;
using App.Core.Models.Messaging;
using App.Infrastructure.Config;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using RestSharp;

namespace App.Infrastructure.Messaging
{
    public class PushNotificationService : IPushNotificationService
    {
        private static IApplePushSettings _appleConfig;
        private static IGooglePushSettings _googleConfig;
        private static PushNotification _push;

        private static ILogService _logService;

        public PushNotificationService(ILogService logService)
        {
            _appleConfig = AppConfig.Instance.Messaging.Apple.Current;
            _googleConfig = AppConfig.Instance.Messaging.Google.Current;

            _logService = logService;
        }

        public void Send(PushNotification push)
        {
            _push = push;
            switch (push.Device)
            {
                case MobileOS.Android:
                    SendPushToAndroid();
                    break;
                case MobileOS.iOS:
                    //if (AppConfig.Instance.Messaging. == "apple")
                    //    SendPushToIOSThroughApple();
                    //else
                    //{
                    SendPushToIOS();
                    //}
                    break;
                case MobileOS.BlackBerry:
                    SendPushToBlackBerry();
                    break;
                case MobileOS.Windows:
                    SendPushToWindows();
                    break;
            }
        }

        public Task SubscribeToChannelAsync(string deviceToken, string deviceType, List<string> channels)
        {

            var parseConfig = AppConfig.Instance.Messaging.Parse.Current;
            var client = new RestClient(parseConfig.ApiUrl);

            var o = new { deviceToken, deviceType = deviceType.ToLower(), channels };

            var request = new RestRequest("installations", Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            request.AddHeader("Accept", "application/json");
            request.AddHeader("X-Parse-Application-Id", parseConfig.ApplicationId);
            request.AddHeader("X-Parse-REST-API-Key", parseConfig.RestApiKey);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");

            request.AddBody(o);

            client.ExecuteAsync(request, r =>
            {
                var log = _logService.Create();
                log.Message = r.Content;
                log.Type = r.ResponseStatus.ToString();
                log.Username = channels.ToString();
                _logService.SaveOrUpdate(log);
            });

            return null;

        }


        public async Task SendAsync(PushNotification push)
        {
            _push = push;
            switch (push.Device)
            {
                case MobileOS.Android:
                    SendPushToAndroid();
                    break;
                case MobileOS.iOS:
                    SendPushToIOS();
                    //}
                    break;
                case MobileOS.BlackBerry:
                    SendPushToBlackBerry();
                    break;
                case MobileOS.Windows:
                    SendPushToWindows();
                    break;
            }
        }

        private static void TrackEvents(PushBroker push)
        {
            //    //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

        }

        private static void SendPushToWindows()
        {
            throw new NotImplementedException();
        }

        private static void SendPushToBlackBerry()
        {
            throw new NotImplementedException();
        }

        private static void SendPushToIOS()
        {

            using (var push = new PushBroker())
            {
                //    //Wire up the events for all the services that the broker registers
                TrackEvents(push);

                //**** iOS Notification ******

                var isProduction = AppConfig.Instance.Messaging.Apple.Default.Equals("prod");
                //Establish the connection to your certificates. Here we make one for dev and another for production
                byte[] appleCertificate = null;
                //Add here your push private key
                //byte[] appleCertificate = !isProduction ?
                //                          Properties.Resources.TechsAppProdPushPrivateKey2 :
                //                          Properties.Resources.TechsAppProdPushPrivateKey2;

                //If the file exists, go ahead and use it to send an apple push notification
                if (appleCertificate == null)
                    return;

                var channelSettings = new ApplePushChannelSettings(isProduction, appleCertificate, _appleConfig.CertificatePassword, true);

                //Give the apple certificate and its password to the push broker for processing
                push.RegisterAppleService(channelSettings);

                push.QueueNotification(new AppleNotification()
                                           .ForDeviceToken(_push.DeviceToken)
                                           .WithContentAvailable(1)
                                           .WithCustomItem("ticketId", _push.Data.ContainsKey("ticketId") ? _push.Data["ticketId"] : 0)
                                           .WithCustomItem("action", _push.Data.ContainsKey("action") ? _push.Data["action"] : "")
                                           .WithAlert(_push.Message)
                                           .WithBadge(_push.Badge)
                                           .WithSound(_push.Sound));
            }
        }

        private static void SendPushToAndroid()
        {

            using (var push = new PushBroker())
            {

                var info = _push.Data;

                TrackEvents(push);
                //**** Android Notification ******
                //Register the GCM Service and sending an Android Notification with your browser API key found in your google API Console for your app. Here, we use ours.
                push.RegisterGcmService(new GcmPushChannelSettings(_googleConfig.SenderId, _googleConfig.AuthToken, _googleConfig.ApplicationIdPackageName));

                info.Add("alert", _push.Message);
                info.Add("badge", _push.Badge);
                info.Add("sound", _push.Sound);
                info.Add("vibrate", _push.Vibrate.ToString());

                var json = info.Aggregate(string.Empty, (current, o) => current + String.Format("\"{0}\":\"{1}\",", o.Key.ToLower(), o.Value));
                json = "{" + json.Remove(json.Length - 1) + "}";

                //Queue the Android notification. Unfortunately, we have to build this packet manually. 
                //*********************************
                push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(_push.DeviceToken).WithJson(json));
                push.StopAllServices();

            }

        }

        #region Events Tracing

        private static void ChannelDestroyed(object sender)
        {
            Tracing.Information("Push Channel Destroyed");
        }

        private static void ChannelCreated(object sender, PushSharp.Core.IPushChannel pushChannel)
        {
            Tracing.Information("Push Channel Created");
        }

        private static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, PushSharp.Core.INotification notification)
        {
            Tracing.Information("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId);
        }

        private static void NotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
            Tracing.Information(string.Format("Send Failure: {0} -> {1} -> {2}", notification.EnqueuedTimestamp, error.Message, (error.InnerException != null) ? error.InnerException.Message : ""));
        }

        private static void ServiceException(object sender, Exception error)
        {
            Tracing.Information(string.Format("Service Failure: {0} -> {1}", error.Message, (error.InnerException != null) ? error.InnerException.Message : ""));
        }

        private static void ChannelException(object sender, PushSharp.Core.IPushChannel pushChannel, Exception error)
        {
            Tracing.Information(string.Format("Channel Exception: {0} -> {1}", error.Message, (error.InnerException != null) ? error.InnerException.Message : ""));
        }

        private static void NotificationSent(object sender, PushSharp.Core.INotification notification)
        {
            Tracing.Information(string.Format("Sent: {0} -> {1}", notification.EnqueuedTimestamp, notification.Tag));
        }

        #endregion

    }
}
