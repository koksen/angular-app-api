﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Core.Interfaces.Config.Messaging;
using App.Core.Interfaces.Messaging;
using App.Core.Models.Messaging;
using App.Infrastructure.Config;
using Twilio;

namespace App.Infrastructure.Messaging
{
    #region

    #endregion

    public class SmsService : ISmsService
    {

        private static readonly ITwilioSmsSettings _config;

        static SmsService()
        {
            _config = AppConfig.Instance.Messaging.Sms.Current;
        }

        public async Task Send(string[] recipients, string message)
        {
            foreach (var recipient in recipients)
            {
                await Send(recipient, message);
            }
        }

        public async Task Send(MessageInfo message)
        {
            await Send(message.To, message.Body);
        }

        public async Task Send(string recipient, string message)
        {
            var twilio = new TwilioRestClient(_config.AccountSid, _config.AuthToken);

            if (message.Length > _config.SmsMaxLength)
                message = message.Substring(0, _config.SmsMaxLength);

            var smsLog = new List<string>();

            var msg = await Task.Factory.StartNew(() => twilio.SendSmsMessage(_config.FromNumber, recipient, message));

            smsLog.Add(String.Format("Sent: {0}\nStatus: {1}", msg.DateCreated, msg.Status));

        }

    }
}
