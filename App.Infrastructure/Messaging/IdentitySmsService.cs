﻿using System.Threading.Tasks;
using App.Core.Interfaces.Config.Messaging;
using App.Infrastructure.Config;
using Microsoft.AspNet.Identity;
using Twilio;

namespace App.Infrastructure.Messaging
{
    #region

    #endregion

    public class IdentitySmsService : IIdentityMessageService// ISmsService
    {

        private static readonly ITwilioSmsSettings _config;

        static IdentitySmsService()
        {
            _config = AppConfig.Instance.Messaging.Sms.Current;
        }
        
        public Task SendAsync(IdentityMessage message)
        {
            var twilio = new TwilioRestClient(_config.AccountSid, _config.AuthToken);

            if (message.Body.Length > _config.SmsMaxLength)
                message.Body = message.Body.Substring(0, _config.SmsMaxLength);

            twilio.SendSmsMessage(_config.FromNumber, message.Destination, message.Body);

            // Twilio does not return an async Task, so we need this:
            return Task.FromResult(0);

        }
    }
}
