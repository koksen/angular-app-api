﻿using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using App.Core.Interfaces.Config.Mailer;
using App.Infrastructure.Config;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Messaging
{
    public class IdentityEmailService : IIdentityMessageService
    {
        private static readonly IMailerSettings _mailer;

        static IdentityEmailService()
        {
            _mailer = AppConfig.Instance.Mailer;
        }

        public Task SendAsync(IdentityMessage message)
        {
            if (_mailer == null)
                return Task.FromResult(0);

            string html = message.Body;
            var mail = new MailMessage
            {
                From = new MailAddress(_mailer.From, _mailer.Title),
                Subject = message.Subject,
                Priority = MailPriority.High,
                IsBodyHtml = true
            };
            mail.To.Add(new MailAddress(message.Destination, ""));
            mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            // Init SmtpClient and send
            var smtpClient = new SmtpClient();

            return Task.Factory.StartNew(() => smtpClient.SendAsync(mail, "token"));
           
        }

    }
}
