using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text;
using App.Core.Interfaces.Data;
using App.Core.Models;
using App.Core.Models.Common;
using App.Infrastructure.Data.Constraints;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Infrastructure.Data
{
    public class DataContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>, IDataContext //BaseDbContext<DataContext>
    {

        public DataContext()
                    : base("DataContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Migrations.Configuration>());
            //Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = false;
        }

        public static DataContext Create()
        {
            return new DataContext();
        }


        public ObjectContext ObjectContext()
        {
            ObjectContext objContext;
            try
            {
                objContext = (this as IObjectContextAdapter).ObjectContext;
            }

            catch (DbEntityValidationException dbEx)
            {
                var sb = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", validationErrors.Entry.Entity.GetType());
                    foreach (var error in validationErrors.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                    Trace.TraceInformation(sb.ToString());
                    throw new DbEntityValidationException("Entity Validation Failed - errors follow:\n" + sb, dbEx);

                }
                return null;
            }
            catch (Exception ex)
            {
                throw new DbEntityValidationException("Errors follow:\n" + ex.Message, ex);
            }
            return objContext;
        }


        public virtual IDbSet<T> DbSet<T>() where T : BaseModel
        {
            return Set<T>();
        }

        public new DbEntityEntry Entry<T>(T entity) where T : BaseModel
        {
            return base.Entry(entity);
        }


        public DbSet<UserProfile> Profile { get; set; }

        public DbSet<UserSession> Sessions { get; set; }

        public DbSet<Note> Notes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().ToTable("User").Property(p => p.Id).HasColumnName("UserId");

            modelBuilder.Entity<User>().Property(p => p.Email).HasMaxLength(120).IsRequired();

            modelBuilder.Entity<User>().Property(p => p.PasswordHash).HasMaxLength(200);

            modelBuilder.Entity<UserProfile>().Property(p => p.FirstName).HasMaxLength(20).IsRequired();

            modelBuilder.Entity<UserProfile>().Property(p => p.LastName).HasMaxLength(80);

            modelBuilder.Entity<Role>().ToTable("Role").Property(p => p.Id).HasColumnName("RoleId");  

            modelBuilder.Entity<UserClaim>().ToTable("UserClaim").HasKey(r => new { r.ClaimType, r.UserId });

            modelBuilder.Entity<UserLogin>().ToTable("UserLogin").HasKey(r => new { r.ProviderKey, r.UserId });

            modelBuilder.Entity<UserRole>().ToTable("UserRole").HasKey(r => new { r.RoleId, r.UserId });

            modelBuilder.Configurations.Add(new UserProfileConfiguration());

            //modelBuilder.Entity<Profile>()
            //    .ToTable("Profile")
            //    .HasKey(r => new { r.UserId })
            //    .Property(c => c.UserId).IsRequired();
        
        }
            
    }
}