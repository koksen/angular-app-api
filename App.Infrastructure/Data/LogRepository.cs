using App.Core.Interfaces.Data;
using App.Core.Models.Common;

namespace App.Infrastructure.Data
{
    public class LogRepository : BaseRepository<Log>, ILogRepository
    {
		public LogRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}