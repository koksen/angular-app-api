using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using App.Core.Interfaces.Data;
using App.Core.Models.Common;

namespace App.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {        
        private readonly IDatabaseFactory _databaseFactory;
        private IDataContext _datacontext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
            DataContext.ObjectContext().SavingChanges += (sender, e) => BeforeSave(GetChangedOrNewEntities());
        }

        public IDataContext DataContext
        {
            get { return _datacontext ?? (_datacontext = _databaseFactory.Get()); }
        }

        private IEnumerable<BaseModel> GetChangedOrNewEntities()
        {
            const EntityState NewOrModified = EntityState.Added | EntityState.Modified;

            return DataContext.ObjectContext().ObjectStateManager.GetObjectStateEntries(NewOrModified)
                .Where(x => x.Entity != null).Select(x => x.Entity as BaseModel);
        }

        public void BeforeSave(IEnumerable<BaseModel> entities)
        {
            foreach (var entity in entities)
            {
                if ( IsPersistent(entity) )
                    entity.Updated = DateTime.UtcNow;
                entity.Created = !IsPersistent(entity) ? DateTime.UtcNow : entity.Created;
            }
        }

        public int Commit()
        {
            try
            {
                int result = DataContext.ObjectContext().SaveChanges();
                return result;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var error in ex.EntityValidationErrors)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine("Entity {0} in state {1} has validation errors:",
                        error.Entry.Entity.GetType().Name, error.Entry.State);
                    foreach (var ve in error.ValidationErrors)
                    {
                        Console.WriteLine("\tProperty: {0}, Error: {1}",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                    Console.WriteLine();
                }
                throw;
            }

        }

        public string Metadata()
        {
            throw new NotImplementedException();
        }

        public static bool IsPersistent(BaseModel entity)
        {
            var isPersistent = true;
            var keys = GetPrimaryKeys(entity);
            foreach (var key in keys)
            {
                isPersistent = isPersistent && !(key.Value.ToString().Equals(string.Empty) || key.Value.ToString().Equals("0"));
            }

            return isPersistent;
        }

        public static Dictionary<string, object> GetPrimaryKeys(BaseModel entity)
        {
            var properties = entity.GetType().GetProperties();
            var keys = new Dictionary<string, object>();
            foreach (var property in properties)
            {
                var attribute = Attribute.GetCustomAttribute(property, typeof(KeyAttribute)) as KeyAttribute;
                if (attribute != null)
                {
                    keys.Add(property.Name, property.GetValue(entity));
                }
            }
            return keys;
        }
    }
}
