using App.Core.Interfaces.Data;
using App.Core.Models;

namespace App.Infrastructure.Data
{
    public class UserSessionRepository : BaseRepository<UserSession>, IUserSessionRepository
    {
		public UserSessionRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}