﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using App.Core.Models;

namespace App.Infrastructure.Data.Constraints
{
    public class UserProfileConfiguration : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileConfiguration()
        {

            HasKey(e => e.UserId);

            Property(e => e.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(e => e.User)
            .WithRequiredDependent(s => s.Profile)
            .WillCascadeOnDelete(true);


        }
    }
}