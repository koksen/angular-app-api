﻿using App.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Infrastructure.Data
{
    public class RoleStore : RoleStore<Role, int, UserRole>
    {
        public RoleStore(DataContext context)
            : base(context)
        {
        }
    }
}