﻿using App.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Infrastructure.Data
{
    public class UserStore : UserStore<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public UserStore(DataContext context)
            : base(context)
        {
        }
    }
}