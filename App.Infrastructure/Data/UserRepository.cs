using App.Core.Interfaces.Data;
using App.Core.Models;

namespace App.Infrastructure.Data
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
		public UserRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}