using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using App.Core.Interfaces.Data;
using App.Core.Models.Common;

namespace App.Infrastructure.Data
{
    /// <summary>
    ///     An abstract baseclass handling basic CRUD operations against the context.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRepository<T> : IDisposable, IRepository<T> where T : BaseModel
    {
        private readonly IDatabaseFactory _databaseFactory;
        protected readonly IDbSet<T> _dbset;
        protected IDataContext _context;
        protected ObjectContext _ObjectContext { get; set; }

        protected int _pageStart;
        protected int _pageSize;
        protected string _searchKey;

        protected BaseRepository(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
            _dbset = DataContext.DbSet<T>();

        }

        public virtual IQueryable<T> Query
        {
            get { return _dbset; }
        }

        public IDataContext DataContext
        {
            get { return _context ?? (_context = _databaseFactory.Get()); }
        }

        protected string EntitySetName { get; set; }

        public void Dispose()
        {
            DataContext.ObjectContext().Dispose();
        }

        public virtual void AddOrUpdate(T entity)
        {
            if (UnitOfWork.IsPersistent(entity))
            {
                DataContext.Entry(entity).State = EntityState.Modified;
            }
            else
                _dbset.Add(entity);
        }

        public virtual T Create()
        {
            var entity = _dbset.Create();
            entity.Created = DateTimeOffset.UtcNow;
            return _dbset.Create();
        }

        public virtual void Add(T entity)
        {
            _dbset.Add(entity);
        }

        public virtual T GetById(int id)
        {
            //return _dbset.Find(ConvertValue<int, string>(id.ToString(CultureInfo.InvariantCulture)));
            return _dbset.Find(id);
        }

        public static int ConvertValue<TK, TType>(TType value) where TType : IConvertible
        {
            return (int)Convert.ChangeType(value, typeof(TK));
        }

        public virtual IQueryable<T> GetAll()
        {
            return Query;
        }

        public virtual IQueryable<T> GetAllReadOnly()
        {
            return Query.AsNoTracking();
        }

        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public void Delete(int[] keys)
        {
            foreach (var key in keys)
            {
                Delete(GetById(key));
            }
            //keys.ForEach(i => Delete(GetById(i)));
        }

        public T Find()
        {
            return _dbset.Find();
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            var item = Find(predicate);
            return item.FirstOrDefault();
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Find(predicate, null);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate, int maxHits)
        {
            return Find(predicate, null, maxHits);
        }


        public virtual IQueryable<T> Find(Expression<Func<T, bool>> filter,
                                           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, int maxHits = 0)
        {
            IQueryable<T> query = _dbset;
            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = orderBy != null ? orderBy(query) : query.OrderByDescending( o => o.Created );

            return maxHits > 0 ? query.Take(maxHits) : query;
        }

        public virtual IQueryable<T> Select(
                    Expression<Func<T, bool>> filter = null,
                    Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = _dbset;

            if (orderBy != null)
                query = orderBy(query);

            if (filter != null)
                query = query.Where(filter);

            foreach (var includeProperty in includeProperties.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.AsQueryable();
        }

        public virtual IQueryable<T> Select(
                  Expression<Func<T, bool>> filter = null,
                  Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                  int page = 0, int pageSize = 0, string includeProperties = "")
        {
            IQueryable<T> query = _dbset;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            if (page > 0 && pageSize > 0)
            {
                query = query.Skip(pageSize * (page - 1)).Take(pageSize);
            }

            foreach (var includeProperty in includeProperties.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.AsQueryable();
        }

        public virtual IQueryable<T> Select(int maxHits = 100, Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            var query = Select(filter, orderBy, 0, 0, string.Empty);
            if (maxHits > 0)
                query = query.Take(maxHits);

            return query;
        }

        public virtual T GetSingle()
        {
            return _dbset.Find();
        }

        public virtual T GetSingle(Expression<Func<T, bool>> filter)
        {
            return _dbset.SingleOrDefault(filter);
        }

        public IEnumerable<T> SelectIn(Expression<Func<T, int>> valueSelector, IEnumerable<int> values)
        {
            return GetAll().Where(BuildContainsExpression<T, int>(valueSelector, values));
        }

        public IEnumerable<T> SelectIn(Expression<Func<T, string>> valueSelector, IEnumerable<string> values)
        {
            return GetAll().Where(BuildContainsExpression<T, string>(valueSelector, values));
        }

        public Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(Expression<Func<T, TValue>> valueSelector, IEnumerable<TValue> values)
        {
            if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }
            if (null == values) { throw new ArgumentNullException("values"); }
            ParameterExpression p = valueSelector.Parameters.Single();
            var enumerable = values as TValue[] ?? values.ToArray();
            if (!enumerable.Any())
            {
                return e => false;
            }
            var equals = enumerable.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate(Expression.Or);
            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }

        public long Count()
        {
            return _dbset.LongCount();
        }

        public long Count(Expression<Func<T, bool>> filter)
        {
            return filter != null ? _dbset.Where(filter).LongCount() : Count();
        }

        public T GetByKey(object[] keys)
        {
            return _dbset.Find(keys);
        }

        public void SetPaging(int pageStart, int pageSize)
        {
            _pageStart = pageStart;
            _pageSize = pageSize;
        }

        public void SetSearchKey(string searchKey)
        {
            _searchKey = searchKey;
        }

        private EntityKey GeTKey(object keyValue)
        {
            var entitySetName = GetName();
            var objectSet = ((IObjectContextAdapter)_context).ObjectContext.CreateObjectSet<T>();
            var keyPropertyName = objectSet.EntitySet.ElementType.KeyMembers[0].ToString();
            var entityKey = new EntityKey(entitySetName, new[] { new EntityKeyMember(keyPropertyName, keyValue) });
            return entityKey;
        }

        private string GetName()
        {
            return string.Format("{0}.{1}", ((IObjectContextAdapter)_context).ObjectContext.DefaultContainerName, typeof(T).Name);
        }

    }
}