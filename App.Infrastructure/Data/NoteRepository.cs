using App.Core.Interfaces.Data;
using App.Core.Models;

namespace App.Infrastructure.Data
{
    public class NoteRepository : BaseRepository<Note>, INoteRepository
    {
		public NoteRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}