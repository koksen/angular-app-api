using App.Core.Interfaces.Data;
using App.Core.Models;

namespace App.Infrastructure.Data
{
    public class ThingRepository : BaseRepository<Thing>, IThingRepository
    {
		public ThingRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}