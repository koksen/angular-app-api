using App.Core.Interfaces.Data;
using App.Core.Models;

namespace App.Infrastructure.Data
{
    public class UserProfileRepository : BaseRepository<UserProfile>, IUserProfileRepository
    {
		public UserProfileRepository(IDatabaseFactory databaseFactory)
	        : base(databaseFactory)
	    {
	    }
    }
}