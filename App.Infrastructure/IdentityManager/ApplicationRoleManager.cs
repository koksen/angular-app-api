﻿using App.Core.Models;
using App.Infrastructure.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace App.Infrastructure.IdentityManager
{
    public class ApplicationRoleManager : RoleManager<Role, int>
    {

        // Configure the application user manager
        public ApplicationRoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
          
        }

        public static RoleManager<Role, int> Create(IOwinContext context)
        {
            var manager = new RoleManager<Role, int>(new RoleStore(context.Get<DataContext>()));
            
            return manager;
        }
    }
}