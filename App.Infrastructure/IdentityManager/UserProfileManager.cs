﻿using System;
using App.Core.Models;
using App.Infrastructure.Data;
using App.Infrastructure.Messaging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace App.Infrastructure.IdentityManager
{
    public class UserProfileManager : UserManager<User, int>
    {

        //private IIdentityMessageService _identityMessageService;
        // Configure the application user manager
        public UserProfileManager(IUserStore<User, int> store)
            : base(store)
        {
            //, IIdentityMessageService identityMessageService
            //_identityMessageService = identityMessageService;
        }

        public static UserProfileManager Create(IdentityFactoryOptions<UserProfileManager> options, IOwinContext context)
        {
            var manager = new UserProfileManager(new UserStore(context.Get<DataContext>()));
            //var manager = new UserProfileManager(new AzureStore<User>());
            manager.UserValidator = new UserValidator<User,int>(manager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = false,
            };
            
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application 
            // uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider(
                "PhoneCode",
                new PhoneNumberTokenProvider<User, int>
                {
                    MessageFormat = "Your security code is: {0}"
                });

            manager.RegisterTwoFactorProvider(
                "EmailCode",
                new EmailTokenProvider<User, int>
                {
                    Subject = "SecurityCode",
                    BodyFormat = "Your security code is {0}"
                });

            manager.EmailService = new IdentityEmailService();
            manager.SmsService = new IdentitySmsService();

            var dataProtectionProvider = options.DataProtectionProvider ?? new DpapiDataProtectionProvider("Angular");
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<User, int>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}