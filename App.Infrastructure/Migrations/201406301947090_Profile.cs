using System.Data.Entity.Migrations;

namespace App.Infrastructure.Migrations
{
    public partial class Profile : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfile", "UserId", "dbo.User");
            AlterColumn("dbo.UserProfile", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.UserProfile", "LastName", c => c.String());
            AddForeignKey("dbo.UserProfile", "UserId", "dbo.User", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfile", "UserId", "dbo.User");
            AlterColumn("dbo.UserProfile", "LastName", c => c.String(maxLength: 80));
            AlterColumn("dbo.UserProfile", "FirstName", c => c.String(nullable: false, maxLength: 20));
            AddForeignKey("dbo.UserProfile", "UserId", "dbo.User", "UserId");
        }
    }
}
