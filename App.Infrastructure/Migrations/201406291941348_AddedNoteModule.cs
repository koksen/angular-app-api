using System.Data.Entity.Migrations;

namespace App.Infrastructure.Migrations
{
    public partial class AddedNoteModule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Note",
                c => new
                    {
                        NoteId = c.Int(nullable: false, identity: true),
                        UserFromId = c.Int(),
                        UserToId = c.Int(),
                        Type = c.Int(nullable: false),
                        Title = c.String(),
                        Tags = c.String(),
                        NoteTypeId = c.Int(),
                        Description = c.String(nullable: false),
                        SeenDateTime = c.DateTimeOffset(precision: 7),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.NoteId)
                .ForeignKey("dbo.User", t => t.UserFromId)
                .ForeignKey("dbo.User", t => t.UserToId)
                .Index(t => t.UserFromId)
                .Index(t => t.UserToId)
                .Index(t => t.NoteTypeId, name: "IX_NOTE_TYPE");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Note", "UserToId", "dbo.User");
            DropForeignKey("dbo.Note", "UserFromId", "dbo.User");
            DropIndex("dbo.Note", "IX_NOTE_TYPE");
            DropIndex("dbo.Note", new[] { "UserToId" });
            DropIndex("dbo.Note", new[] { "UserFromId" });
            DropTable("dbo.Note");
        }
    }
}
