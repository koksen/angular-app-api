using System.Data.Entity.Migrations;
using App.Infrastructure.Data;
using App.Infrastructure.Seeders;

namespace App.Infrastructure.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
            AppDataSeeder.Seed(context);
        }
    }
}
