using System;
using System.Data.Entity;
using System.Web.Http;
using App.Core.Interfaces.Config.Meta;
using App.Core.Interfaces.Config.Params;
using App.Core.Interfaces.Config.Security;
using App.Core.Interfaces.Data;
using App.Core.Interfaces.Infrastructure;
using App.Core.Interfaces.Messaging;
using App.Core.Interfaces.Services;
using App.Core.Models;
using App.Core.Services;
using App.Infrastructure.Config;
using App.Infrastructure.Data;
using App.Infrastructure.Helpers;
using App.Infrastructure.IdentityManager;
using App.Infrastructure.Messaging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;
using SimpleInjector.Integration.Web;

namespace App.DependencyResolution
{
    public static class SimpleInjectorStartup
    {

        private static Container _container;
        /// <summary>
        /// Starts the application
        /// </summary>
        public static Container Start(IAppBuilder appBuilder, Func<object> service = null) //
        {
            var container = new Container();
            _container = container;

            var services = GlobalConfiguration.Configuration.Services;
            var controllerTypes = services.GetHttpControllerTypeResolver().GetControllerTypes(services.GetAssembliesResolver());

            foreach (var controllerType in controllerTypes)
            {
                container.Register(controllerType);
            }
            if (service!=null)
                container.RegisterSingle(service);

            RegisterServices(container);

            return container;           

        }

        public static Container Container()
        {
            return _container;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="container">The container.</param>
        private static void RegisterServices(Container container)
        {
            var hybridLifestyle = Lifestyle.CreateHybrid(
                () => container.GetCurrentExecutionContextScope() != null,
                new ExecutionContextScopeLifestyle(),
                new WebRequestLifestyle());

          
            // infrastructure
            container.RegisterPerWebRequest<IUnitOfWork, UnitOfWork>();
            container.RegisterPerWebRequest<IDataContext, DataContext>();
            container.RegisterPerWebRequest<IDatabaseFactory, DatabaseFactory>();
            container.RegisterPerWebRequest<DbContext, DataContext>();

            //container.Register<UserManager<User, int>, UserProfileManager>();
            container.Register<RoleManager<Role, int>, ApplicationRoleManager>();

            container.Register<IUserStore<User, int>,
                UserStore<User, Role, int, UserLogin, UserRole, UserClaim>>();

            container.Register<IRoleStore<Role, int>, RoleStore<Role, int, UserRole>>();

         
            //App context
            container.RegisterSingle<IAppContext, AppContext>();

            //container.Register<IAuthenticationService, AuthenticationService>();
            container.RegisterPerWebRequest<IUserProfileService, UserProfileService>();
            container.RegisterPerWebRequest<IUserProfileRepository, UserProfileRepository>();
            container.RegisterPerWebRequest<IUserSessionService, UserSessionService>();
            container.RegisterPerWebRequest<IUserSessionRepository, UserSessionRepository>();
            container.RegisterPerWebRequest<IThingService, ThingService>();
            container.RegisterPerWebRequest<IThingRepository, ThingRepository>();
            container.RegisterPerWebRequest<INoteService, NoteService>();
            container.RegisterPerWebRequest<INoteRepository, NoteRepository>();

            container.RegisterPerWebRequest<ILogService, LogService>();
            container.RegisterPerWebRequest<ILogRepository, LogRepository>();


            // Messaging Configuration
            container.RegisterSingle(AppConfig.Instance.Messaging.Apple.Current);
            container.RegisterSingle(AppConfig.Instance.Messaging.Google.Current);
            container.RegisterSingle(AppConfig.Instance.Messaging.Sms.Current);
            container.RegisterSingle(AppConfig.Instance.Messaging.Mailer.Current);


            //// settings
            container.RegisterSingle<IParamsSettings>(AppConfig.Instance.Params);
            container.RegisterSingle<IMetaSettings>(AppConfig.Instance.Meta);
            container.RegisterSingle<ISecuritySettings>(AppConfig.Instance.Security);

            // Messaging services
            container.RegisterPerWebRequest<IPushNotificationService, PushNotificationService>();
            container.RegisterPerWebRequest<ISmsService, SmsService>();
            container.RegisterPerWebRequest<IEmailService, EmailMessageService>();
            container.RegisterPerWebRequest<IIdentityMessageService, IdentityEmailService>();
        }        
    }
}
