using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SimpleInjector;

namespace App.DependencyResolution
{
    public sealed class SignalRSimpleInjectorDependencyResolver : DefaultDependencyResolver
    {
        private readonly Container _container;

        public SignalRSimpleInjectorDependencyResolver(Container container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            _container = container;
        }

        [DebuggerStepThrough]
        public override object GetService(Type serviceType)
        {
            //return _container.GetInstance(serviceType) ?? base.GetService(serviceType);
            return ((IServiceProvider)_container).GetService(serviceType) ?? base.GetService(serviceType);
        }

        [DebuggerStepThrough]
        public override IEnumerable<object> GetServices(Type serviceType)
        {
            //IEnumerable<object> instances = _container.GetAllInstances(serviceType);
            //IEnumerable<object> instances2 = base.GetServices(serviceType);

            //return instances2.Concat(instances);
            return _container.GetAllInstances(serviceType).Concat(base.GetServices(serviceType)); 
        }

    }
}