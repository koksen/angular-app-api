﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="KriaSoft LLC">
//   Copyright © 2014 Konstantin Tarkus, KriaSoft LLC.  See LICENSE.txt
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;

[assembly: AssemblyTitle("App.Core")]
[assembly: AssemblyDescription("App Core Library")]