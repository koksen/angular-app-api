#region credits
// ***********************************************************************
// Assembly	: Flext.Core
// Author	: Victor Cardins
// Created	: 02-24-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-21-2013
// ***********************************************************************
#endregion

using System.Collections.Generic;
using App.Core.Interfaces.Paging;
using App.Core.Models.Common;

namespace App.Core.Common.Paging
{
    #region

    

    #endregion

    public class Paging<T> : IPage<T> where T : BaseModel
    {
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }
        public IEnumerable<T> Entities { get; set; }

        public Paging(IEnumerable<T> entities, int count, int pageSize, int currentPage)
        {
            Entities = entities;
            Count = count;
            CurrentPage = currentPage;
            PageSize = pageSize;
            PagesCount = count <= pageSize ? 1 : (count / pageSize) + 1;
        }

        public Paging()
        {
        }
    } 
}