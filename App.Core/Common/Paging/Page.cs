using System.Collections.Generic;
using App.Core.Interfaces.Paging;
using App.Core.Models.Common;

namespace App.Core.Common.Paging
{

    public class Page<T> : IPage<T> where T : BaseModel
    {
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }
        public IEnumerable<T> Entities { get; set; }

        public Page(IEnumerable<T> entities, int count, int pageSize, int currentPage)
        {
            Entities = entities;
            Count = count;
            CurrentPage = currentPage;
            PageSize = pageSize;
            PagesCount = count <= pageSize ? 1 : (count / pageSize) + 1;
        }

        public Page()
        {
        }
    } 
}