#region credits
// ***********************************************************************
// Assembly	: TechsApp.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.Configuration;

namespace App.Core.Common.Images.Interfaces
{
    #region

    

    #endregion

    public interface IImageResize
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [ConfigurationProperty("name")]
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        [ConfigurationProperty("width")]
        int Width { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [ConfigurationProperty("height")]
        int Height { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [ConfigurationProperty("enabled")]
        bool Enabled { get; set; }
    }
}