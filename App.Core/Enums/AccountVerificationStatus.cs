#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 02-24-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.ComponentModel;

namespace App.Core.Enums
{

    public enum AccountVerificationStatus
    {
        [Description("Your account was verified successfully.")]
        Success,

        [Description("Verification code is required.")]
        MissingCode,

        [Description("Sorry, your account is closed. Contact System Administrator for more information.")]
        AccountClosed,

        [Description("Sorry, you are not allowed to login at this moment.")]
        LoginNotAllowed,

        [Description("Two factor auth mode not mobile.")]
        TwoFactorAuthNotMobile,

        [Description("Current auth status not mobile.")]
        CurrentTwoFactorAuthStatusNotMobile,

        [Description("Mobile code failed to verify.")]
        WrongVerificationCode,

        [Description("Invalid Username.")]
        InvalidUsername
        
    }
}