﻿namespace App.Core.Enums
{
    public enum AccessMedia
    {
        Web  = 1, 
        Mobile = 2
    }
}