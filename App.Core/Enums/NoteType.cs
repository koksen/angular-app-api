using System.ComponentModel;

namespace App.Core.Enums
{

    public enum NoteType
    {
        [Description("General")]
        General,

        [Description("Confirm Email")]
        Email,

        [Description("Friend Request")]
        FriendRequest,

        [Description("Task Action")]
        TaskAction,
        
    }
}