namespace App.Core.Enums
{
    public enum DataCacheKey
    {
        LastTweet,
        PaymentType,
        PaymentOption,
        CompanyCodeColor,
        TicketStatus,
        CancellationReason,
        Task,
        TaskCategory,
        TaskSubCategory,
        TaskGroup,
        TaskChain,
        Ticket,
        TicketClosed,
        TicketCompleted,
        TicketCancelled,
        UserActivity,
        LeadSource,
        CommissionType,
        DateRange,
        AppUser
    }

}