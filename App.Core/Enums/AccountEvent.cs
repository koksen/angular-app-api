using System.ComponentModel;

namespace App.Core.Enums
{

    public enum AccountEvent
    {
        [Description("Password Change")]
        ChangePassword,

        [Description("Confirm Email")]
        ConfirmEmail,

        [Description("Confirm Email Result")]
        ConfirmEmailResult,

        [Description("Delete Account")]
        DeleteAccount,

        [Description("Forgot Password")]
        ForgotPassword,

        [Description("Logout")]
        Logout,

        [Description("Account Registration")]
        Register,

        [Description("Password Reset")]
        ResetPassword,

        [Description("Resend Confirmation Email")]
        ResendConfirmationEmail
        
    }
}