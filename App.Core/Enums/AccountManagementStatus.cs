#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 02-24-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.ComponentModel;

namespace App.Core.Enums
{

    public enum AccountManagementStatus
    {
        [Description("Your account was created successfully.")]
        SuccessOnCreation,

        [Description("Your account was updated successfully.")]
        SuccessOnUpdating,

        [Description("Username is invalid.")]
        InvalidUsername,

        [Description("Password is invalid.")]
        InvalidPassword,

        [Description("SecurityPin is invalid.")]
        InvalidSecurityPin,

        [Description("Email is invalid.")]
        InvalidEmail,

        [Description("Phone is invalid.")]
        InvalidPhone,

        [Description("Missing Tenant.")]
        MissingTenant,

        [Description("Password is required.")]
        MissingPassword,

        [Description("Username is required.")]
        MissingUsername,

        [Description("Your account is not activated yet.")]
        AccountNotActivated,

        [Description("Email already in use")]
        EmailAlreadyInUse,

        [Description("Phone number already in use")]
        PhoneAlreadyInUse,

        [Description("User not found")]
        UserNotFound        
        
    }
}