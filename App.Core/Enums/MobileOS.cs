﻿namespace App.Core.Enums
{
    public enum MobileOS
    {
        iOS,
        Android,
        BlackBerry,
        Windows,
        Other
    }
}