﻿namespace App.Core.Enums
{
    public enum UserLevel
    {
        Guest  = 1, // 001
        User   = 2, // 010
        Admin  = 4 
    }
}