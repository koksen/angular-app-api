﻿namespace App.Core.Enums
{
    public enum ViewInfoStatus
    {
        Error = 0,
        Success = 1,
        Warning = 2
    }
}