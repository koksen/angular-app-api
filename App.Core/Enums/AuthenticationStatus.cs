#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 02-24-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.ComponentModel;

namespace App.Core.Enums
{

    public enum AuthenticationStatus
    {
        [Description("Your account was authenticated successfully.")]
        Authenticated,

        [Description("Verification key is missing.")]
        MissingVerificationKey,

        [Description("Verification keys does not match.")]
        NotMatchingVerificationKey,

        [Description("Your account is activated.")]
        AccountAlreadyVerified,

        [Description("Your account is not activated.")]
        AccountVerified,

        [Description("Your account is not activated yet.")]
        AccountNotVerified,

        [Description("Invalid username or password.")]
        InvalidUsernameOrPassword,

        [Description("Invalid pin.")]
        InvalidPin,

        [Description("Password has expired.")]
        PasswordExpired,

        [Description("Account not allowed to login.")]
        LoginNotAllowed,

        [Description("Account is locked out.")]
        AccountLockedOut,

        [Description("Your account is no longer active.")]
        AccountClosed,

        [Description("Password is required.")]
        MissingPassword,

        [Description("Username is required.")]
        MissingUsername,

        [Description("Tenant is required.")]
        MissingTenant,

        [Description("The new password must be different than the old password.")]
        NewPasswordMustDifferFromOld,

        [Description("Mobile authentication not allowed..")]
        AccountTwoFactorAuthModeNotMobile,

        [Description("Missing mobile phone number..")]
        MissingMobilePhoneNumber
        
    }
}