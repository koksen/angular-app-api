﻿using System.Runtime.Serialization;
using App.Core.Enums;

namespace App.Core.ViewModels
{
    [DataContract]
    public class UserSessionRequest
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public AccessMedia Media { get; set; }        
        public string UserAgent { get; set; }
        public string Ip { get; set; }
        public string DeviceId { get; set; }

    }
}
