﻿namespace App.Core.ViewModels
{
    public class UnlockRequest
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
