﻿using System;
using System.Runtime.Serialization;

namespace App.Core.ViewModels
{
    [DataContract]
    public abstract class BaseViewModel
    {
        //[DataMember]
        public int Id { get; set; }

        //[NotInjected]
        [DataMember]
        public DateTimeOffset Created { get; set; }

        [DataMember]
        public DateTimeOffset? Updated { get; set; }

    }
}
