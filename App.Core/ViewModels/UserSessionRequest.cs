﻿using System;
using System.Runtime.Serialization;

namespace App.Core.ViewModels
{
    [DataContract]
    public class UserSessionSummary : BaseViewModel
    {
        public int UserId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        public string WebUserAgent { get; set; }
        public string MobileUserAgent { get; set; }
        public string WebIp { get; set; }
        public string MobileIp { get; set; }
        public string MobileDeviceId { get; set; }
        [DataMember]
        public DateTimeOffset? WebLastLogin { get; set; }
        [DataMember]
        public DateTimeOffset? MobileLastLogin { get; set; }
        [DataMember]
        public DateTimeOffset? WebLockDateTime { get; set; }
        [DataMember]
        public DateTimeOffset? MobileLockDateTime { get; set; }
    }
}
