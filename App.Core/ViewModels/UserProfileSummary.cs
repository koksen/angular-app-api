﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using App.Core.ViewModels.Common;

namespace App.Core.ViewModels
{
    [DataContract]
    public class UserProfileSummary : BaseViewModel
    {

        public UserProfileSummary()
        {
            Address = new AddressSummary();
        }

        public int UserId { get; set; }

        [DataMember]
        public int? ParentUserId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        [Required]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public AddressSummary Address { get; set; }

        [DataMember]
        public string PhotoID { get; set; }

        [DataMember]
        public string FullName { get { return FirstName + " " + LastName; } }

        [DataMember]
        public string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(FirstName))
                {
                    return
                        string.Format("{0} {1}.", FirstName,
                            !string.IsNullOrEmpty(LastName) ? LastName.Substring(0, 1) : "").Trim();
                }
                return UserName;
            }
        }

        [DataMember]
        public List<RoleSummary> Roles { get; set; }
    }
}
