﻿using System.Data.Entity.Spatial;
using System.Runtime.Serialization;
using App.Core.Converters;
using Newtonsoft.Json;

namespace App.Core.ViewModels.Common
{
    [DataContract]
    public class AddressSummary
    {
        [DataMember]
        public string FullAddress { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public double TimezoneOffset { get; set; }

        [DataMember]
        [JsonConverter(typeof(DbGeographyConverter))]
        public DbGeography GeoLocation { get; set; }
    }
}
