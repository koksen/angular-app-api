﻿
namespace App.Core.ViewModels.Common
{
    public class NoteStatusUpdate
    {
        public int NoteId { get; set; }

        public int UserId { get; set; }
        public bool Seen { get; set; }

    }
}
