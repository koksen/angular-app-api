﻿using System;
using System.Runtime.Serialization;

namespace App.Core.ViewModels.Common
{
    public class NoteSummary : BaseViewModel
    {
        [DataMember]
        public int NoteId { get; set; }
        [DataMember]
        public int? UserFromId { get; set; }
        public string UserFrom { get; set; }
        [DataMember]
        public int? UserToId { get; set; }
        public string UserTo { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public int? NoteTypeId { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTimeOffset? SeenDateTime { get; set; }
    }
}
