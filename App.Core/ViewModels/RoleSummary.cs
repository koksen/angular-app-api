﻿using System.Runtime.Serialization;

namespace App.Core.ViewModels
{
    [DataContract]
    public class RoleSummary
    {
        [DataMember]
        public int BitMask { get; set; }

        [DataMember]
        public string Title { get; set; }

    }
}
