﻿namespace App.Core.ViewModels
{
    public class ThingSummary : BaseViewModel
    {
        public int ThingId { get; set; }

        public string Name { get; set; }
    }
}
