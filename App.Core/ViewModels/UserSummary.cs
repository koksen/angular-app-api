﻿using System.Collections.Generic;
using System.Security.Claims;
using App.Core.Models;

namespace App.Core.ViewModels
{
    public class UserSummary : BaseViewModel
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public ICollection<Role> Roles { get; set; }

        public ICollection<Claim> Claims { get; set; }

    }
}
