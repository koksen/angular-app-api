﻿namespace App.Core.ViewModels
{
    public class UnlockResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
