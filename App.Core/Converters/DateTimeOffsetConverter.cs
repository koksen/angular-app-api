﻿using System;
using Newtonsoft.Json;

namespace App.Core.Converters
{
    public class DateTimeOffsetConverter2 : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTimeOffset);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            //var value = reader.Value;
            var jsonSerializerSettings = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat, 
                    DateParseHandling = DateParseHandling.DateTimeOffset, 
                    DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind
                };

            //var converted = JsonConvert.DeserializeObject(reader.Value.ToString(), jsonSerializerSettings);
            var converted = (DateTimeOffset)reader.Value;
            return converted.AddMilliseconds(-converted.Millisecond);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // Base serialization is fine 
            serializer.Serialize(writer, value);
        }
    }
}