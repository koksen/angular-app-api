﻿using System;
using System.Data.Entity.Spatial;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace App.Core.Converters
{
    public class DbGeographyConverter : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DbGeography);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return default(DbGeography);

            if (reader.TokenType == JsonToken.PropertyName || reader.TokenType == JsonToken.StartObject)
            {
                JObject location = JObject.Load(reader);

                JToken lat;
                JToken lng;
                location.TryGetValue("latitude", out lat);
                location.TryGetValue("longitude", out lng);

                if (lat != null && lng != null)
                {
                    var point = string.Format("POINT ({0} {1})", lng, lat);
                    var result = DbGeography.PointFromText(point, 4326);
                    return result;
                }               
            }

            if (reader.TokenType == JsonToken.String)
            {
                var value = reader.Value.ToString();
                var latLongStr = value.Split(',');
                if (latLongStr.Length != 2)
                    return null;

                //Are we supposed to populate ModelState with errors here if we can't conver the value to a point?
                var point = string.Format("POINT ({0} {1})", latLongStr[0], latLongStr[1]);
                //4326 format puts LONGITUDE first then LATITUDE
                var result = DbGeography.FromText(point, 4326);
                return result;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var dbGeo = (DbGeography) value;
            var coords = new {dbGeo.Latitude, dbGeo.Longitude};
            // Base serialization is fine
            serializer.Serialize(writer, coords);
        }
    }

}