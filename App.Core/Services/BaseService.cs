using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using App.Core.Interfaces.Common.Validation;
using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Interfaces.Validation;
using App.Core.Models.Common;

namespace App.Core.Services
{
    /// <summary>
    /// Base for all services... If you need specific businesslogic
    /// override these methods in inherited classes and implement the logic there.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseService<T> : IService<T> where T : BaseModel
    {

        protected IRepository<T> Repository;

        protected IUnitOfWork UnitOfWork;

        protected BaseService()
        {
        }

        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public double TimezoneOffset { get; set; }

        public virtual T Create()
        {
            var entity = Repository.Create();
            entity.Created = DateTimeOffset.UtcNow;
            return entity;
        }

        public virtual IQueryable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public virtual IQueryable<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            var items = Find(null, orderBy, int.MaxValue);
            return items.AsQueryable();
        }

        public virtual IQueryable<T> GetAllReadOnly()
        {
            return Repository.GetAllReadOnly();
        }

        public virtual T GetById(int id)
        {
            return Repository.GetById(id);
        }

        public virtual IValidationContainer<T> SaveOrUpdate(T entity)
        {
            var validation = entity.GetValidationContainer();
            if (!validation.IsValid)
                return validation;

            Repository.AddOrUpdate(entity);
            UnitOfWork.Commit();
            return validation;
        }


        public virtual IValidationContainer<T> Insert(T entity)
        {
            var validation = entity.GetValidationContainer();
            if (!validation.IsValid)
                return validation;

            Repository.Add(entity);
            UnitOfWork.Commit();
            return validation;
        }

        public virtual void Delete(T entity)
        {
            Repository.Delete(entity);
            UnitOfWork.Commit();
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            Delete(entity);
        }

        public void Delete(List<int> keys)
        {
            keys.ForEach(Delete);
        }

        public void Delete(int[] keys)
        {
            Repository.Delete(keys);
            UnitOfWork.Commit();
        }


        public T Single(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            var query = Repository.Find(expression);

            if (includes != null)
            {
                foreach (var i in includes)
                {
                    query.Include(i);
                }
            }

            return query.AsNoTracking().FirstOrDefault();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression, int maxHits = 100)
        {
             var items = Repository.Find(expression, null, maxHits);
            return items;
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, int maxHits = 100)
        {
            var items = Repository.Find(expression, orderBy, maxHits);
            return items;
        }

        public IEnumerable<T> FindIn(Expression<Func<T, int>> valueSelector, IEnumerable<int> values)
        {
            return Repository.SelectIn(valueSelector, values);
        }

        public IEnumerable<T> FindIn(Expression<Func<T, string>> valueSelector, IEnumerable<string> values)
        {
            return Repository.SelectIn(valueSelector, values);
        }

        public long Count()
        {
            return Repository.Count();
        }

        public long Count(Expression<Func<T, bool>> expression)
        {
            return Repository.Count(expression);
        }

        public void SetSearchKey(string searchKey)
        {
            Repository.SetSearchKey(searchKey);
        }

        public bool IsUnique(string property, int value)
        {
            var entity = GetAll().FirstOrDefault(x => x.Id == value);
            return entity == null;
        }

        public string MetaData()
        {
            return UnitOfWork.Metadata();
        }
    }
}
