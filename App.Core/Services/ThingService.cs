using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;
using Omu.ValueInjecter;

namespace App.Core.Services
{
    public partial class ThingService : BaseService<Thing>, IThingService
    {
        protected IThingRepository _entityRepository;

        public ThingService(IUnitOfWork unitOfWork, IThingRepository entityRepository)
            : base(unitOfWork)
        {
            Repository = _entityRepository = entityRepository;
        }

        public IValidationContainer<Thing> AddOrUpdate(ThingSummary model)
        {
            var entity = Single(x => x.ThingId == model.ThingId) ?? Create(); 
            var isNew = entity.ThingId == 0;

            entity.InjectFrom(model);

            var validator = isNew ? Insert(entity) : SaveOrUpdate(entity);
            return validator;
        }

    }
}