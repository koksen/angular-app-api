using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Models.Common;

namespace App.Core.Services
{
    public class LogService : BaseService<Log>, ILogService
    {
        protected ILogRepository _logRepository;

        public LogService(IUnitOfWork unitOfWork, ILogRepository logRepository)
            : base(unitOfWork)
        {
            Repository = _logRepository = logRepository;
        }

    }
}