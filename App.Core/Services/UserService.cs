using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;
using Omu.ValueInjecter;

namespace App.Core.Services
{
    public partial class UserService : BaseService<User>, IUserService
    {
        protected IUserRepository _userRepository;

        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository)
            : base(unitOfWork)
        {
            Repository = _userRepository = userRepository;
        }


    }
}