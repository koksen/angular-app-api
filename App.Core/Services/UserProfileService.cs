using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using App.Core.Enums;
using App.Core.Extensions;
using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;
using Omu.ValueInjecter;

namespace App.Core.Services
{
    public class UserProfileService : BaseService<UserProfile>, IUserProfileService
    {
        protected IUserProfileRepository _userProfileRepository;

        public UserProfileService(IUnitOfWork unitOfWork, IUserProfileRepository userProfileRepository)
            : base(unitOfWork)
        {
            Repository = _userProfileRepository = userProfileRepository;
        }

        public IValidationContainer<UserProfile> AddOrUpdate(UserProfileSummary model)
        {
            var userProfile = Single(x => x.UserId == model.UserId) ?? Create(); //, x => x.User
            var isNew = userProfile.UserId == 0;

            userProfile.InjectFrom(model);
            userProfile.Address.InjectFrom(model.Address);

            var validator = isNew ? Insert(userProfile) : SaveOrUpdate(userProfile);
            return validator;
        }

        public string SetProfilePicture(string userName, string resizeName, string pictureId)
        {
            var user = Single(x => x.User.UserName == userName, i => i.User); //, x => x.User
            if (user == null)
                return null;


            user.PhotoID = pictureId;
            var validator = SaveOrUpdate(user);

            return validator.IsValid ? user.PhotoID : string.Empty;
        }

        public UserProfileSummary GetSummary(User user)
        {
            if (user == null) return null;

            var userProfile = GetById(user.Id);
            var summary = new UserProfileSummary();
            summary = (UserProfileSummary)summary.InjectFrom(user);

            if (userProfile != null)
            {
                summary.InjectFrom(userProfile);
                summary.Address.InjectFrom(userProfile.Address);                
            }
            summary.Roles = LoadRoleSummary(user);

            return summary;
        }

        private static List<RoleSummary> LoadRoleSummary(User user)
        {
            var roles = user.GetClaimValues(ClaimTypes.Role).ToList();
            var userRoles = new List<RoleSummary>();
            foreach (var r in roles)
            {
                UserLevel result;
                userRoles.Add(Enum.TryParse(r, out result)
                    ? new RoleSummary { BitMask = (int)result, Title = r }
                    : new RoleSummary { BitMask = (int)UserLevel.User, Title = "User" });
            }

            return userRoles;
        }
    }
}