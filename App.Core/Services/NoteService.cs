using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Filters;
using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Models;
using App.Core.ViewModels.Common;
using LinqKit;
using Omu.ValueInjecter;

namespace App.Core.Services
{
    public class NoteService : BaseService<Note>, INoteService
    {
        protected INoteRepository _entityRepository;

        public NoteService(IUnitOfWork unitOfWork, INoteRepository entityRepository)
            : base(unitOfWork)
        {
            Repository = _entityRepository = entityRepository;
        }

        private IEnumerable<NoteSummary> GetNotes(int userId, bool onlyUnseen = false)
        {
            var predicate = PredicateBuilder.True<Note>();
            predicate = predicate.And(n => (n.UserFromId == userId || n.UserToId == userId) ||
                                     (n.UserFromId == null || n.UserToId == null));
            if (onlyUnseen)
            {
                predicate = predicate.And(x => x.SeenDateTime == null);
            }

            var result = _entityRepository.Query.AsExpandable().Where(predicate).OrderByDescending(n => n.Created).ThenBy(n => n.Type).ToList();
            var notes = result.Select(m => new NoteSummary
            {
                Type = m.Type.ToString()
            }.InjectFrom<NullableInjection>(m) as NoteSummary);
            return notes;

        }

        public IEnumerable<NoteSummary> GetUnseen(int userId)
        {
            var notes = GetNotes(userId, true);
            return notes.ToList();
        }

        public IEnumerable<NoteSummary> GetAll(int userId)
        {
            var notes = GetNotes(userId);
            return notes.ToList();
        }

        public bool UpdateStatus(NoteStatusUpdate model)
        {
            var note = Single(n => n.UserToId == model.UserId && n.NoteId == model.NoteId);
            if (note ==null)
                return false;

            note.SeenDateTime = (model.Seen) ? DateTimeOffset.UtcNow : (DateTimeOffset?)null;

            var validator = SaveOrUpdate(note);

            return validator.IsValid;
        }
    }
}