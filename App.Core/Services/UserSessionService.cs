using System;
using App.Core.Enums;
using App.Core.Interfaces.Data;
using App.Core.Interfaces.Services;
using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;
using Omu.ValueInjecter;

namespace App.Core.Services
{
    public partial class UserSessionService : BaseService<UserSession>, IUserSessionService
    {
        protected IUserSessionRepository _userSessionRepository;

        public UserSessionService(IUnitOfWork unitOfWork, IUserSessionRepository userSessionRepository)
            : base(unitOfWork)
        {
            Repository = _userSessionRepository = userSessionRepository;
        }

        public IValidationContainer<UserSession> AddOrUpdate(UserSessionSummary model)
        {
            var userSession = Single(x => x.UserId == model.UserId) ?? Create(); //, x => x.User
            var isNew = userSession.UserId == 0;

            userSession.InjectFrom(model);

            var validator = isNew ? Insert(userSession) : SaveOrUpdate(userSession);
            return validator;
        }

        public bool IsLocked(UserSessionRequest model)
        {
            var userSession = Single(x => x.UserId == model.UserId);
            if (userSession == null)
                return false;

            if (model.Media == AccessMedia.Mobile)
            {
                return userSession.MobileLockDateTime != null;
            }
            else
            {
                return userSession.WebLockDateTime != null;
            }

        }

        public bool Lock(UserSessionRequest model)
        {
            var userSession = Single(x => x.UserId == model.UserId) ?? Create();
            var isNew = userSession.UserId == 0;
            if (model.Media == AccessMedia.Mobile)
            {
                userSession.MobileLockDateTime = DateTimeOffset.UtcNow;
                userSession.MobileDeviceId = model.DeviceId;
                userSession.MobileIp = model.Ip;
                userSession.MobileUserAgent = model.UserAgent;
            }
            else
            {
                userSession.WebLockDateTime = DateTimeOffset.UtcNow;
                userSession.WebUserAgent = model.UserAgent;
                userSession.WebIp = model.Ip;
            }
            userSession.UserId = model.UserId;
            var validator = isNew ? Insert(userSession) : SaveOrUpdate(userSession);
            return validator.IsValid;
        }

        public UnlockResponse UnLock(UserSessionRequest model)
        {
            var unlockResponse = new UnlockResponse { Success = true };
            var userSession = Single(x => x.UserId == model.UserId);
            if (userSession.UserId == 0)
                return new UnlockResponse { Success = true };

            if (model.Media == AccessMedia.Mobile)
            {
                //if (userSession.MobileDeviceId != model.DeviceId)
                //    unlockResponse.Message = "Session can only be unlocked from the device which originated the session";
                
                //if (userSession.MobileIp != model.Ip)
                //    unlockResponse.Message = "Session can only be unlocked from the IP which originated the session";

                userSession.MobileLockDateTime = null;
            }
            else
            {
                //if (userSession.WebIp != model.Ip)
                //    unlockResponse.Message = "Session can only be unlocked from the IP which originated the session";

                userSession.WebLockDateTime = null;

            }

            if (!string.IsNullOrEmpty(unlockResponse.Message))
            {
                unlockResponse.Success = false;
                return unlockResponse;
            }

            var validator = SaveOrUpdate(userSession);
            unlockResponse.Success = validator.IsValid;
            if (!validator.IsValid)
                unlockResponse.Message = "Data could not be updated";

            return unlockResponse;
        }

    }
}