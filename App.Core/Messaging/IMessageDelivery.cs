﻿
namespace App.Core.Messaging
{
    public interface IMessageDelivery
    {
        void Send(Message msg);
    }
}
