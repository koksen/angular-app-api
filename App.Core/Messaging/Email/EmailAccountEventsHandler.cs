﻿using TechsApp.Core.Interfaces.Bus;
using TechsApp.Core.Interfaces.Messaging;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.Email
{
    public class EmailAccountEventsHandler :
        EmailNotificationEventHandler,
        IEventHandler<AccountCreatedEvent>,        
        IEventHandler<PasswordResetRequestedEvent>,
        IEventHandler<PasswordChangedEvent>,
        IEventHandler<PasswordResetSecretAddedEvent>,
        IEventHandler<PasswordResetSecretRemovedEvent>,
        IEventHandler<UsernameReminderRequestedEvent>,
        IEventHandler<AccountClosedEvent>,
        IEventHandler<AccountReopenedEvent>,
        IEventHandler<UsernameChangedEvent>,
        IEventHandler<EmailChangeRequestedEvent>,
        IEventHandler<EmailChangedEvent>,
        IEventHandler<EmailVerifiedEvent>,
        IEventHandler<MobilePhoneChangedEvent>,
        IEventHandler<MobilePhoneRemovedEvent>,
        IEventHandler<CertificateAddedEvent>,
        IEventHandler<CertificateRemovedEvent>
    {

        public EmailAccountEventsHandler(IMessageFormatter messageFormatter, IMailService mailService) 
            : base(messageFormatter, mailService)
        {
        }

        public void Handle(AccountCreatedEvent evt)
        {
            Process(evt, new { evt.InitialPassword, evt.VerificationKey });
        }
        
        public void Handle(PasswordResetRequestedEvent evt)
        {
            Process(evt, new { evt.VerificationKey });
        }

        public void Handle(PasswordChangedEvent evt)
        {
            Process(evt);
        }

        public void Handle(PasswordResetSecretAddedEvent evt)
        {
            Process(evt);
        }

        public void Handle(PasswordResetSecretRemovedEvent evt)
        {
            Process(evt);
        }
        
        public void Handle(UsernameReminderRequestedEvent evt)
        {
            Process(evt);
        }

        public void Handle(AccountClosedEvent evt)
        {
            Process(evt);
        }
        
        public void Handle(AccountReopenedEvent evt)
        {
            Process(evt, new { evt.VerificationKey });
        }

        public void Handle(UsernameChangedEvent evt)
        {
            Process(evt);
        }

        public void Handle(EmailChangeRequestedEvent evt)
        {
            Process(evt, new{evt.OldEmail, evt.NewEmail, evt.VerificationKey});
        }

        public void Handle(EmailChangedEvent evt)
        {
            Process(evt, new { evt.OldEmail, evt.VerificationKey });
        }
        
        public void Handle(EmailVerifiedEvent evt)
        {
            Process(evt);
        }

        public void Handle(MobilePhoneChangedEvent evt)
        {
            Process(evt);
        }

        public void Handle(MobilePhoneRemovedEvent evt)
        {
            Process(evt);
        }

        public void Handle(CertificateAddedEvent evt)
        {
            Process(evt, new { evt.Certificate.Thumbprint, evt.Certificate.Subject });
        }

        public void Handle(CertificateRemovedEvent evt)
        {
            Process(evt, new { evt.Certificate.Thumbprint, evt.Certificate.Subject });
        }

    }
}