﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.IO;
using TechsApp.Core.Configuration;
using TechsApp.Core.Constants;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.Email
{
    public class EmailMessageFormatter : IMessageFormatter
    {
        public class Tokenizer
        {
            public virtual string Tokenize(
                UserAccountEvent accountEvent, 
                ApplicationInformation appInfo, 
                string msg, 
                IDictionary<string, string> values)
            {
                var user = accountEvent.Account;

                msg = msg.Replace("{username}", user.Username);
                msg = msg.Replace("{email}", user.Email);
                msg = msg.Replace("{mobile}", user.MobilePhoneNumber);

                msg = msg.Replace("{applicationName}", appInfo.ApplicationName);
                msg = msg.Replace("{emailSignature}", appInfo.EmailSignature);
                msg = msg.Replace("{loginUrl}", appInfo.LoginUrl);

                if (values.ContainsKey("VerificationKey"))
                {
                    msg = msg.Replace("{confirmPasswordResetUrl}", appInfo.ConfirmPasswordResetUrl + values["VerificationKey"]);
                    msg = msg.Replace("{confirmChangeEmailUrl}", appInfo.ConfirmChangeEmailUrl + values["VerificationKey"]);
                    msg = msg.Replace("{cancelVerificationUrl}", appInfo.CancelVerificationUrl + values["VerificationKey"]);
                }

                foreach(var item in values)
                {
                    msg = msg.Replace("{" + item.Key + "}", item.Value);
                }

                return msg;
            }
        }
       
        public ApplicationInformation ApplicationInformation 
        {
            get
            {
                return _appInfo.Value;
            }
        }

        readonly Lazy<ApplicationInformation> _appInfo;
        public EmailMessageFormatter(ApplicationInformation appInfo)
        {
            if (appInfo == null) throw new ArgumentNullException("appInfo");
            _appInfo = new Lazy<ApplicationInformation>(()=>appInfo);
        }
        public EmailMessageFormatter(Lazy<ApplicationInformation> appInfo)
        {
            if (appInfo == null) throw new ArgumentNullException("appInfo");
            _appInfo = appInfo;
        }

        public Message Format(UserAccountEvent accountEvent, IDictionary<string, string> values)
        {
            if (accountEvent == null) throw new ArgumentNullException("accountEvent");
            return CreateMessage(GetSubject(accountEvent, values), GetBody(accountEvent, values));
        }

        protected virtual Tokenizer GetTokenizer(UserAccountEvent evt)
        {
            return new Tokenizer();
        }

        protected Message CreateMessage(string subject, string body)
        {
            if (subject == null || body == null) return null;
            return new Message { Subject = subject, Body = body };
        }

        protected string FormatValue(UserAccountEvent evt, string value, IDictionary<string, string> values)
        {
            if (value == null) return null;

            var tokenizer = GetTokenizer(evt);
            return tokenizer.Tokenize(evt, ApplicationInformation, value, values);
        }

        protected virtual string GetSubject(UserAccountEvent evt, IDictionary<string, string> values)
        {
            var v = FormatValue(evt, LoadSubjectTemplate(evt), values);
            return v;
        }
        protected virtual string GetBody(UserAccountEvent evt, IDictionary<string, string> values)
        {
            var v = FormatValue(evt, LoadBodyTemplate(evt), values);
            return v;
        }

        protected virtual string LoadSubjectTemplate(UserAccountEvent evt)
        {
            var t = LoadTemplate(CleanGenericName(evt.GetType()) + "_Subject");
            return t;
        }

        protected virtual string LoadBodyTemplate(UserAccountEvent evt)
        {
            var t = LoadTemplate(CleanGenericName(evt.GetType()) + "_Body");
            return t;
        }

        private static string CleanGenericName(Type type)
        {
            var name = type.Name;
            var idx = name.IndexOf('`');
            if (idx > 0)
            {
                name = name.Substring(0, idx);
            }
            return name;
        }

        static string LoadTemplate(string name)
        {            
            name = String.Format(TechsAppConstants.Email.ResourcePathTemplate, name);

            var asm = typeof(EmailMessageFormatter).Assembly;
            using (var s = asm.GetManifestResourceStream(name))
            {
                if (s == null) return null;
                using (var sr = new StreamReader(s))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
   
}
