﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.Email
{
    public class EmailEventHandler
    {
        readonly IMessageFormatter messageFormatter;
        readonly IMessageDelivery messageDelivery;

        public EmailEventHandler(IMessageFormatter messageFormatter)
            : this(messageFormatter, new SmtpMessageDelivery())
        {
        }

        public EmailEventHandler(IMessageFormatter messageFormatter, IMessageDelivery messageDelivery)
        {
            if (messageFormatter == null) throw new ArgumentNullException("messageFormatter");
            if (messageDelivery == null) throw new ArgumentNullException("messageDelivery");

            this.messageFormatter = messageFormatter;
            this.messageDelivery = messageDelivery;
        }

        public virtual void Process(UserAccountEvent evt, object extra = null)
        {
            var data = new Dictionary<string, string>();
            if (extra != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(extra))
                {
                    object obj2 = descriptor.GetValue(extra);
                    if (obj2 != null)
                    {
                        data.Add(descriptor.Name, obj2.ToString());
                    }
                }
            }

            var msg = messageFormatter.Format(evt, data);
            if (msg == null) return;

            msg.To = data.ContainsKey("NewEmail") ? 
                         data["NewEmail"] : 
                         evt.Account.Email;

            if (!String.IsNullOrWhiteSpace(msg.To))
            {
                messageDelivery.Send(msg);
            }
        }
    }
}
