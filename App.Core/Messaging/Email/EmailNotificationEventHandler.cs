﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TechsApp.Core.Interfaces.Messaging;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.Email
{
    public class EmailNotificationEventHandler
    {
        readonly IMessageFormatter _messageFormatter;
        readonly IMailService _mailService;

        public EmailNotificationEventHandler(IMessageFormatter messageFormatter, IMailService mailService) 
        {
            if (messageFormatter == null) throw new ArgumentNullException("messageFormatter");
            if (mailService == null) throw new ArgumentNullException("mailService");

            _messageFormatter = messageFormatter;
            _mailService = mailService;
        }

        public virtual void Process(UserAccountEvent evt, object extra = null)
        {
            var data = new Dictionary<string, string>();
            if (extra != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(extra))
                {
                    object obj2 = descriptor.GetValue(extra);
                    if (obj2 != null)
                    {
                        data.Add(descriptor.Name, obj2.ToString());
                    }
                }
            }

            var msg = CreateMessage(evt, data);
            if (msg == null) return;

            if (!String.IsNullOrWhiteSpace(msg.To))
            {
                _mailService.Send(msg);
            }
        }

        protected virtual Message CreateMessage(UserAccountEvent evt, IDictionary<string, string> extra)
        {
            var msg = _messageFormatter.Format(evt, extra);
            if (msg != null)
            {
                msg.To = extra.ContainsKey("NewEmail") ?
                         extra["NewEmail"] :
                         evt.Account.Email;
            }
            return msg;
        }
    }
}
