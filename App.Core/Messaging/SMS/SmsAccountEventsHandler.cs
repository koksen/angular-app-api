﻿using TechsApp.Core.Interfaces.Bus;
using TechsApp.Core.Interfaces.Messaging;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.SMS
{
    public class SmsAccountEventsHandler :
        SmsNotificationEventHandler,
        IEventHandler<VerifyAccountEvent>
        //IEventHandler<TwoFactorAuthenticationCodeNotificationEvent>
    {

        public SmsAccountEventsHandler(IMessageFormatter messageFormatter, ISmsService smsService) 
            : base(messageFormatter, smsService)
        {
        }

        public void Handle(VerifyAccountEvent evt)
        {
            Process(evt, new { evt.Code });
        }

        public void Handle(TwoFactorAuthenticationCodeNotificationEvent evt)
        {
            Process(evt, new { evt.Code });
        }

    }
}