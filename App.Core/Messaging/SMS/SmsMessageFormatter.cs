﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.IO;
using TechsApp.Core.Configuration;
using TechsApp.Core.Constants;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.SMS
{
    public class SmsMessageFormatter : IMessageFormatter
    {
        readonly Lazy<ApplicationInformation> appInfo;

        public SmsMessageFormatter(ApplicationInformation appInfo)
        {
            if (appInfo == null) throw new ArgumentNullException("appInfo");
            this.appInfo = new Lazy<ApplicationInformation>(()=>appInfo);
        }

        public SmsMessageFormatter(Lazy<ApplicationInformation> appInfo)
        {
            if (appInfo == null) throw new ArgumentNullException("appInfo");
            this.appInfo = appInfo;
        }

        public ApplicationInformation ApplicationInformation
        {
            get
            {
                return appInfo.Value;
            }
        }

        public Message Format(UserAccountEvent accountEvent, IDictionary<string, string> values)
        {
            if (accountEvent == null) throw new ArgumentNullException("accountEvent");

            var message = GetMessageBody(accountEvent, values);
            if (message == null)
                return null;

            return new Message
            {
                Subject = message,
                Body = message
            };
        }

        private string GetMessageBody(UserAccountEvent evt, IDictionary<string, string> values)
        {
            var txt = LoadTemplate(CleanGenericName(evt.GetType()));
            if (string.IsNullOrEmpty(txt))
                return null;

            txt = txt.Replace("{applicationName}", ApplicationInformation.ApplicationName);
            if (values.ContainsKey("Code"))
            {
                txt = txt.Replace("{code}", values["Code"]);
            }

            return txt;
        }

        private static string CleanGenericName(Type type)
        {
            var name = type.Name;
            var idx = name.IndexOf('`');
            if (idx > 0)
            {
                name = name.Substring(0, idx);
            }
            return name;
        }

        static string LoadTemplate(string name)
        {
            name = String.Format(TechsAppConstants.SMS.ResourcePathTemplate, name);

            var asm = typeof(SmsMessageFormatter).Assembly;
            using (var s = asm.GetManifestResourceStream(name))
            {
                if (s == null) return null;
                using (var sr = new StreamReader(s))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
  
}
