﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TechsApp.Core.Interfaces.Bus;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.SMS
{
    public abstract class _SmsNotificationEventHandler :
        //IEventHandler<AccountCreatedEvent>,
        IEventHandler<MobilePhoneChangeRequestedEvent>,
        IEventHandler<TwoFactorAuthenticationCodeNotificationEvent>
    {
        readonly IMessageFormatter messageFormatter;

        protected _SmsNotificationEventHandler(IMessageFormatter messageFormatter)
        {
            if (messageFormatter == null) throw new ArgumentNullException("messageFormatter");

            this.messageFormatter = messageFormatter;
        }

        protected abstract void Send(Message message);

        public virtual void Process(UserAccountEvent evt, object extra = null)
        {
            var data = new Dictionary<string, string>();
            if (extra != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(extra))
                {
                    object obj2 = descriptor.GetValue(extra);
                    if (obj2 != null)
                    {
                        data.Add(descriptor.Name, obj2.ToString());
                    }
                }
            }

            var msg = CreateMessage(evt, data);
            if (msg != null)
            {
                Send(msg);
            }
        }

        protected virtual Message CreateMessage(UserAccountEvent evt, IDictionary<string, string> extra)
        {
            var msg = messageFormatter.Format(evt, extra);
            if (msg != null)
            {
                msg.To = extra.ContainsKey("NewMobilePhoneNumber") ? 
                         extra["NewMobilePhoneNumber"] : 
                         evt.Account.MobilePhoneNumber;
            }
            return msg;
        }

        public void Handle(MobilePhoneChangeRequestedEvent evt)
        {
            Process(evt, new { evt.NewMobilePhoneNumber, evt.Code });
        }

        public void Handle(TwoFactorAuthenticationCodeNotificationEvent evt)
        {
            Process(evt, new { evt.Code });
        }

        //public void Handle(AccountCreatedEvent evt)
        //{
        //    Process(evt, new { evt.InitialPassword, evt.VerificationKey });
        //}
    }
    
}
