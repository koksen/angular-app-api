﻿using System;
using System.Collections.Generic;
using TechsApp.Core.Configuration;

namespace TechsApp.Core.Messaging.SMS
{
    public class _SmsAccountEventsHandler : 
        _SmsNotificationEventHandler
    {
        const string sid = "";
        const string token = "";
        const string fromPhone = "";

        public _SmsAccountEventsHandler(ApplicationInformation appInfo)
            : base(new SmsMessageFormatter(appInfo))
        {
        }

        static string BasicAuthToken
        {
            get
            {
                var val = sid + ":" + token;
                var bytes = System.Text.Encoding.UTF8.GetBytes(val);
                val = Convert.ToBase64String(bytes);
                return val;
            }
        }

        static HttpContent GetBody(Message msg)
        {
            var values = new[]
                { 
                    new KeyValuePair<string, string>("From", fromPhone),
                    new KeyValuePair<string, string>("To", msg.To),
                    new KeyValuePair<string, string>("Body", msg.Body)
                };

            return new FormUrlEncodedContent(values);
        }

        protected override void Send(Message message)
        {
           
        }
    }
}