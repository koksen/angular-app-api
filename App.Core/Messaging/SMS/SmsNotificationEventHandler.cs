﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TechsApp.Core.Interfaces.Messaging;
using TechsApp.Core.Services.Account;

namespace TechsApp.Core.Messaging.SMS
{
    public class SmsNotificationEventHandler
    {
        readonly IMessageFormatter _messageFormatter;
        readonly ISmsService _smsService;

        public SmsNotificationEventHandler(IMessageFormatter messageFormatter, ISmsService smsService) 
        {
            if (messageFormatter == null) throw new ArgumentNullException("messageFormatter");
            if (smsService == null) throw new ArgumentNullException("smsService");

            _messageFormatter = messageFormatter;
            _smsService = smsService;
        }

        public virtual void Process(UserAccountEvent evt, object extra = null)
        {
            var data = new Dictionary<string, string>();
            if (extra != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(extra))
                {
                    object obj2 = descriptor.GetValue(extra);
                    if (obj2 != null)
                    {
                        data.Add(descriptor.Name, obj2.ToString());
                    }
                }
            }

            var msg = CreateMessage(evt, data);
            if (msg == null) return;

            if (!String.IsNullOrWhiteSpace(msg.To))
            {
                _smsService.Send(msg);
            }
        }

        protected virtual Message CreateMessage(UserAccountEvent evt, IDictionary<string, string> extra)
        {
            var msg = _messageFormatter.Format(evt, extra);
            if (msg == null) return null;
            
            msg.To = extra.ContainsKey("NewMobilePhoneNumber") ?
                        extra["NewMobilePhoneNumber"] :
                        evt.Account.MobilePhoneNumber;
            
            return msg;
        }
    }
}
