﻿using System;
using System.Collections.Generic;
using TechsApp.Core.Configuration;

namespace TechsApp.Core.Messaging.SMS
{
    public class TwilloSmsEventHandler : SmsEventHandler
    {
        const string sid = "";
        const string token = "";
        const string fromPhone = "";

        public TwilloSmsEventHandler(ApplicationInformation appInfo)
            : base(new SmsMessageFormatter(appInfo))
        {
        }

        static string Url
        {
            get
            {
                return String.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/SMS/Messages", sid);
            }
        }

        static string BasicAuthToken
        {
            get
            {
                var val = sid + ":" + token;
                var bytes = System.Text.Encoding.UTF8.GetBytes(val);
                val = Convert.ToBase64String(bytes);
                return val;
            }
        }

        static HttpContent GetBody(Message msg)
        {
            var values = new[]
                { 
                    new KeyValuePair<string, string>("From", fromPhone),
                    new KeyValuePair<string, string>("To", msg.To),
                    new KeyValuePair<string, string>("Body", msg.Body)
                };

            return new FormUrlEncodedContent(values);
        }

        protected override void SendSms(Message message)
        {
            if (String.IsNullOrWhiteSpace(sid)) return;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", BasicAuthToken);
            var result = client.PostAsync(Url, GetBody(message)).Result;
            result.EnsureSuccessStatusCode();
        }
    }
}