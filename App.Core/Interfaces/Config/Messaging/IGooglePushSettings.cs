#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

namespace App.Core.Interfaces.Config.Messaging
{
    #region

    #endregion
    public interface IGooglePushSettings : IMessagingServiceSettings
    {

        /// <summary>
        /// Gets or sets the params.
        /// </summary>
        string ApplicationIdPackageName { get; set; }

        /// <summary>
        /// Gets or sets the params.
        /// </summary>
        string SenderId { get; set; }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        string AuthToken { get; set; }

    }
}