#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

namespace App.Core.Interfaces.Config.Messaging
{
    #region

    #endregion
    public interface IApplePushSettings : IMessagingServiceSettings
    {

        /// <summary>
        /// Gets or sets the certificate file.
        /// </summary>
        /// <value>
        /// The certificate file.
        /// </value>
        string CertificateFile { get; set; }

        /// <summary>
        /// Gets or sets the certificate password.
        /// </summary>
        /// <value>
        /// The certificate password.
        /// </value>
        string CertificatePassword { get; set; }

        /// <summary>
        /// Gets or sets the default sound.
        /// </summary>
        /// <value>
        /// The default sound.
        /// </value>
        string DefaultSound { get; set; }

    }
}