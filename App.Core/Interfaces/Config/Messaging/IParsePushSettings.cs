#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

namespace App.Core.Interfaces.Config.Messaging
{
    #region

    #endregion
    public interface IParsePushSettings : IMessagingServiceSettings
    {

        /// <summary>
        /// Gets or sets the application id.
        /// </summary>
        /// <value>
        /// The application id.
        /// </value>
        string ApplicationId { get; set; }


        /// <summary>
        /// Gets or sets the dotnet key.
        /// </summary>
        /// <value>
        /// The dotnet key.
        /// </value>
        string DotNetKey { get; set; }


        /// <summary>
        /// Gets or sets the rest API key.
        /// </summary>
        /// <value>
        /// The rest API key.
        /// </value>
        string RestApiKey { get; set; }


        /// <summary>
        /// Gets or sets the broadcast channel.
        /// </summary>
        /// <value>
        /// The broadcast channel.
        /// </value>
        string BroadcastChannel { get; set; }


        /// <summary>
        /// Gets or sets the API URL.
        /// </summary>
        /// <value>
        /// The API URL.
        /// </value>
        string ApiUrl { get; set; }

    }
}