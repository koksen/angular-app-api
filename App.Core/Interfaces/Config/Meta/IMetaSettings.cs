#region credits
// ***********************************************************************
// Assembly	: Flext.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-21-2013
// ***********************************************************************
#endregion

namespace App.Core.Interfaces.Config.Meta
{
    #region

    

    #endregion

    public interface IMetaSettings
    {
        string AppName { get; set; }

        string Title { get; set; }

        string Description { get; set; }

        string Copyright { get; set; }

        string Keywords { get; set; }

        string Email { get; set; }

    }
}