#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.Configuration;

namespace App.Core.Interfaces.Config.GeoLocation
{
    #region

    

    #endregion
    public interface IGeoService
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [ConfigurationProperty("name")]
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [ConfigurationProperty("title")]
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the params.
        /// </summary>
        [ConfigurationProperty("params")]
        string Params { get; set; }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        [ConfigurationProperty("format")]
        string Format { get; set; }

         /// <summary>
        /// Gets or sets the format.
        /// </summary>
        [ConfigurationProperty("type")]
        string Type { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [ConfigurationProperty("description")]
        string Description { get; set; }

        /// <summary>
        /// Tells if the webserver is enabled
        /// </summary>
        [ConfigurationProperty("enabled")]
        bool Enabled { get; set; }
    }
}