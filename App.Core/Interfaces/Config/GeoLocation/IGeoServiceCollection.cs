#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.Collections.Generic;

namespace App.Core.Interfaces.Config.GeoLocation
{
    #region

    

    #endregion

    public interface IGeoServiceCollection : IEnumerable<IGeoService>
    {

    }
}