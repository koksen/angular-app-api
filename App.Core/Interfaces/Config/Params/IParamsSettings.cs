#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

namespace App.Core.Interfaces.Config.Params
{
    #region

    

    #endregion

    public interface IParamsSettings
    {       
        string LongDateTimeFormat { get; set; }

        string ShortDateTimeFormat { get; set; }

        string TinyDateTimeFormat { get; set; }

        int StartWorkTime { get; set; }

        int EndWorkTime { get; set; }

        int CacheDurationInMinutes { get; set; }

    }
}