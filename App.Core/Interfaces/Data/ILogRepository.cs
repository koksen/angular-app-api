
using App.Core.Models.Common;

namespace App.Core.Interfaces.Data
{ 
    public interface ILogRepository : IRepository<Log>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
