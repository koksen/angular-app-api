
using App.Core.Models;

namespace App.Core.Interfaces.Data
{ 
    public partial interface IUserSessionRepository : IRepository<UserSession>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
