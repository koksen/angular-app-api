
using App.Core.Models;

namespace App.Core.Interfaces.Data
{ 
    public partial interface IUserProfileRepository : IRepository<UserProfile>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
