using System;

namespace App.Core.Interfaces.Data
{
    public interface IDatabaseFactory : IDisposable
    {
        IDataContext Get();
    }
}