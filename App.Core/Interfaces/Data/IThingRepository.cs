
using App.Core.Models;

namespace App.Core.Interfaces.Data
{ 
    public partial interface IThingRepository : IRepository<Thing>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
