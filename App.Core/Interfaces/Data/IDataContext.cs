using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using App.Core.Models.Common;

namespace App.Core.Interfaces.Data
{
    public interface IDataContext
    {
        ObjectContext ObjectContext();

        IDbSet<T> DbSet<T>() where T : BaseModel;

        DbEntityEntry Entry<T>(T entity) where T : BaseModel;

        void Dispose();
    }
}