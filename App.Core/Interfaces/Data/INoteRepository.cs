
using App.Core.Models;

namespace App.Core.Interfaces.Data
{ 
    public partial interface INoteRepository : IRepository<Note>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
