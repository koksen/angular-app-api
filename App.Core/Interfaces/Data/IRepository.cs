﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace App.Core.Interfaces.Data
{
    /// <summary>
    /// Contract with the  generic methods for all the Entities
    /// </summary>
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Query { get; }

        IQueryable<T> GetAll();

        IQueryable<T> GetAllReadOnly();

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate, int maxHits);

        IQueryable<T> Find(Expression<Func<T, bool>> filter,
                           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, int maxHits = 0);

        T GetSingle();

        T GetSingle(Expression<Func<T, bool>> filter);

        T GetById(int id);

        void AddOrUpdate(T entity);

        T Create();

        void Add(T entity);

        void Delete(T entity);

        void Delete(int[] keyValues);

        IQueryable<T> Select(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, int page = 0, int pageSize = 0, string includeProperties = "");

        IQueryable<T> Select(int maxHits, Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);

        IEnumerable<T> SelectIn(Expression<Func<T, int>> valueSelector, IEnumerable<int> values);

        IEnumerable<T> SelectIn(Expression<Func<T, string>> valueSelector, IEnumerable<string> values);

        Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(
            Expression<Func<T, TValue>> valueSelector, IEnumerable<TValue> values);

        long Count();

        long Count(Expression<Func<T, bool>> predicate);

        void SetSearchKey(string searchKey);
        
    }
}
