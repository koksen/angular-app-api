
using App.Core.Models;

namespace App.Core.Interfaces.Data
{ 
    public partial interface IUserRepository : IRepository<User>
    {		
        // Add extra datainterface methods in a partial interface
    }
}
