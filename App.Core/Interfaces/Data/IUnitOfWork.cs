﻿//using Breeze.ContextProvider;

namespace App.Core.Interfaces.Data
{
    /// <summary>
    /// Contract for the UnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        int Commit();

        //Breeze specific
        string Metadata();

        //SaveResult Commit(JObject changeSet);
    }
}
