using System.Collections.Generic;
using App.Core.Models;
using App.Core.ViewModels.Common;

namespace App.Core.Interfaces.Services
{
    public interface INoteService : IService<Note>
    {
        // Add extra serviceinterface methods in a partial interface
        IEnumerable<NoteSummary> GetUnseen(int userId);
        IEnumerable<NoteSummary> GetAll(int userId);
        bool UpdateStatus(NoteStatusUpdate model);
    }
}