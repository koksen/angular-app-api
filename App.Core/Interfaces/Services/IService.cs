using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using App.Core.Interfaces.Validation;

namespace App.Core.Interfaces.Services
{
    public interface IService<T>
    {

        double TimezoneOffset { get; set; }

        T Create();

        IQueryable<T> GetAll();

        IQueryable<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);

        IQueryable<T> GetAllReadOnly();
        
        T GetById(int id);
        
        IValidationContainer<T> SaveOrUpdate(T entity);

        IValidationContainer<T> Insert(T entity);
        
        void Delete(T entity);
        
        void Delete(int id);
        
        void Delete(List<int> keys);
        
        IEnumerable<T> Find(Expression<Func<T, bool>> expression, int maxHits = 100);
        
        IEnumerable<T> Find(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, int maxHits = 100);
        
        IEnumerable<T> FindIn(Expression<Func<T, int>> valueSelector, IEnumerable<int> values);

        T Single(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes);
        
        long Count();
        
        long Count(Expression<Func<T, bool>> expression);
        
        void SetSearchKey(string searchKey);

        bool IsUnique(string property, int value);

        string MetaData();
    }
}
