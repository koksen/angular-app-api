using App.Core.Models;
using App.Core.ViewModels;

namespace App.Core.Interfaces.Services
{
    public partial interface IUserSessionService : IService<UserSession>
    {
        // Add extra serviceinterface methods in a partial interface
        bool Lock(UserSessionRequest model);

        bool IsLocked(UserSessionRequest model);
        UnlockResponse UnLock(UserSessionRequest model);
      
    }
}