using App.Core.Models.Common;

namespace App.Core.Interfaces.Services
{
    public interface ILogService : IService<Log>
    {
    }
}