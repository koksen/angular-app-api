using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;

namespace App.Core.Interfaces.Services
{
    public partial interface IThingService : IService<Thing>
    {
        // Add extra serviceinterface methods in a partial interface
        IValidationContainer<Thing> AddOrUpdate(ThingSummary model);
    }
}