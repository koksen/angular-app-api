using App.Core.Interfaces.Validation;
using App.Core.Models;
using App.Core.ViewModels;

namespace App.Core.Interfaces.Services
{
    public partial interface IUserProfileService : IService<UserProfile>
    {
        // Add extra serviceinterface methods in a partial interface
        IValidationContainer<UserProfile> AddOrUpdate(UserProfileSummary model);
        string SetProfilePicture(string userName, string resizeName, string pictureId);
        UserProfileSummary GetSummary(User user);
    }
}