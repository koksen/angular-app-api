using App.Core.Models;

namespace App.Core.Interfaces.Services
{
    public partial interface IUserService : IService<User>
    {
    }
}