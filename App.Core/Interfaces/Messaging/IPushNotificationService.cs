﻿using System.Collections.Generic;
using System.Threading.Tasks;
using App.Core.Models.Messaging;

namespace App.Core.Interfaces.Messaging
{
    public interface IPushNotificationService
    {
        void Send(PushNotification message);

        Task SendAsync(PushNotification message);

        Task SubscribeToChannelAsync(string deviceToken, string deviceType, List<string> channels);
    }

}
