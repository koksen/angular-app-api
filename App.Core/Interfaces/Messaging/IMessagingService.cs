﻿using System.Threading.Tasks;
using App.Core.Models.Messaging;

namespace App.Core.Interfaces.Messaging
{
    public interface IMessagingService 
    {
        Task Send(string recipient, string message);

        Task Send(string[] recipient, string message);

        Task Send(MessageInfo message);
    }

}
