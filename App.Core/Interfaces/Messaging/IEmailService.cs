﻿using System.Collections.Generic;
using System.Threading.Tasks;
using App.Core.Models.Messaging;

namespace App.Core.Interfaces.Messaging
{
    public interface IEmailService 
    {
        Task Send(string recipient, string message, string subject, Dictionary<string, string> data = null);
        Task Send(string[] recipient, string message, string subject, Dictionary<string, string> data = null);
        Task Send(MessageInfo message);

    }

}
