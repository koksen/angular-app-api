﻿#region credits
// ***********************************************************************
// Assembly	: App.Core
// Author	: Victor Cardins
// Created	: 03-16-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion
namespace App.Core.Interfaces.Storage
{
    public interface IStorageProvider
    {
        T GetValue<T>(string key);

        void SetValue(string key, object value);
    }
}
