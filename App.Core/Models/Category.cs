﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace DurandalAuth.Domain.Model
{
    /// <summary>
    /// Categories for the Articles
    /// </summary>
    [DataContract(IsReference = true)]
    public class Category
    {
        [Key]
        [DataMember]
        public int CategoryId { get; set; }

        /// <summary>
        /// Category Name
        /// </summary>
        [StringLength(100)]
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// The category name accesible by url
        /// </summary>
        [StringLength(100)]
        [DataMember]
        public string UrlCodeReference { get; set; }

        /// <summary>
        /// Create a url reference
        /// </summary>
        public void SetUrlReference()
        {
            char[] arr = Name.Where(c => (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c))).ToArray();
            var urlcodereference = new string(arr);
            UrlCodeReference = urlcodereference.Trim().ToLower().Replace(" ", "-");
        }
    }
}
