﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Core.Models
{
    public class ApplicationUser : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        public string MyAddress { get; set; }
        public new string Id { get; private set; }
    }
}