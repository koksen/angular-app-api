﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;

namespace App.Core.Models
{
    public class Address
    {
        [Required]
        public string FullAddress { get; set; }

        public string City { get; set; }

        [MaxLength(10)]
        public string ZipCode { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public double TimezoneOffset { get; set; }

        public DbGeography GeoLocation { get; set; }
    }
}