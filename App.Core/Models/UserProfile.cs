﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Core.Models.Common;

namespace App.Core.Models
{
    public class UserProfile : BaseModel
    {

        [Key]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public int? ParentUserId { get; set; }

        [ForeignKey("ParentUserId")]
        public User ParentUser { get; set; }

        public Guid UserGuid { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get { return string.Format("{0} {1}", FirstName, LastName).Trim(); }
        }

        public string DisplayName
        {
            get { return string.Format("{0} {1}.", FirstName, !string.IsNullOrEmpty(LastName) ? LastName.Substring(0, 1) : string.Empty); }
        }

        public Address Address { get; set; }

        public string PhotoID { get; set; }

        public int MasterUserId
        {
            get { return ParentUserId.HasValue ? ParentUserId.Value : Id; }
        }

        public UserProfile()
        {
            UserGuid = Guid.NewGuid();
            Address = new Address();
        }
    }
}
