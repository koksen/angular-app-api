﻿using System.ComponentModel.DataAnnotations;
using App.Core.Models.Common;

namespace App.Core.Models
{
    public class Thing : BaseModel
    {
        [Key]
        public int ThingId { get; set; }

        public string Name { get; set; }
    }
}
