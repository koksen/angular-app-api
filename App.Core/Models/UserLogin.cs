﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Core.Models
{
    public class UserLogin : IdentityUserLogin<int> { }
}