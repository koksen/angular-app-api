﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Core.Models.Common;

namespace App.Core.Models
{
    public class UserSession : BaseModel
    {
        [Key]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public string WebUserAgent { get; set; }
        public string MobileUserAgent { get; set; }
        public string WebIp { get; set; }
        public string MobileIp { get; set; }
        public string MobileDeviceId { get; set; }
        public DateTimeOffset? WebLastLogin { get; set; }
        public DateTimeOffset? MobileLastLogin { get; set; }
        public DateTimeOffset? WebLockDateTime { get; set; }
        public DateTimeOffset? MobileLockDateTime { get; set; }

    }
}
