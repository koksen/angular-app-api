#region credits
// ***********************************************************************
// Assembly	: SmartApi.Core
// Author	: Victor Cardins
// Created	: 03-20-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

namespace App.Core.Models.Messaging
{
    #region

    

    #endregion

    public class MessageInfo
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
