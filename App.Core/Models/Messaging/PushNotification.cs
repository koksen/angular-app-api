﻿using System.Collections.Generic;
using App.Core.Enums;

namespace App.Core.Models.Messaging
{
    public class PushNotification
    {
        //string deviceToken, string message, MobileOS device, string sound, int? badge, bool vibrate

        public string DeviceToken { get; set; }

        public string[] DeviceTokens { get; set; }

        public string Icon { get; set; }

        public string Channel { get; set; }

        public string Message { get; set; }

        private string _sound;
        public string Sound
        {
            get { return !string.IsNullOrEmpty(_sound) ? (_sound.Contains(".mp3") ? _sound : string.Format("{0}.mp3", _sound)).ToLower() : "sound.caf"; }
            set { _sound = value; }
        }

        public MobileOS Device { get; set; }

        public int Badge { get; set; }

        public bool Vibrate { get; set; }

        public Dictionary<string, object> Data { get; set; }

        public PushNotification()
        {
            Badge = 0;
            Vibrate = true;
            Data = new Dictionary<string, object>();
        }
    }
}
