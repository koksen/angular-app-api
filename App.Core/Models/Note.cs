﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Core.Enums;
using App.Core.Models.Common;

namespace App.Core.Models
{
    public class Note : BaseModel
    {
        [Key]
        public int NoteId { get; set; }

        public int? UserFromId { get; set; }

        [ForeignKey("UserFromId")]
        public virtual User UserFrom { get; set; }

        public int? UserToId { get; set; }

        [ForeignKey("UserToId")]
        public virtual User UserTo { get; set; }

        [Required]
        public NoteType Type { get; set; }

        public string Title { get; set; }

        public string Tags { get; set; }

        [Index("IX_NOTE_TYPE", IsUnique = false)]
        public int? NoteTypeId { get; set; }
        
        [Required]
        public string Description { get; set; }

        public DateTimeOffset? SeenDateTime { get; set; }
    }
}
