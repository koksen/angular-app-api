﻿using System.Runtime.Serialization;

namespace App.Core.Models.Common
{
    /// <summary>
    ///  Audit info for the different Entities
    /// </summary>
    [DataContract(IsReference = true)]
    public abstract class BaseModelComplete : BaseModel
    {
        /// <summary>
        ///  User creating the entity
        /// </summary>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// User updating the entity
        /// </summary>
        [DataMember]
        public string UpdatedBy { get; set; }

    }
}