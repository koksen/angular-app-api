﻿
using System;

namespace App.Core.Models.Common
{
    public class Weather
    {
        public DateTime datetime { get; set; }
        public double windDirection { get; set; }
        public double elevation { get; set; }
        public double temperature { get; set; }
        public double humidity { get; set; }
        public double windSpeed { get; set; }
        public double dewPoint { get; set; }
        public string countryCode { get; set; }
        public string ICAO { get; set; }
        public string observation { get; set; }
        public string stationName { get; set; }
        public string clouds { get; set; }
        public string weatherCondition { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
    }
}
