#region credits
// ***********************************************************************
// Assembly	: SmartApi.Core
// Author	: Victor Cardins
// Created	: 03-20-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.ComponentModel.DataAnnotations;

namespace App.Core.Models.Common
{
    #region

    

    #endregion

    public class Log : BaseModel
    {
        [Key]
        public int LogId { get; set; }

        [Required]
        public string Type { get; set; }
        
        [Required]
        public string Message { get; set; }

        public int Code { get; set; }

        public string ExceptionType { get; set; }

        public string Username { get; set; }
    }
}
