﻿
using System;

namespace App.Core.Models.Common
{
    public class Location
    {
        public DateTime time { get; set; }
        public DateTime sunset { get; set; }
        public DateTime sunrise { get; set; }
        public double rawOffset { get; set; }
        public double dstOffset { get; set; }
        public double gmtOffset { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
        public string timezoneId { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }

    }
}
