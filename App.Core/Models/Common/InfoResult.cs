#region credits
// ***********************************************************************
// Assembly	: SmartApi
// Author	: Victor Cardins
// Created	: 03-09-2013
// 
// Last Modified By : Victor Cardins
// Last Modified On : 03-28-2013
// ***********************************************************************
#endregion

using System.Runtime.Serialization;

namespace App.Core.Models.Common
{
    #region

    

    #endregion

    [DataContract]
    public class InfoResult
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string ReturnUrl { get; set; }

        [DataMember]
        public object Data { get; set; }

    }
}