using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace App.Core.Models.Common
{
    [DataContract(IsReference = true)]
    public abstract class BaseModel : IValidatableObject
    {
        [NotMapped]
        public int Id
        {
            get
            {
                var keyAttributedProps = GetType().GetProperties().FirstOrDefault(p => p.GetCustomAttributes(typeof(KeyAttribute), true).Length == 1);
                var value = (int) ((keyAttributedProps != null) ? keyAttributedProps.GetValue(this, null) : default(int));
                return value;
            }
            set {}
        }

        /// <summary>
        /// Date the entity was created
        /// </summary>
        private DateTimeOffset _created;
        [DataMember]
        public DateTimeOffset Created
        {
            get { return _created.AddMilliseconds(-_created.Millisecond); }
            set { _created = value; }
        }

        /// <summary>
        /// Date the entity was updated
        /// </summary>
        private DateTimeOffset? _updated;
        [DataMember]
        public DateTimeOffset? Updated
        {
            get { return _updated.HasValue ? _updated.Value.AddMilliseconds(-_updated.Value.Millisecond) : (DateTimeOffset?)null; }
            set { _updated = value; }
        }

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // Override this method to implement custom validation in your entities

            // This is only for making it compile... and returning null will give an exception.
            if (false)
                yield return new ValidationResult("Well, this should not happend...");
        }

        public virtual bool IsTransient()
        {
            return Id == default(int);
        } 
    }
}