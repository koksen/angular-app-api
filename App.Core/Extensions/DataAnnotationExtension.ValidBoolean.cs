﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Core.Extensions
{
    public partial class DataAnnotationExtension
    {
        public class ValidBooleanAttribute : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                return value != null && Convert.ToBoolean(value);
            }
        }
    }
}
