﻿/*
 * Copyright (c) Brock Allen.  All rights reserved.
 * see license.txt
 */

using System;
using System.Security.Claims;
using System.Security.Principal;

namespace App.Core.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool HasClaim(this ClaimsPrincipal user, string type)
        {
            if (user != null)
            {
                return user.HasClaim(x => x.Type == type);
            }
            return false;
        }

        public static int GetUserID(this IPrincipal p)
        {
            var cp = p as ClaimsPrincipal;
            if (cp != null)
            {
                var id = cp.Claims.GetValue(ClaimTypes.NameIdentifier);
                Int32 g;
                if (Int32.TryParse(id, out g))
                {
                    return g;
                }
            }
            throw new Exception("Invalid NameIdentifier");
        }

        public static string GetUserName(this IPrincipal p)
        {
            var cp = p as ClaimsPrincipal;
            if (cp != null)
            {
                var username = cp.Claims.GetValue(ClaimTypes.Name);
                return username;
            }
            throw new Exception("Invalid Name");
        }

        public static bool HasUserID(this IPrincipal p)
        {
            var cp = p as ClaimsPrincipal;
            if (cp != null)
            {
                var id = cp.Claims.GetValue(ClaimTypes.NameIdentifier);
                int g;
                if (int.TryParse(id, out g))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
