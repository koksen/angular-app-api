using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Models;

namespace App.Core.Extensions
{
    public static class UserExtensions
    {
        public static bool HasClaim(this User account, string type)
        {
            if (account == null) throw new ArgumentException("account");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentException("type");

            return account.Claims.Any(x => x.ClaimType == type);
        }

        public static bool HasClaim(this User account, string type, string value)
        {
            if (account == null) throw new ArgumentException("account");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentException("type");
            if (String.IsNullOrWhiteSpace(value)) throw new ArgumentException("value");

            return account.Claims.Any(x => x.ClaimType == type && x.ClaimValue == value);
        }

        public static IEnumerable<string> GetClaimValues(this User account, string type)
        {
            if (account == null) throw new ArgumentException("account");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentException("type");

            var query =
                from claim in account.Claims
                where claim.ClaimType == type
                select claim.ClaimValue;
            return query.ToArray();
        }

        public static string GetClaimValue(this User account, string type)
        {
            if (account == null) throw new ArgumentException("account");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentException("type");

            var query =
                from claim in account.Claims
                where claim.ClaimType == type
                select claim.ClaimValue;
            return query.SingleOrDefault();
        }
    }
}