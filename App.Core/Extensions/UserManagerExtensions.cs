using System;
using App.Core.Helpers;
using Microsoft.AspNet.Identity;

namespace App.Core.Extensions
{
    public static class UserManagerExtensions
    {
        /// <summary>
        ///     Find a user by email
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static TUser FindByPhoneNumber<TUser, TKey>(this UserManager<TUser, TKey> manager, string phoneNumber)
            where TKey : IEquatable<TKey>
            where TUser : class, IUser<TKey>
        {
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }
           
            return AsyncHelper.RunSync(() => manager.FindByEmailAsync(phoneNumber));
        }
    }
}